/*
*
*	Programmer: Lukasz Czerwinski | luki2p@gmail.com
*   ===============================================================
*   Copyright (c) 2015 Lukasz Czerwinski & LUCKYJQUERY.NET
*   ===============================================================
*   For Envato Marketplace - CC
*   ===============================================================
*	SLIDE GALLERY | JQUERY 1.11+ & JQUERY UI
*
*/
;(function ($) {
    /* Global variables */
    var prty, $this, li, hrefT, viewPort;

	$.fn.slideGallery = function(settings) {
		//Defaults settings
		 var opts = $.extend({}, prty.settings, settings);
         //Setup
         prty.Setup($(this), opts);
	}; //End FN

	//Basic arrays
	prty = {
        //Default settings
        settings: {
            Speed			: 250,			//Speed animations
            thumbHeight		: 170,			//Default height of thumbs
            thumbWidth		: 240,			//Default Width of thumbs
            viewHeight		: 450,			//Default Height of viewPort
            items			: [],			//Array with elements
            actItem			: 0,			//Array with active element
            category		: true,			//Short by category
            description		: true,			//Display the description
            Columns			: 5,			//Columns number
            navigation		: true,			//Display buttons to navigate
                keyboard	: 1,			//(true) Keyboard
                keyClose	: "c",			//(string) Key to close
                keyPrev		: "p",			//(string) Key to previous element
                keyNext		: "n",			//(string) Key to next element
            margin			: 10 			//margin between thumbs

        },
		/*  define basic elements*/
        //Gallery div
        galleryDiv: '',
        items: '',
        picts: '',
        Close: '',
        Prev: '',
        Next: '',
        categoryUl: '',
        cateMenu: '',
        /* FUNCTIONS */
        Setup: function(gDiv, settings) {
            //Select the gallery div
            prty.galleryDiv = gDiv;
            //Select the items
            prty.items = gDiv.find('ul:last').addClass('entries').wrap('<div class="wrapper-ul"></div>').find('li');
            //Create category menu
            if (settings.category) {
                prty.categoryUl = gDiv.prepend('<ul class="categories"><li class="menu"><a href="#">Category</a></li><li data-cat="All"><a href="#" class="activeCategory">All</a></li></ul>').find('ul.categories');
                prty.cateMenu = gDiv.find('li.menu a');
            }
            //Build entry
            for(var i = 0; i < prty.items.length; i++) {
                //Select element
                $this = $(prty.items[i]).find('a');
                    //Push information to array
                    settings.items.push(new Array(
                        $this.attr('href'),
                        $this.data('category'),
                        $this.text(),
                        $this.parent().find('.content').html(),
                        $('li').index($this.parent('li')),
                        $this.data('thumb')
                    ));
                    li = $this.parent("li").attr('id', settings.items[i][4]);
                    //Thumbnail SRC
                    if(settings.items[i][5] !== undefined) {
                        hrefT = settings.items[i][5];
                    } else {
                        hrefT = settings.items[i][0];
                    }
                    //Create thumbnail
                    $this.replaceWith('<div class="mascItem"><img src="'+hrefT+'" alt="'+settings.items[i][2]+'" data-url="'+settings.items[i][0]+'" /></div><a href="#" class="viewIt"></a><div class="title">'+settings.items[i][2]+'</div><div class="corn"></div>');
                    //Resize
                    var tHeight = li.find('img').height(),
                        tWidth = li.find('img').width();
                        prty.resizerIMG(li.find('img'), tHeight, tWidth, settings);
                    //Add attributes
                    if(settings.category) {
                        li.attr('data-category', settings.items[i][1]);
                        if (prty.categoryUl.find('li[data-cat|="'+settings.items[i][1]+'"]').length == 0) {
                            prty.categoryUl.append('<li data-cat="'+settings.items[i][1]+'"><a href="#">'+settings.items[i][1]+'</a></li>');
                        }
                    }
            } //End each fn, build
                    //Category support
                    if(settings.category) prty.category(settings);

            //Thumb events
            prty.mouseEvents(settings);
            //Responsive support
            $(window).off().on('resize', function(){
                prty.rwd(settings);
            });
        },
        //Mouse Events
        mouseEvents: function(settings) {
            var hovert, hovered;
            //Events for thumbnails
            prty.items
            //Mouseover
            .on('mouseover', function() {
                hovert = $(this);
                //Change opacity and show the button
                hovert.find('img').stop(true, false).animate({opacity: 0.5}).addBack().find('a').stop(true, false).fadeIn(settings.Speed);
            })
            //Mouseout
            .on('mouseleave', function() {
                hovered = $(this);
                //Check if the thumb is hover and it isn't active
                if (!hovered.is('.active')) {
                    //Change opacity and show the button
                    hovered.find('img').stop(true, true).animate({opacity: 1.0}).addBack().find('a').stop(true, false).fadeOut(settings.Speed);
                }
            })
            //Click a thumbnail button
            .find('a:last').hide().on('click', function(e) {
                e.preventDefault();
                Click = $(this);
                //Resetting
                settings.actItem = 0;

                //Add the elements to the array, but not active
                while (settings.items[settings.actItem][4] != Click.parents('li').attr('id')) {
                    settings.actItem++;
                }
                    /* Toggle viewPort */
                    prty.viewPort(Click.parents('li'), settings);
            });
        },
        //Build the ViewPort
        viewPort: function(itm, settings) {
            var IdItem = itm.attr('id');
                //deactive the thumb
                prty.thumbsAnimate($('.slideGallery .active').not(itm), false, settings);
            //Toggle viewport
            if(!itm.is('.active')) {
                //Active the thumb
                prty.thumbsAnimate(itm, true, settings);
                //Wait to end other animations
                var Wait = setInterval(function(){
                    if (!$('.viewPort').not('#'+IdItem+'.viewPort').length) {
                        clearInterval(Wait);
                            //Build structure
                            itm.parents('div').first().append('<div class="viewPort" id="'+IdItem+'"><div class="wrapper"></div></div>');
                            if(typeof itm.position().top != undefined) {
                                //Declare elements of structure
                                viewPort= $('#'+IdItem+'.viewPort').
                                //Set styles
                                css({
                                    height: settings.viewHeight,
                                    top: itm.position().top+settings.thumbHeight+settings.margin,
                                    left: 0
                                });
                                //Load content
                                prty.loadContent(viewPort, settings);
                                //Show in browser window
                                prty.visible(itm, settings);
                                //Add navigation
                                if(settings.navigation) {
                                    prty.navigation(viewPort, settings);
                                }
                            //Responsive;
                            prty.rwd(settings);
                            }
                    }
                }, 200);
            } else {
                //deactive the thumb
                prty.thumbsAnimate(itm, false, settings);
            }
			//Hide viewPorts
			$('div.viewPort').slideUp(settings.Speed, function() {
				$(this).remove();
			});
        },
        //Content
        loadContent: function(viewPort, settings) {
            //Define basic variables for content
            var wrapper = viewPort.find('.wrapper'), hrefY, hrefV,
                hrefLink = settings.items[settings.actItem][0];
                //If image
                if (hrefLink.indexOf("jpg", ".") > 0 || hrefLink.indexOf("png", ".") > 0 || hrefLink.indexOf("gif", ".") > 0) {

                       wrapper.append("<div class='image'><img /></div>").
                       hide();
                        var ImgLoad = new Image ();

                       ImgLoad.onload = function () {
                            //Add the SRC and change size
                            wrapper.find(".image img").attr("src", ImgLoad.src).height(settings.viewHeight);
                            ImgLoad.onload = function(){};

                       };
                       ImgLoad.src = hrefLink;
                }

                /* OTHER MEDIA */
                //YouTube
                if (hrefLink.indexOf( "youtube", "." ) > 0) {
                        //YouTube links
                        hrefY = "http://www.youtube.com/embed/" +prty.youtubeParser(hrefLink);
                        wrapper.append( "<div class='object'><iframe class='' type='text/html' width='100%' height='" + settings.viewHeight + "' src='" + hrefY + "?autoplay=0&rel=0' frameborder='0'></iframe></div>" );
                }
                //If Vimeo
                if (hrefLink.indexOf( "vimeo", "." ) > 0) {
                    hrefV = hrefLink.substr(-8);
                    wrapper.append( "<div class='object'><iframe src='http://player.vimeo.com/video/"+hrefV+"?title=0&amp;byline=0&amp;portrait=0;&amp;autoplay=1' width='100%' height='"+settings.viewHeight+"' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>" );
                }
                /* TEXT Content */
                wrapper.append('<div class="text"><h2>'+settings.items[settings.actItem][2]+'</h2>'+settings.items[settings.actItem][3]+'</div>');
                viewPort.slideDown(settings.Speed, function(){
                    wrapper.fadeIn(settings.Speed);
                });
        },
        //Scroll to Item
        visible: function(itm, settings){
          var windowScroll = $('html, body'),
              itmScroll = itm.position().top+itm.height();
            if(windowScroll.scrollTop() != itmScroll) {
                windowScroll.animate({
                    scrollTop: itmScroll
                }, settings.Speed);
            }
        },
        thumbsAnimate: function(itm, fx, settings) {
            if(fx) {
                //Add class
                itm.toggleClass('active', true)
                //Show the corn
                .find('.corn').fadeIn(settings.Speed).parent()
                    //Animate the margin
                    .animate({'margin-bottom': settings.viewHeight+settings.margin*2}, settings.Speed).addBack()
                .find('img').stop(true, false).animate({opacity: 0.5}, settings.Speed, function(){
                        itm.find('a').stop(true, false).fadeIn(settings.Speed);
                    });
                //Animate the title color
                if(settings.colorAnimate !== "") {
                    itm.find('.title').stop(true, false).animate({
                        color: settings.colorAnimate
                    }, settings.Speed);
                }
            } else {
                //Remove class
                itm.toggleClass('active', false)
                //Hide the corn
                .find('.corn').fadeOut(settings.Speed).parent()
                    //Animate the margin
                    .animate({'margin-bottom': settings.margin}, settings.Speed).addBack()
                .find('img').stop(true, false).animate({opacity: 1.0}, settings.Speed, function(){
                        itm.find('a').stop(true, false).fadeOut(settings.Speed);
                    });
                //Animate the title color
                if(settings.colorAnimate != "") {
                    itm.find('.title').stop(true, false).animate({
                        color: settings.currentcolor
                    }, settings.Speed);
                }
            }
        },
        //Navigation
        navigation: function(viewPort, settings) {
            //Add elements of nav
            viewPort.find('.wrapper').append('<a href="#" class="close"></a><a href="#" class="prev"></a><a href="#" class="next"></a>');
            //Set the element
            prty.Close = viewPort.find('.close'),
			prty.Next = viewPort.find('.next'),
			prty.Prev = viewPort.find('.prev');
            var nextItem = $('.slideGallery .active').next('li').is(':visible'),
                prevItem = $('.slideGallery .active').prev('li').is(':visible');
                /* Support for navigation */
                //Preveus
                if(prevItem){
                    //Click
                    prty.Prev.click(function(e){
                        prty.prevItem(settings);
                        e.preventDefault();
                    });
                } else {
                    prty.Prev.hide();
                }
                //Next
                if(nextItem ){
                    prty.Next.click(function(e){
                        prty.nextItem(settings);
                        e.preventDefault();
                    });
                } else {
                    prty.Next.hide();
                }
                    prty.Close.click(function(e){
                        prty.viewPort($('.slideGallery .active'), settings);
                        e.preventDefault();
                    });
                    //Keyboard support
                    if(settings.keyboard) {
                        prty.keyboard(nextItem, prevItem, settings);
                    }
        },
        //Preveus item
        prevItem: function(settings) {
            var prevItem = $('.slideGallery .active').prev('li');
                settings.actItem--;
                prty.viewPort(prevItem, settings);
        },
        //Preveus item
        nextItem: function(settings) {
            var nextItem = $('.slideGallery .active').next('li');
                settings.actItem++;
                prty.viewPort(nextItem, settings);
        },
        //Category support
        category: function(settings){
		//Basic
		var clicked, toSort, entries,
            toClick = prty.galleryDiv.find('.categories li:not(.menu) a')
            .on('click', function(e) {
                e.preventDefault();
			    clicked = $(this),
				toSort = $(this).parent('li').data('cat'),
				entries = prty.galleryDiv.find('ul.entries');
				if (!clicked.is('.activeCategory')) {
					//Close viewPort
					if($('.viewPort').length)  prty.viewPort($('.slideGallery .active'), settings);
					//Toggle out active classes
					toClick.toggleClass('activeCategory', false);
					//Add active class
					clicked.toggleClass('activeCategory', true);
					if (toSort != "All") {
						//Hide other
						entries.find('li[data-category!="'+toSort+'"]').hide(settings.Speed);
						//Show the shorted
						entries.find('li[data-category="'+toSort+'"]').show(settings.Speed);
					} else {
						//Show all
						entries.find('li').show(settings.Speed);
					}
				}
		});
		prty.cateMenu.click(function(e) {
            e.preventDefault();
			var cMenu = $(this);
			if (!cMenu.is('.open')) {
				//Toggle class
				cMenu.toggleClass('open', true).
				//Show li
				parents('ul.categories').find('li').not(':first').show(settings.Speed);
			} else {
				//Toggle class
				cMenu.toggleClass('open', false).
				//hide li
				parents('ul.categories').find('li').not(':first').hide(settings.Speed);
			}
		});

        },
        //Keyboard support
        keyboard: function(nextItem, prevItem, settings) {
            var codeAscii, KeyCode;
            $(document.documentElement).off().on('keydown', function (event) {
                codeAscii = event.which;
                KeyCode = String.fromCharCode(codeAscii).toLowerCase();

                //Left
                if(event.keyCode == 37 || KeyCode == settings.keyPrev) {
                    if (prevItem) prty.prevItem(settings);
                }
                //Right
                if(event.keyCode == 39 || KeyCode == settings.keyNext) {
                    if (nextItem) prty.nextItem(settings);
                }
                //ESC
                if(event.keyCode == 27 || KeyCode == settings.keyClose) {
                    prty.viewPort($('.slideGallery .active'), settings);
                }
            });
        },
        //resize
        resizerIMG: function (Img, Height, Width, settings) {
            if (Width>Height) {
                Img.height(settings.thumbHeight);
                if (Img.width() < settings.thumbWidth) {
                    Img.width(settings.thumbWidth);
                }
            } else {
                Img.width(settings.thumbWidth);
                if (Img.height() < settings.thumbHeight) {
                    Img.height(settings.thumbHeight);
                }
            }
        },
        //RWD
        rwd: function (settings) {
            //Basic informations
            var gallery = $('.slideGallery'),
                galleryWidth = gallery.width(),
                tWidth = settings.thumbWidth+settings.margin,
                realColumns = Math.floor(galleryWidth/tWidth),
                viewPort = gallery.find('.viewPort'),
                tItem = gallery.find('.active'),
                newPosY;
                //Remove break lines
                gallery.find('ul.entries li').css('margin-right', settings.margin);
                    //Grids
                    if (settings.Columns <= realColumns-1) realColumns = settings.Columns;
                    if(realColumns-1 <= 1) realColumns = 1;
                //ViewPort
                if (viewPort.length) {
                    newPosY = tItem.parent('ul').find('li.active').position().top+settings.thumbHeight+settings.margin;
                    //Change witdh
                    viewPort.css('width', (tItem.width()+settings.margin)*(realColumns));
                        //Check visibility
                        if(viewPort.position().top != newPosY) {
                            viewPort.css('top', newPosY);
                            //Scroll to the viewPort
                            prty.visible(tItem, settings);
                        }
                }
                //Responsive menu
                if (settings.category) {
                    if ($(window).width() >= 818) {
                        gallery.find('ul.categories').find('li').not(':first').fadeIn(settings.Speed);
                    } else {
                        gallery.find('ul.categories').find('li').not(':first').hide().
                        parent().find('li.menu a').toggleClass('open', false);
                    }

                }
        },
        youtubeParser: function(url){
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match&&match[7].length==11){
                return match[7];
            }
        }
	};

})(jQuery); //The end