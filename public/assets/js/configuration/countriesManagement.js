/**
 *
 */



function displayListCountries()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	 $.ajax
    ({
        url : base_url + "/administrator/displayListCountries",
        data : {_token : _token},
        dataType : "html",
        type : "POST",
        success : function(response){
        	$('.ListCountriesGrid').html(response);
        }
    });

}

$(function(){
	displayListCountries();
	$("#BTN_ADD_COUNTRY").on('click',function(){
		var base_url 	= $("#BASE_URL").val();
		var url 		= base_url + "/administrator/AddNewCountryForm";
		$.colorbox({href: url ,width:"600",height:"470",iframe : true,escKey: false,overlayClose: false});
	});


	$(".ListCountriesGrid").on('click',"img[id*=EDIT_COUNTRY_]",function(){
	      var c_id = $(this).parents('tr').data('c_id');
	      var base_url = $("#BASE_URL").val();
	      var url = base_url + "/administrator/EditCountry/" + c_id;
			$.colorbox({href: url ,width:"600",height:"470",iframe : true,escKey: false,overlayClose: false});
	});

	$(".ListCountriesGrid").on('click',"img[id*=DELETE_COUNTRY_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var c_id 		= $(this).parents('tr').data('c_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token	= $('input[name=_token]').val();
	        var str_params ={id : c_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/administrator/DeleteCountry/" + c_id,
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListCountries();
	                      }
	                  });
	              }
	            }
	        });
	});

});