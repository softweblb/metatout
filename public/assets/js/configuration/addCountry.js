/**
 *
 */

function ValidationCountryForm()
{

}

$(function(){
	 $("#BTN_ADD_COUNTRY").on('click',function(){
		 	var base_url = $('#BASE_URL').val();
		 	var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_ADD_COUNTRY").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/ajaxAddCountry",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.parent.displayListCountries();
	            	  window.parent.$.colorbox.close();
	              }
	            }
	        });
	    });
});