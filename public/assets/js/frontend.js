/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
 $(function(){
	
     $(window).resize(function(){
         ResizePage();
         resizeHomePage();
     })
     ResizePage();
     $('.SideUp').on('click',function(){
         $(document).scrollTo('0px', 800);
     });
     var height = $(document).height();
    // $('.ContactContent').css('top',( height - 162 ) + "px");

     $('.ChangeLang').on('click',function(){
         var lang = $(this).data('lang');
         var base_href = $('base').attr('href');
         document.cookie ='bplus_Lang=' + lang + '; expires=Fri, 3 Aug 2999 20:47:11 UTC; path=/';
         window.location.href = $("#MAIN_BASE_URL").val();
     });

     //
    

     $('.SearchIcon').on('click',function(){
         $('.SearchBar').slideDown('fast');
     });
     $('.btnSearch').on('click',SearchPage);
     $('.textSearch').on('keypress',function(e){
        if(IsEnter(e))
        {
            SearchPage();
        }
     });

     //HomeProductsItems
     resizeHomePage();
     $('.ImgProfile').each(function(n){
    	 
     });
    $(document).scroll(function(){
    	 /**var top_menu = $(document).scrollTop();
    	 $('.MainRow').css('top',top_menu);
    	$('.MainMenu').css('top',top_menu);*/
     }); 
    
    $(document).scroll(function() {
    	var currPos = $(document).scrollLeft();
    	var currPosMenu = $('.MainMenuBlock').css('left');
        var currPosMenu = $('.MobMainMenu').css('left');
        var pos = - currPos - 28;
        $('.MainMenuBlock').css('left',pos);
        $('.MobMainMenu').css('left',pos);
    });
    
    $('body').flowtype({
		minFont : 8,
		maxFont : 16
	});

   if(msieversion() == 1)
   {
	   $('.MsCatTitle').css('display','');
   }
   
});

function ResizePage()
{
    var height = $(document).height();
    var innerHeight = $('.CertificatesImages').innerHeight();
    var menuheight = parseInt($('.MainRow').innerHeight());
    $('.Page').height(height);
    $('.MediaPage').height(height + 200);

}

function SearchPage()
{
    var search_key 	= $('input[name=textSearch]').val();
    var base_url 	= $('#MAIN_BASE_URL').val();
    search_key 		= encodeURIComponent(search_key);
    
    window.location.href = base_url + "search/listResults/" + search_key;
}

function resizeHomePage()
{
	var height_home = $('.HomePage').height();
	var mob_height_home = $('.MobHomePage').height()/3;
	var pos = 0;
	var pos_title = 0;
	var Mobpos = 0;

	var userAgent = navigator.userAgent;
	if(userAgent.indexOf('Firefox') > 0)
	{
		pos = height_home * 0.72;
		pos_title = height_home * 0.86;
		Mobpos = mob_height_home * 0.4;
	}
	else
	{

		pos = height_home * 0.725;
		pos_title = height_home * 0.88;
		Mobpos = mob_height_home * 0.4;
	}
	$('.HomeProductsItems').css('top',pos + "px");
	$('.HomeProductsItemsTitle').css('top',pos_title + "px");
	$('.MobHomeProductsItems').css('top',Mobpos + "px");
	$('.ProductHomeItem').each(function(n){
		var pos = $(this).offset();
		var width_bar = $(this).width();
		var left_pos = pos.left;
		
		$( $('.ProductHomeItemTitle')[n] ).css('left',left_pos + 'px');
		$( $('.ProductHomeItemTitle')[n] ).css('position','absolute');
		$( $('.ProductHomeItemTitle')[n] ).width(width_bar + 'px');
		
	});
	
	
	left_pos = $( $('.ProductHomeItemTitle')[0] ).css('left');
	$( $('.ProductHomeItemTitle')[0] ).css('left',( left_pos - 20) + 'px');
	//
	var page_width = window.innerWidth;
	var window_width = ( page_width - 18 ) + "px";
	$('#MAINMENU').css('width',String( window_width ));
	$('#MAINROW').css('width',String( window_width ));
	$('#MAINTABLE').css('width',String( window_width ));
	
	
	var main_menu_row = $('.HomeProductsItems').width();
	$('.HomePage').css('width',String( main_menu_row ));
	$('.homebar').css('width',String( main_menu_row ));
	$('.WebsiteFooter').css('width',String( main_menu_row ));
	$('.MobFooter').css('width',String( main_menu_row ));
	
	//var top_menu = $(document).scrollTop();
	//$('.MainMenuBlock').css('top',top_menu);
	
	//$('.MainPageContent').width(page_width);
	
	 var document_width = $(window).width();
     var menu_right = document_width - 85;
     $('.RightButtons').css('left',menu_right + 'px');
     
     var position_top = screen.height / 4;
     $('.RightButtons').css('top' , position_top + "px");
     
     var menu_mob_width = $('#MobMAINBLOCKMENU').width();
     menu_mob_width = menu_mob_width + 30;
     menu_mob_width = menu_mob_width + "px";
     $('.MobMainMenu').width(menu_mob_width);
     $('.MobHomePage').width(menu_mob_width);
     $('.MobFooter').width(menu_mob_width);
	
}

window.onresize = function() {
	resizeHomePage();
};
function ScrollMenu()
{
	console.log('IN');
}

