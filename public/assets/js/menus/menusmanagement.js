/**
 *
 */
function displayListMenus()
{
	var base_url = $('#BASE_URL').val();
    var _token = $('input[name=_token]').val();
    var cm_is_active = $('#CM_IS_ACTIVE').val();
    $.ajax
    ({
        url : base_url + "/displayListMenus",
        data : {_token : _token , cm_is_active : cm_is_active},
        dataType : "html",
        type : "POST",
        success : function(response){
            $('.ListMenuGirds').html(response);
        }
    });
}

$(function(){
	displayListMenus();
	$('#CM_IS_ACTIVE').on('change',displayListMenus);
	$("#BTN_ADD_MENU").on('click',function(){
		var base_url = $("#BASE_URL").val();
	    $.colorbox({href:base_url + "/administrator/AddMenu",width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});

	$(".ListMenuGirds").on('click',"img[id*=EDIT_MENU_]",function(){
	      var cm_id = $(this).parents('tr').data('cm_id');
	      var base_url = $("#BASE_URL").val();
	      var href = base_url + "/administrator/EditMenu/" + cm_id;
		    $.colorbox({href: href,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});

	$(".ListMenuGirds").on('click',"img[id*=DELETE_MENU_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cm_id 	= $(this).parents('tr').data('cm_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cm_id : cm_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/administrator/DeleteMenu/" + cm_id,
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListMenus();
	                      }
	                  });
	              }
	            }
	        });
	});
});