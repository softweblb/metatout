/**
 *
 */

$(function(){
	 $("#BTN_EDIT_MENU").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_MENU").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/EditMenuOperation",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	 window.parent.displayListMenus();
	            	 window.parent.$.colorbox.close();
	              }
	            }
	        });
	    });


});