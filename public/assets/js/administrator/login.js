/**
 *
 */

$(function(){
	$("#U_USERNAME,#U_PASSWORD").on('keypress',function(e){
	        if(IsEnter(e))
	        {
	            $("#BTN_LOGIN").trigger('click');
	        }
	    })
	   $("#BTN_LOGIN").on('click',function(){
	       var str_params = $('#LogInForm').serialize();
	       $.ajax
	        ({
	            url : "aLogin",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	                if(response.is_error == 1)
	                {
	                    $('.bg-warning').html(response.error_msg);
	                    $('.bg-warning').height(50);
	                    $('.bg-warning').css('text-align' , "center");
	                }
	                else
	                {
	                    window.location.href = "administrator/dashboard";
	                }

	            }
	        });
	   });
});