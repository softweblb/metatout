/**
 *
 */


$(function(){
	$('textarea[id=CA_ALBUM_CAPTION]').ckeditor();
	 $("#BTN_ADD_ALBUM").on('click',function(){
		 var base_url = $('#BASE_URL').val();
		 var _token = $('input[name=_token]').val();
	        var page_id = $('#PAGE_ID').val();
	        var str_params = $("#FORM_ADD_ALBUM_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/RequestAddAlbum",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/PageContent/" + page_id;
	              }
	            }
	        });
	    });


});