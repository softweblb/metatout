/**
 *
 */


function displayListSingleImageManagement()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	var cms_page = $('input[id=CMS_PAGE]').val();
    var cms_lang = $('select[id=CMS_LANG]').val();
    if(cms_page != 0)
	{
    	 $.ajax
	    ({
	        url : base_url + "/displayListSingleImage",
	        data : {_token : _token , cms_page : cms_page , cms_lang : cms_lang},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	        	$('.ListSingleImageGrid').html(response);
	            $('#BTN_ADD_SI').css('display','');
	        }
	    });
	}

}

$(function(){
	displayListSingleImageManagement();
	$("#CMS_LANG").on('change',displayListSingleImageManagement);
	$("#BTN_ADD_SI").on('click',function(){
	    var cms_page = $('input[id=CMS_PAGE]').val();
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/addSImage/" + cms_page;
	});


	$(".ListSingleImageGrid").on('click',"img[id*=EDIT_SI_]",function(){
	      var cms_page = $('input[id=CMS_PAGE]').val();
	      var cm_row_id 	= $(this).parents('tr').data('cm_row_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/editSImage/" + cm_row_id;
	});

	$(".ListSingleImageGrid").on('click',"img[id*=DELETE_SI_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cm_row_id 	= $(this).parents('tr').data('cm_row_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cm_row_id : cm_row_id , _token : _token};
	        console.log(str_params);
	         $.ajax
	        ({
	            url : base_url + "/ajaxDeleteSImage",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListSingleImageManagement();
	                      }
	                  });
	              }
	            }
	        });
	});

});