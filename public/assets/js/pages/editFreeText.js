/**
 *
 */

$(function(){
	$('textarea[id*=FT_TEXT_CONTENT]').ckeditor();
	 $("#BTN_EDIT_FT").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_FT_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/ajaxEditFreeText",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/FreeTextManagement";
	              }
	            }
	        });
	    });
})