/**
 *
 */

$(function(){
	$('textarea[id=CA_ALBUM_CAPTION]').ckeditor();
	 $("#BTN_EDIT_ALBUM").on('click',function(){
		 var base_url = $('#BASE_URL').val();
		 var page_id = $('#PAGE_ID').val();
		 var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_ALBUM_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/RequestEditAlbum",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/PageContent/" + page_id;
	              }
	            }
	        });
	    });
});