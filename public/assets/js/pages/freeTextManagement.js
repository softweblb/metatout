/**
 *
 */

function displayListFreeTextContent()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
    var cms_lang = $('select[id=CMS_LANG]').val();
    $.ajax
    ({
        url : base_url + "/administrator/displayListFreeText",
        data : {_token : _token , cms_lang : cms_lang},
        dataType : "html",
        type : "POST",
        success : function(response){
        	$('.ListFreeTextGrid').html(response);
        }
    });

}

$(function(){
	displayListFreeTextContent();
	$("#CMS_LANG").on('change',displayListFreeTextContent);
	$("#BTN_ADD_FT").on('click',function(){
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/administrator/AddNewFreeText";
	});


	$(".ListFreeTextGrid").on('click',"img[id*=EDIT_FT_]",function(){
	      var base_url = $("#BASE_URL").val();

	      var ft_row_id = $(this).parents('tr').data('ft_row_id');
	      window.location.href = base_url + "/administrator/EditFreeText/" + ft_row_id;
	});

	$(".ListFreeTextGrid").on('click',"img[id*=DELETE_FT_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var ft_row_id 	= $(this).parents('tr').data('ft_row_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={ft_row_id : ft_row_id , _token : _token};
	        console.log(str_params);
	         $.ajax
	        ({
	            url : base_url + "/administrator/ajaxDeleteFreeText",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListFreeTextContent();
	                      }
	                  });
	              }
	            }
	        });
	});

});