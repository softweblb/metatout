/**
 *
 */


$(function(){
	$("#BTN_SAVE").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FRM_PAGE_TYPES").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/SavePageTypeManagement",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	 alert(response.error_msg);
	                 window.location.href = base_url + "/administrator/PageTypeManagement";
	              }
	            }
	        });
	    });

})