/**
 *
 */
$(function(){
	$('textarea[id*=CE_EVENT_DESCRIPTION_]').ckeditor();
	 $("input[name=ce_from_date]").datetimepicker({
	        //language:  'fr',
	        weekStart: 1,
	        todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
	        showMeridian: 1
	    });
		$("input[name=ce_to_date]").datetimepicker({
	        //language:  'fr',
	        weekStart: 1,
	        todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
	        showMeridian: 1
	    });
	 $("#BTN_ADD_EVENT").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_ADD_EVENT_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	        var page_id = $("#PAGE_ID").val();
	         $.ajax
	        ({
	            url : base_url + "/AjaxAddEvent",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/PageContent/" + page_id;
	              }
	            }
	        });
	    });


});