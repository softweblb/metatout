/**
 *
 */

$(function(){
	$('textarea[id*=RT_PAGE_CONTENT]').ckeditor();
	 $("#BTN_ADD_RT").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_ADD_RT_INFO").serialize();
	        var page_id = $("#PAGE_ID").val();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/ajaxAddRichText",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/PageContent/" + page_id;
	              }
	            }
	        });
	    });
})