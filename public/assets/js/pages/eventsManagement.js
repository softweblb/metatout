/**
 *
 */


function displayListEventsManagement()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	var cms_page = $('input[id=CMS_PAGE]').val();
    var cms_lang = $('select[id=CMS_LANG]').val();
    if(cms_page != 0)
	{
    	 $.ajax
	    ({
	        url : base_url + "/displayListEvents",
	        data : {_token : _token , cms_page : cms_page , cms_lang : cms_lang},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	        	$('.ListEventsGrid').html(response);
	        }
	    });
	}

}

$(function(){
	displayListEventsManagement();
	$("#CMS_LANG").on('change',displayListEventsManagement);
	$("#BTN_ADD_EVENT").on('click',function(){
	    var cms_page = $('input[id=CMS_PAGE]').val();
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/AddEvents/" + cms_page;
	});


	$(".ListEventsGrid").on('click',"img[id*=EDIT_EVENT_]",function(){
	      var ce_row_id = $(this).parents('tr').data('ce_row_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/EditEvents/" + ce_row_id;
	});

	$(".ListEventsGrid").on('click',"img[id*=DELETE_EVENT_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var ce_row_id 	= $(this).parents('tr').data('ce_row_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={ce_row_id : ce_row_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/DeleteEvent/" + ce_row_id,
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListEventsManagement();
	                      }
	                  });
	              }
	            }
	        });
	});

});