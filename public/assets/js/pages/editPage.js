/**
 *
 */

$(function(){
	 $("#BTN_EDIT_PAGE").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_PAGE").serialize();
	        var menu_id = window.parent.$('#CMS_MENU').val();
	        str_params +=  "&_token=" + _token + "&menu_id=" + menu_id;
	         $.ajax
	        ({
	            url : base_url + "/ajaxEditPage",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	                  window.parent.displayListPages();
	                  window.parent.$.colorbox.close();
	              }
	            }
	        });
	    });
})