/**
 *
 */

$(function(){
	 $("#BTN_ST").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_ST_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/administrator/SaveSimpleTextInfo",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = window.location.href;
	              }
	            }
	        });
	    });
	 $('textarea').ckeditor();
})