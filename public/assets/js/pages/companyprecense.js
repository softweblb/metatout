/**
 *
 */

function displayListCompanyPrecense()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	$.ajax
    ({
        url : base_url + "/administrator/displayListPrecense",
        data : {_token : _token },
        dataType : "html",
        type : "POST",
        success : function(response){
        	$('.ListCompanyPrecense').html(response);
        }
    });

}

$(function(){
	displayListCompanyPrecense();
	$("#BTN_COMPANY_PRECENSE").on('click',function(){
		var base_url = $("#BASE_URL").val();
		var href_url = base_url + "/administrator/AddCompanyPrecense";
		$.colorbox({href: href_url,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});


	$(".ListCompanyPrecense").on('click',"img[id*=EDIT_COMPANY_]",function(){
		var cp_id 	= $(this).parents('tr').data('cp_id');
		var base_url = $("#BASE_URL").val();
		var href_url = base_url + "/administrator/EditCompanyPrecense/" + cp_id;
		$.colorbox({href: href_url,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});

	$(".ListCompanyPrecense").on('click',"img[id*=DELETE_COMPANY_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cp_id 	= $(this).parents('tr').data('cp_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={_token : _token , cp_id : cp_id};
	         $.ajax
	        ({
	            url : base_url + "/administrator/RequestDeleteCompanyPrecense",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListCompanyPrecense();
	                      }
	                  });
	              }
	            }
	        });
	});

});