/**
 *
 */

$(function(){
	$('textarea[id*=CM_MEDIA_CAPTION]').ckeditor();
	 $("#BTN_ADD_SI").on('click',function(){
		 var base_url = $('#BASE_URL').val();
		 var cms_page = $('#PAGE_ID').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_ADD_SI_INFO").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/AjaxAddSImage",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/PageContent/" + cms_page;
	              }
	            }
	        });
	    });
})