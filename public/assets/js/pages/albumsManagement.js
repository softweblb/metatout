/**
 *
 */


function displayListAlbums()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	var cms_page = $('input[id=CMS_PAGE]').val();
    if(cms_page != 0)
	{
    	 $.ajax
	    ({
	        url : base_url + "/displayListAlbums",
	        data : {_token : _token , cms_page : cms_page},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	        	$('.ListAlbumsGrid').html(response);
	        }
	    });
	}

}

$(function(){
	displayListAlbums();
	$("#BTN_ADD_ALBUMS").on('click',function(){
	    var cms_page = $('input[id=CMS_PAGE]').val();
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/administrator/AddAlbums/" + cms_page;
	});


	$(".ListAlbumsGrid").on('click',"img[id*=EDIT_ALBUM_]",function(){
	      var ca_id = $(this).parents('tr').data('ca_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/EditAlbums/" + ca_id;
	});

	$(".ListAlbumsGrid").on('click',"img[id*=DELETE_ALBUM_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var ca_id 	= $(this).parents('tr').data('ca_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={ca_id : ca_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/administrator/DeleteAlbum/" + ca_id,
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListAlbums();
	                      }
	                  });
	              }
	            }
	        });
	});

});