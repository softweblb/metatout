/**
 *
 */

function displayListSimpleTextContent()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
    var cms_lang = $('select[id=CMS_LANG]').val();
    $.ajax
    ({
        url : base_url + "/displayListSimpleText",
        data : {_token : _token , cms_lang : cms_lang},
        dataType : "html",
        type : "POST",
        success : function(response){
        	$('.ListSimpleText').html(response);
            $('#BTN_ADD_ST').css('display','');
        }
    });

}

$(function(){
	displayListSimpleTextContent();
	$("#CMS_LANG").on('change',displayListSimpleTextContent);
	$("#BTN_ADD_ST").on('click',function(){
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/AddST";
	});


	$(".ListSimpleText").on('click',"img[id*=EDIT_ST_]",function(){
	      var cc_row_id = $(this).parents('tr').data('cc_row_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/EditST/" + cc_row_id;
	});

	$(".ListSimpleText").on('click',"img[id*=DELETE_ST_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cc_row_id 	= $(this).parents('tr').data('cc_row_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cc_row_id : cc_row_id , _token : _token};
	        console.log(str_params);
	         $.ajax
	        ({
	            url : base_url + "/deleteSimpleText",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListSimpleTextContent();
	                      }
	                  });
	              }
	            }
	        });
	});

});