/**
 *
 */

function displayListRichTextContent()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
	var cms_page = $('input[id=CMS_PAGE]').val();
    var cms_lang = $('select[id=CMS_LANG]').val();
    if(cms_page != 0)
	{
    	 $.ajax
	    ({
	        url : base_url + "/administrator/displayListRichText",
	        data : {_token : _token , cms_page : cms_page , cms_lang : cms_lang},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	        	$('.ListRichTextGrid').html(response);
	            $('#BTN_ADD_RT').css('display','');
	        }
	    });
	}

}

$(function(){
	displayListRichTextContent();
	$("#CMS_LANG").on('change',displayListRichTextContent);
	$("#BTN_ADD_RT").on('click',function(){
	    var cms_page = $('input[id=CMS_PAGE]').val();
		var base_url = $("#BASE_URL").val();
		window.location.href = base_url + "/administrator/AddRT/" + cms_page;
	});


	$(".ListRichTextGrid").on('click',"img[id*=EDIT_RT_]",function(){
		var cms_page = $('input[id=CMS_PAGE]').val();
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/administrator/EditRT/" + cms_page;
	});

	$(".ListRichTextGrid").on('click',"img[id*=DELETE_RT_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cp_id 	= $(this).parents('tr').data('cp_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cp_id : cp_id , _token : _token};
	        console.log(str_params);
	         $.ajax
	        ({
	            url : base_url + "/administrator/deleteRichText",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListPages();
	                      }
	                  });
	              }
	            }
	        });
	});

});