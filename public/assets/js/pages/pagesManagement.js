function displayListPages()
{
	var base_url = $('#BASE_URL').val();
	var _token = $('input[name=_token]').val();
    var cms_menu = $('select[id=CMS_MENU]').val();
    if(cms_menu != 0)
	{
    	 $.ajax
	    ({
	        url : base_url + "/displayListPages",
	        data : {_token : _token , cms_menu : cms_menu},
	        dataType : "html",
	        type : "POST",
	        success : function(response){
	        	$('.ListPagesGrid').html(response);
	            $('#BTN_ADD_PAGE').css('display','');
	        }
	    });
	}

}

$(function(){
	displayListPages();
	$("#CMS_MENU").on('change',displayListPages);
	$("#BTN_ADD_PAGE").on('click',function(){
	    var cms_menu = $('select[id=CMS_MENU]').val();
		var base_url = $("#BASE_URL").val();
	    $.colorbox({href:base_url + "/addPage/" + cms_menu,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});


	$(".ListPagesGrid").on('click',"img[id*=EDIT_PAGE_]",function(){
	      var page_id = $(this).parents('tr').data('cp_id');
	      var base_url = $("#BASE_URL").val();
	      $.colorbox({href:base_url + "/editPage/" + page_id,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});

	$(".ListPagesGrid").on('click',"img[id*=DELETE_PAGE_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var cp_id 	= $(this).parents('tr').data('cp_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={cp_id : cp_id , _token : _token};
	        console.log(str_params);
	         $.ajax
	        ({
	            url : base_url + "/ajaxDeletePage",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListPages();
	                      }
	                  });
	              }
	            }
	        });
	});

});