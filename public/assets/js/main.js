$(function() {

    var $sidebar   = $(".sidebar"), 
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 120
            });
        }
    });
    
    $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
    });

          var slideCount = $('.scroller-content ul li').length;
          var slideWidth = $('.scroller-content ul li').width();
          var slideHeight = $('.scroller-content ul li').height();
          var sliderUlWidth = slideCount * slideWidth;

          $('.scroller-content').css({ width: slideWidth, height: slideHeight });

          $('.scroller-content ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

      $('.scroller-content ul li:last-child').prependTo('.scroller-content ul');

      function moveLeft() {
          $('.scroller-content ul').animate({
              left: + slideWidth
          }, 200, function () {
              $('.scroller-content ul li:last-child').prependTo('.scroller-content ul');
              $('.scroller-content ul').css('left', '');
          });
      };

      function moveRight() {
          $('.scroller-content ul').animate({
              left: - slideWidth
          }, 200, function () {
              $('.scroller-content ul li:first-child').appendTo('.scroller-content ul');
              $('.scroller-content ul').css('left', '');
          });
      };

      $('.scroller-left').click(function () {
          moveLeft();
      });

      $('.scroller-right').click(function () {
          moveRight();
      });

    $(".contact-row button").on("click",function(){
        //alert("The message has been sent successfully!");
    });
    
    var page_name   = location.pathname.split('/').slice(-1)[0];
    var page_width  = $(window).width();

    if( page_width > 768 )
    {
        if( page_name === "announcements" )
        {
            $("footer").css("position","fixed");
            $("footer").css("bottom",0);
        }
    }




    
});