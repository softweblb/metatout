/**
 *
 */


function displayListRoles()
{
	var base_url = $('#BASE_URL').val();
    var _token = $('input[name=_token]').val();

    $.ajax
    ({
        url : base_url + "/displayListRoles",
        data : {_token : _token},
        dataType : "html",
        type : "POST",
        success : function(response){
            $('.ListRoleGirds').html(response);
        }
    });
}

$(function(){
	displayListRoles();


	$("#BTN_ADD_ROLES").on('click',function(){
		var base_url = $("#BASE_URL").val();
      $.colorbox({href:base_url + "/addRole" ,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});
	$(".ListRoleGirds").on('click',"img[id*=EDIT_ROLE_]",function(){
	      var role_id = $(this).parents('tr').data('role_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/editRole/" + role_id;
	});

	$(".ListRoleGirds").on('click',"img[id*=DELETE_ROLE_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var role_id 	= $(this).parents('tr').data('role_id');
	      var base_url 	= $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={role_id : role_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/DeleteRole",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListRoles();
	                      }
	                  });
	              }
	            }
	        });
	});
});