/**
 *
 */

 $(function(){
	 $("#BTN_EDIT_ROLE").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_ROLE").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/ajaxEditRole",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/RolesManagement";
	              }
	            }
	        });
	    });
})