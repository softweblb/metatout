/**
 *
 */

function displayListLanguages()
{
	var base_url = $('#BASE_URL').val();
    var _token = $('input[name=_token]').val();

    $.ajax
    ({
        url : base_url + "/displayListLanguages",
        data : {_token : _token},
        dataType : "html",
        type : "POST",
        success : function(response){
            $('.ListlangGirds').html(response);
        }
    });
}

$(function(){
	displayListLanguages();
	$('.ListlangGirds').on('click','img[id*=EDIT_LANG_]',function(){
	      var lang_id = $(this).parents('tr').data('lang_id');
	      var base_url = $("#BASE_URL").val();
	      var url = base_url + "/editLanguage/" + lang_id;
	      $.colorbox({href: url ,width:"700",height:"500",iframe : true,escKey: false,overlayClose: false});
	});


	$("#BTN_ADD_LANGUAGE").on('click',function(){
		  var url = base_url + "/editLanguage/" + lang_id;
	      $.colorbox({href: url ,width:"700",height:"450",iframe : true,escKey: false,overlayClose: false});
	});
});