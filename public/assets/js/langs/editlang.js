/**
 *
 */

 $(function(){
	 $("#BTN_EDIT_LANG").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_LANGUAGE").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/ajaxEditLang",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	                 window.parent.displayListLanguages();
	                 window.parent.$.colorbox.close();
	              }
	            }
	        });
	    });
})