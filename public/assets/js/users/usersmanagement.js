/**
 *
 */

function displayListUsers()
{
	var base_url = $('#BASE_URL').val();
    var _token = $('input[name=_token]').val();

    $.ajax
    ({
        url : base_url + "/displayListUsers",
        data : {_token : _token},
        dataType : "html",
        type : "POST",
        success : function(response){
            $('.ListUserGirds').html(response);
        }
    });
}

$(function(){
	displayListUsers();


	$("#BTN_ADD_USER").on('click',function(){
		var base_url = $("#BASE_URL").val();
      window.location.href = base_url + "/addUser";
	});
	$(".ListUserGirds").on('click',"img[id*=EDIT_USER_]",function(){
	      var user_id = $(this).parents('tr').data('user_id');
	      var base_url = $("#BASE_URL").val();
	      window.location.href = base_url + "/editUser/" + user_id;
	});

	$(".ListUserGirds").on('click',"img[id*=DELETE_USER_]",function(){
	      if(!confirm('Are you sure do you want to delete ?'))
	           return false;
	      var user_id = $(this).parents('tr').data('user_id');
	      var base_url = $('#BASE_URL').val();
	      var _token = $('input[name=_token]').val();
	        var str_params ={user_id : user_id , _token : _token};
	         $.ajax
	        ({
	            url : base_url + "/DeleteUser",
	            data : str_params,
	            dataType : "Json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  $.alert({
	                      title: 'Alert!',
	                      content: response.error_msg,
	                      confirm: function(){
	                    	  displayListUsers();
	                      }
	                  });
	              }
	            }
	        });
	});
});