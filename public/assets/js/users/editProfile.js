/**
 *
 */

$(function(){
	$('#BTN_EDIT_USER').on('click',function(){
		var frm_profile = $("#FORM_EDIT_PROFILE").serialize();
		var base_url = $('#BASE_URL').val();
		$.ajax
        ({
            url : base_url + "/editProfile",
            data : frm_profile,
            dataType : "json",
            type : "POST",
            success : function(response){
            	 $.alert({
                     title: 'Alert!',
                     content: response.error_msg,
                     confirm: function(){
                         window.parent.$.colorbox.close();
                     }
                 });
            }
        });
	});
});