/**
 *
 */

$(function(){
	$("input[name=u_date_birth]").datetimepicker();
	 $("#BTN_EDIT_USER").on('click',function(){
		 var base_url = $('#BASE_URL').val();
	        var _token = $('input[name=_token]').val();
	        var str_params = $("#FORM_EDIT_USER").serialize();
	        str_params +=  "&_token=" + _token;
	         $.ajax
	        ({
	            url : base_url + "/ajaxEditUser",
	            data : str_params,
	            dataType : "json",
	            type : "POST",
	            success : function(response){
	              if(response.is_error == 0)
	              {
	            	  window.location.href = base_url + "/administrator/UserManagement"
	              }
	            }
	        });
	    });

	    $('#U_DATE_BIRTH').datetimepicker({
	        //language:  'fr',
	        weekStart: 1,
	        todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			forceParse: 0,
	        showMeridian: 1
	    });
})