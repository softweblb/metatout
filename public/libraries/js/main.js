/**
 *
 */


$(function(){


              var jssor_1_options = {
              $AutoPlay: true,
              $AutoPlaySteps: 1,
              $SlideDuration: 160,
              $SlideWidth: 809,
              $SlideSpacing: 3,
              $FillMode: 2,
              $Cols: 1,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1,
                
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$,
                $SpacingX: 1,
                $SpacingY: 1,
                $ChanceToShow: 0,
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("showroom_slider", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end


      $("#makeMeScrollable").smoothDivScroll({
       mousewheelScrolling: "horizontal",
      manualContinuousScrolling: true,
      autoScrollingMode: "onStart"
    });

	/* How to Handle Hashtags */
	jQuery(window).hashchange(function(){
		var hash = location.hash;
		jQuery('a[href='+hash+']').trigger('click');
	});
	jQuery('section.content.hide').hide();
	/* Main Navigation Clicks */
	jQuery('.main-nav ul li a').click(function() {
		var link = jQuery(this).attr('href').substr(1);

		if ( !jQuery('section.content.show, section#' + link).is(':animated') ) {
			jQuery('.main-nav ul li a').removeClass('active'); //remove active
			jQuery('section.content.show').addClass('show').animate({'opacity' : 0}, {queue: false, duration: 1000,
				complete: function() {
					jQuery('section.content.show').hide();
					jQuery('a[href="#'+link+'"]').addClass('active'); // add active
					jQuery('section#' + link).show();
					jQuery('section#' + link).addClass('show').animate({'opacity' : 1}, {queue: false, duration: 1000});
				}
			});
		}
	});


	   $('.HomePagebanner').unslider({
		    speed: 500,               //  The speed to animate each slide (in milliseconds)
		    delay: 10000,              //  The delay between slide animations (in milliseconds)
		    complete: function() {},  //  A function that gets called after every slide animation
		    keys: true,               //  Enable keyboard (left, right) arrow shortcuts
		    dots: true,               //  Display dot navigation
		    fluid: true              //  Support responsive design. May break non-responsive designs
		  });
})


$(function(){
  ResizeScopeRow();
  //$.albumize();


   $('.Itemlist').on('mouseover',function(){
       $(this).attr('src',$(this).data('hover_src'));
   });
   $('.Itemlist').on('mouseout',function(){
       $(this).attr('src',$(this).data('src'));
   });
   $('.ScopeItem').on('mouseover',function(){
       $(this).attr('src',$(this).data('over_src'));
   });
   $('.ScopeItem').on('mouseout',function(){
       $(this).attr('src',$(this).data('src'));
   });

  $(".chameleon").on('click',function(){
               window.open("http://www.chameleonsarl.com",'_blank');
   });

    $(".softweb").on('click',function(){
               window.open("http://www.softweblb.com",'_blank');
   });

   $("#FACEBOOK_BTN").on('click',function(){
               window.open("https://www.facebook.com/Metatout-Prefabs-1032253740123112",'_blank');
   });
   $("#FACEBOOK_BTN").on('mouseover',function(){
              $(this).attr('src', 'libraries/imgs/FACEBOOK-ICON-MOUSE-OVER.png');
   });
   $("#FACEBOOK_BTN").on('mouseout',function(){
               $(this).attr('src', 'libraries/imgs/FACEBOOK-ICON.png');
   });



   $(document).on('click','.ProjectsBtnReadMore',function(){
       number_of_rows     = $('input[name=number_of_rows]').val();
       current_row      = $('input[name=current_row]').val();
	   if( current_row < number_of_rows )
	   {
		   current_row = parseFloat(current_row)+1;
		   $('input[name=current_row]').val(current_row);
		   var project_block_height = $('.ProjectAlbums').height();
		   project_block_height = parseFloat(project_block_height) + 290;
		   $('.ProjectAlbums').css('height' , project_block_height + 'px');
       $('.row'+current_row).css('visibility', 'visible');


       if( current_row == number_of_rows )
       {
          $('.ProjectsBtnReadMore').attr('src' , 'libraries/imgs/btnshowless.png');
          $('.ProjectsBtnReadMore').addClass('ProjectsBtnReadLess');
          $('.ProjectsBtnReadLess').removeClass('ProjectsBtnReadMore');
       }


    }

   });


   $(document).on('click','.ProjectsBtnReadLess',function(){
       var total1 = $('#ProjectAlbums div:last').parent().attr('class');
       var total = total1.substring(3);

       $('#ProjectAlbums').css('height', '590px');
    while(total>2)
    {
         $('#ProjectAlbums .row'+total).css('visibility', 'hidden');
          $(this).attr('src' , 'libraries/imgs/btnshowmore.png');
          $(this).addClass('ProjectsBtnReadMore');
          $(this).removeClass('ProjectsBtnReadLess');
          $('input[name=current_row]').val('2');
         total=total-1;
       }

   });

   $(document).on('click','.ProjectsBtnReadMoreMob',function(){

   $('.hiddenp').css('display', 'block');
   $(this).parent().addClass('hidden');
   $(this).parent().removeClass('visible-xs');
   });



      $(document).on('mouseover', '.BtnReadMore , .ProjectsBtnReadMore', function() {
               $(this).attr('src', 'libraries/imgs/btnshowmore-hover.png');
   });
      $(document).on('mouseout', '.BtnReadMore , .ProjectsBtnReadMore', function(){
               $(this).attr('src', 'libraries/imgs/btnshowmore.png');
   });
      $(document).on('mouseover', '.BtnReadLess, .ProjectsBtnReadLess', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless-hover.png');
   });
      $(document).on('mouseout', '.BtnReadLess, .ProjectsBtnReadLess', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless.png');
   });
      $(document).on('mouseover', '.BtnReadMore2', function(){
               $(this).attr('src', 'libraries/imgs/btnshowmore-hover.png');
   });
      $(document).on('mouseout', '.BtnReadMore2', function(){
               $(this).attr('src', 'libraries/imgs/btnshowmore.png');
   });
      $(document).on('mouseover', '.BtnReadLess2', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless-hover.png');
   });
      $(document).on('mouseout', '.BtnReadLess2', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless.png');
   });
      $(document).on('mouseover', '.BtnReadLessMob', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless-hover.png');
   });
      $(document).on('mouseout', '.BtnReadMoreMob', function(){
               $(this).attr('src', 'libraries/imgs/btnshowmore.png');
   });
      $(document).on('mouseout', '.BtnReadLessMob', function(){
               $(this).attr('src', 'libraries/imgs/btnshowless-hover.png');
   });
      $(document).on('mouseover', '.BtnReadMoreMob', function(){
               $(this).attr('src', 'libraries/imgs/btnshowmore-hover.png');
   });   //
   $('.EagleLogo').on('click',function(){
      $(document).scrollTo( 0, 800,{onAfter:function(){
             $('.AboutText').slideUp();
       } } );
   });


        if ($(window).width() <900) {

              $('#map_contact').attr('src','themes/images/ESS-MAP-MOBILE.jpg');
        } else {

        }

$(window).on('resize', function() {

        if ($(window).width() <900) {

                 $('#map_contact').attr('src','themes/images/ESS-MAP-MOBILE.jpg');
        } else {

                 $('#map_contact').attr('src','themes/images/ESS-MAP.jpg');
        }

});





   $(window).on('resize',function(){
      //Menu
     ResizeScopeRow();
     var window_width = $(window).width();
     var menu_width = $('.Menu ul').width();
     var item_width = menu_width / 6;
    // $('.LiItem').width(item_width + ' px !important');
     $('.LiItem').each(function(){
       $(this).width(item_width + ' px');
     });

    /** $('.ScopeMainItem').each(function(){
       var window_width = $(window).width();
       var width_item = window_width/6;
       $(this).css('width',220);
       $(this).css('width',220);
       $(this).css('height',220);
     });*/
     $('.ProjectMainBox').each(function(){
       var window_width = $(window).width();
       var width_item = window_width/6;
       $(this).css('width',width_item);
       $(this).css('width',width_item);
       $(this).css('height',width_item);
     });

   });
   /** $('.ScopeMainItem').each(function(){
     var window_width = $(window).width();
     var width_item = window_width/6;
     $(this).css('width',220);
     $(this).css('width',220);
     $(this).css('height',220);
   });*/
   $('.ProjectMainBox').each(function(){
     var window_width = $(window).width();
     var width_item = window_width/6;
     $(this).css('width',width_item);
     $(this).css('width',width_item);
     $(this).css('height',width_item);
   });
   $('.MenuItem').on('click',function(){
       var menu = $(this).data('menu');
        switch(menu)
        {
          case "homepage":
                {
                    $(document).scrollTo( 0, 800,{onAfter:function(){
                    } } );
                }
            break;
            case "aboutus":
                {
                                 var pos = $($('.AboutUsTextSection')).offset();
                     $(document).scrollTo( pos.top - 109, 800, {onAfter:function(){
                              $('.AboutText').slideToggle();
                              $('.navbar-toggle').click();
                    } } );

                }
            break;
            case "scope":
                {
                    var pos = $($('.scope')).offset();
                     $(document).scrollTo( pos.top - 109, 800, {onAfter:function(){
                              $('.AboutText').slideToggle();
                              $('.navbar-toggle').click();
                    } } );
                }
            break;
            case "projects":
                {
                    if($(window).width()<751){var pos = $($('#MobileAlbums')).offset();}else{var pos = $($('.ProjectAlbums')).offset();}
                     $(document).scrollTo( pos.top - 110, 800, {onAfter:function(){
                              $('.AboutText').slideToggle();
                              $('.navbar-toggle').click();
                    } } );
                }
            break;
            case "clients":
                {
                    var pos = $('.ClientsSection').offset();
                    $(document).scrollTo( pos.top - 112, 800, {onAfter:function(){
                        $('.AboutText').slideToggle();
                        $('.navbar-toggle').click();
                    } } );
                }
            break;
            case "contactus":
                {
                    var pos = $('.InformationSection').offset();
                     $(document).scrollTo( pos.top - 112, 800, {onAfter:function(){
                            $('.AboutText').slideToggle();
                            $('.navbar-toggle').click();
                    } } );
                }
            break;
        }
   });
  // calculate main photo height
  var menu_height = $(".Menu").height();
  var document_height = $(window).height();
  //$('#MAIN_IMAGE').height(document_height);
  //$('.AboutUsSection').height(document_height - menu_height);
  //$('.ScopeSection').height(document_height);
  $('.MainPhoto').each(function(){
     var window_width = $(window).width();
     var window_height = $(window).height();
     $(this).width(window_width + "px");
     $(this).css('left' , "0px");
  })

    var screen_width = screen.width;
     $(document).ready(function(){

       number_of_rows     = $('input[name=number_of_rows]').val();
       current_row      = $('input[name=current_row]').val();


    });

     $(document).on('click', '.BtnReadMore', function(){
	      $("#Textcontainer").attr('style','');
	      $(this).attr({"src": "libraries/imgs/btnshowless.png", "style": "position: relative;" });
	      $(this).removeClass('BtnReadMore');
	      $(this).addClass('BtnReadLess');
        $('.AboutUsTextSection').css('top' , '0px');
    });

    $(document).on('click', '.BtnReadLess', function() {
      $("#Textcontainer").attr('style','height: 370px;');
      $(this).attr({"src": "libraries/imgs/btnshowmore.png", "style": "top: -37px;position: relative;" });
      $(this).removeClass('BtnReadLess');
      $(this).addClass('BtnReadMore');
    });


var y=3;
    $(document).on('click', '.BtnReadMore2', function(){
      var r= '.row'+(y);
      var proid = $(r).attr('id');
      var x=$("#ProjectAlbums").height();
      var t='row'+(y);


      $("#ProjectAlbums").attr('style','height:'+(x + 279));
      $(".tbo").attr('style', 'display: block');
      $(r).attr('style','display: block');
      if(proid=='last')
      {
        $(".BtnReadMore2").attr('src', 'libraries/imgs/btnshowless.png');
        $(this).addClass('BtnReadLess2');
        $(this).removeClass('BtnReadMore2');
         }
       else{
          y++;
       }
      });

    $(document).on('click', '.BtnReadLess2', function() {

      $("#ProjectAlbums").attr('style','height: 616px;');
      $(".tbo").attr('style','display: none');
      $(this).attr({"src": "libraries/imgs/btnshowmore.png", "style": "top: -37px;position: relative;" });
      $(this).addClass('BtnReadMore2');
      $(this).removeClass('BtnReadLess2');
    });


});

    $(document).on('click', '.BtnReadMoreMob', function() {

      $("#ProjMobLR").attr('style','display: block');
      $(this).attr({"src": "themes/images/SHOWLESS.png", "style": "top: -37px;position: relative;" });
      $(this).addClass('BtnReadLessMob');
      $(this).removeClass('BtnReadMoreMob');
    });
    $(document).on('click', '.BtnReadLessMob', function() {

      $("#ProjMobLR").attr('style','display: none');
      $(this).attr({"src": "themes/images/SHOWMORE.png", "style": "top: -37px;position: relative;" });
      $(this).addClass('BtnReadMoreMob');
      $(this).removeClass('BtnReadLessMob');
    });


function ResizeScopeRow()
{
 $('.ScopeSectionTitle').each(function(n){
       var window_width = $(window).width();
       var width_item = window_width/6;
      // $(this).next().width(width_item);

	  	var width = $(this).next().width();
	  	$(this).width(width);
	  	$(this).css({width : width});
     });

}

$(document).on('click', 'div[id*=albumgal]', function(){
  var str=$(this).attr('id');
var lastChar=str.charAt(str.length - 1);

	$('body').scrollTop(0);
    $('html, body').css({
		    'overflow': 'hidden',
		    'height': '100%'
		});
		$( '#gallery' ).jGallery({
		  height: '97%',
		  autostart: true,
		  autostartAtAlbum: lastChar,
		  canClose: true
		});
		$('#overlay').css({
		    'opacity': '0.8',
		    'z-index': '2333'
		});
     $('#loading').css({
		    'z-index': '3333'
		});
});



    $(document).on('click', '.jgallery-close', function(){


       $('html, body').css({
    'overflow': 'visible',
    'height': '100%'
});
      $('#overlay').css({
    'opacity': '0',
    'z-index': '0'
});
      $('#loading').css({

    'z-index': '0'
});
     });

    $(document).on('click', '.jgallery-close', function(){
    $( '#gallery' ).jGallery().destroy();

    if($(document).width() > 750)
    {
    var pos = $($('#ProjectAlbums')).offset();
    $.scrollTo(pos.top-100, 0);
     }
     else{
           var pos = $($('#MobileAlbums')).offset();
           $.scrollTo(pos.top-50, 0);
     }
  });


$(document).on('click', '#overlay', function(){

$(".jgallery-close").click();


});



        // hit escape to close the overlay
        $(document).keyup(function(e) {
            if (e.which === 27) {
                $( '#gallery' ).jGallery().destroy();
                $('html, body').css({
                   'overflow': 'visible',
                   'height': '100%'
                     });
               $('#overlay').css({
                   'opacity': '0',
                   'z-index': '0'
                     });
               $('#loading').css({

                   'z-index': '0'
                     });
                var pos = $('#ProjectAlbums').offset();
                $.scrollTo(pos.top-100, 0);

            }
        });


$(document).on('scroll', function(){

if($(document).scrollTop()>=2100)
{

  $('#mobmapcont').fadeOut(2500);
}
});


(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 6;
                } else if (width >= 350) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', '160px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            });
    });
})(jQuery);