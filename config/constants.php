<?php
/***********************************************************
constants.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 27, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

return [
    "PROFILE_IMAGE_PATH" => "assets/media/profile/",
    "COMPANY_LOGO_IMAGE_PATH" => "assets/media/company/",
    "SINGLE_IMAGE_PATH" => "assets/media/singleimagepath/",
    "ALBUMS_PATH" => "assets/media/albums/",
    "CALENDER_ADMIN_DATA_PATH" => "assets/resources/calendar/",
];

?>