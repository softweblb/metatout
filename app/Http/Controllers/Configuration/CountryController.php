<?php
/***********************************************************
CountryController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 23, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Http\Controllers\Configuration;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;
use App\Models\SYSTEM\SYSTEMCountries;
use Illuminate\Support\Facades\Hash;


class CountryController extends Controller
{


    public function index()
    {
        return view('system.countries',array());
    }

    /**
     * Display for for add a new country
     *
     * @author Moe Mantach
     * @access public
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function AddNewCountryForm()
    {
        return view('system.addcountry',array());
    }


    /**
     * Display form
     * @param unknown $id
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function EditCountryForm( $id )
    {
      $countries = SYSTEMCountries::find( $id );
       return view('system.editcountry',array("countries" =>$countries , 'country_id' => $id));
    }

    /**
     * Display list of roles saved in the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function ajaxListCountries()
    {
        $countries = SYSTEMCountries::all();
        $data = array(
            "countries" => $countries
        );
        return view("system.displaylistcountries",$data);
    }


    /**
     *Add new role to the database with title and description
     *
     *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
    public function ajaxAddCountry()
    {
        $code       = $_POST['code'];
        $name       = $_POST['name'];

        $fields_array = array(
            'code' => $code,
            'name' => $name
        );
        $id = DB::table('country')->insertGetId($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";
        $result_array['id']         = $id;

        return json_encode($result_array);
    }

    public function ajaxEditCountry()
    {
        $name           = $_POST['name'];
        $id             = $_POST['id'];
        $code           = $_POST['code'];

        $fields_array = array(
            'name' => $name,
            'code' => $code
        );
        DB::table('country')->where('id', $id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    /**
     * function to delete role from the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function DeleteCountry($id)
    {
        SYSTEMCountries::deleteCountry( $id );

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

}
