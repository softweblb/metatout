<?php
/***********************************************************
MediaController.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 4, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
all functionality and requests related to the
***********************************************************/

namespace App\Http\Controllers\Media;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Models\Media\WEBMedia;
use App\Models\SYSTEM\SYSTEMLanguages;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;



class MediaController extends Controller
{

    /**
     * Get id of media and delete it from the database
     *
     * @author Moe Mantach
     * @param Integer $id
     */
    public function DeleteMedia( $id )
    {
        WEBMedia::deleteRow($id);
    }
}