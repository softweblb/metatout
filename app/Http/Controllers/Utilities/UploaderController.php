<?php
/************************************************************
UploaderController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 27, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controller for upload
************************************************************/


namespace App\Http\Controllers\Utilities;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Config;
use Redirect;
use File;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class UploaderController extends Controller
{
    public function Index( $key )
    {
        switch( $key )
        {
            case "Event":
                {
                    $result_array = array();

                    $row_id    = $_POST['row_id'];
                    $file_name = $_FILES['files']['name'];
                    $file_type = $_FILES['files']['type'];
                    $file_tmp  = $_FILES['files']['tmp_name'];

                    $cms_event_display  = DB::table('cms_events')->where('ce_row_id', $row_id)->where('ce_is_deleted', 0)->get();


                    $base_dir = $cms_event_display[0]->ce_media_base_dir;

                    if($base_dir == '')
                    {
                        $base_dir = date('Y/m/d/') . $row_id. "/";
                    }

                    $directory = public_path(). "/" . Config::get( 'constants.EVENT_PATH') . $base_dir;
                    $image_url = url(). "/" . Config::get( 'constants.EVENT_PATH') . $base_dir;

                    if(!is_dir($directory))
                    {
                        $result = File::makeDirectory($directory, 0777, true);
                    }

                    $file_info = explode(".", $file_name);

                    $extention  = $file_info[count($file_info) - 1];
                    $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 999999);

                    $file_path = $directory . $file_name . "." . $extention;
                    $image_url = $image_url . $file_name . "." . $extention;
                    if(move_uploaded_file($file_tmp, $file_path))
                    {
                        $fields_array = array(
                            'ce_media_base_dir' => $base_dir,
                            'ce_media_file_name' => $file_name,
                            'ce_media_file_extention' => $extention
                        );
                        DB::table('cms_events')->where('ce_row_id', $row_id)->update($fields_array);
                    }


                    $result_array = array();
                    $result_array['is_error']   =  0;
                    $result_array['error_msg']  =  "Operation Complete Successfuly";
                    $result_array['image_url']  =  $image_url;
                    echo  json_encode($result_array);
                }
            break;
            case "AImages":
            case "SImage":
                {
                    $result_array = array();


                    $page_id     = isset( $_POST['page_id']) ? $_POST['page_id'] : 0;
                    $album_id    = isset( $_POST['album_id']) ? $_POST['album_id'] : 0;
                    $cm_id       = isset( $_POST['cm_id']) ? $_POST['cm_id'] : 0;
                    if(is_array($_FILES['files']['name']))
                    {
                        $file_name = $_FILES['files']['name'][0];
                        $file_type = $_FILES['files']['type'][0];
                        $file_tmp  = $_FILES['files']['tmp_name'][0];
                        $file_size  = $_FILES['files']['size'][0];
                    }
                    else {
                        $file_name = $_FILES['files']['name'];
                        $file_type = $_FILES['files']['type'];
                        $file_tmp  = $_FILES['files']['tmp_name'];
                        $file_size  = $_FILES['files']['size'];
                    }

                    if($page_id > 0)
                        $cms_media_display  = DB::table('cms_media')->where('cm_row_id', $page_id)->where('cm_media_is_active', 1)->get();
                    else
                        $cms_media_display  = DB::table('cms_media')->where('cm_id', $cm_id)->where('cm_media_is_active', 1)->get();

                    if(count($cms_media_display) > 0)
                        $base_dir = $cms_media_display[0]->cm_media_base_dir;
                    else
                        $base_dir = "";

                    if($base_dir == '')
                    {
                        $base_dir = date('Y/m/d/') . $page_id. "/";
                    }

                    if($key == 'SImage')
                    {
                        $directory = public_path(). "/" . Config::get( 'constants.SINGLE_IMAGE_PATH') . $base_dir;
                        $image_url = url(). "/" . Config::get( 'constants.SINGLE_IMAGE_PATH') . $base_dir;
                    }
                    else
                    {
                        $directory = public_path(). "/" . Config::get( 'constants.ALBUMS_PATH') . $base_dir;
                        $image_url = url(). "/" . Config::get( 'constants.ALBUMS_PATH') . $base_dir;
                    }

                    if(!is_dir($directory))
                    {
                        $result = File::makeDirectory($directory, 0777, true);
                    }

                    $file_info = explode(".", $file_name);

                    $extention  = $file_info[count($file_info) - 1];
                    $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 8888888);

                    $file_path = $directory . $file_name . "." . $extention;
                    $image_url = $image_url . $file_name . "." . $extention;

                    if(move_uploaded_file($file_tmp, $file_path))
                    {
                        switch($key)
                        {
                            case "SImage":
                            {
                                $fields_array = array(
                                    'cm_media_base_dir' => $base_dir,
                                    'cm_media_file_name' => $file_name,
                                    'cm_media_file_extention' => $extention
                                );
                                $row_id = $_POST['row_id'];
                                $res =  DB::table('cms_media')->where('cm_row_id',$row_id)->update($fields_array);
                                $result_array['is_error']   = 0;
                                $result_array['error_msg']  = "operation Complete Successfuly";
                                $result_array['image_url']  = $image_url;
                                $result_array['cm_id']      = $cm_id;
                            }
                            break;
                            case "AImages":
                            {
                                if($cm_id > 0)
                                {
                                    $fields_array = array(
                                        'cm_media_base_dir' => $base_dir,
                                        'cm_media_file_name' => $file_name,
                                        'cm_media_file_extention' => $extention
                                    );
                                    DB::table('cms_media')->where('cm_id', $cm_id)->update($fields_array);
                                }
                                else
                                {
                                    $fields_array = array(
                                        'fk_album_id' => $album_id,
                                        'fk_page_id' => $page_id,
                                        'cm_media_is_active' => 1,
                                        'cm_date_creation' => date("Y-m-d H:i:s"),
                                        'cm_media_base_dir' => $base_dir,
                                        'cm_media_file_name' => $file_name,
                                        'cm_media_file_extention' => $extention
                                    );

                                  $cm_id =   DB::table('cms_media')->insertGetId($fields_array);
                                }

                                $result_array['files'][0]['deleteType']     = 'DELETE';
                                $result_array['files'][0]['deleteUrl']      = url() . '/administrator/DeleteMedia/' . $cm_id;
                                $result_array['files'][0]['name']           = $file_name;
                                $result_array['files'][0]['cm_id']          = $cm_id;
                                $result_array['files'][0]['size']           = $file_size;
                                $result_array['files'][0]['thumbnailUrl']   = $image_url;
                                $result_array['files'][0]['type']           = $file_type;
                                $result_array['files'][0]['url']            = $image_url;
                            }
                            break;
                        }

                    }



                    echo  json_encode($result_array);
                }
            break;
            case "companylogo":
                {
                    $result_array = array();

                    $user_id    = session('user_id');

                    $file_name = $_FILES['files']['name'];
                    $file_type = $_FILES['files']['type'];
                    $file_tmp  = $_FILES['files']['tmp_name'];

                    $sys_company_details  = DB::table('sys_company_details')->get();


                    $base_dir = $sys_company_details[0]->cd_company_logo_base_src;

                    if($base_dir == '')
                    {
                        $base_dir = $user_id. "/";
                    }

                    $directory = public_path(). "/" . Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $base_dir;
                    $image_url = url(). "/" . Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $base_dir;

                    if(!is_dir($directory))
                    {
                        $result = File::makeDirectory($directory, 0777, true);
                    }

                    $file_info = explode(".", $file_name);

                    $extention  = $file_info[count($file_info) - 1];
                    $file_name  = md5(date("Y-m-d H:i:s") ). "_" . date("YmdHis") . "_" . rand(0, 8888888);

                    $file_path = $directory . $file_name . "." . $extention;
                    $image_url = $image_url . $file_name . "." . $extention;
                    if(move_uploaded_file($file_tmp, $file_path))
                    {
                        $fields_array = array(
                            'cd_company_logo_base_src' => $base_dir,
                            'cd_company_logo_filename' => $file_name,
                            'cd_company_logo_extention' => $extention
                        );
                        DB::table('sys_company_details')->where('cd_id', '1')->update($fields_array);
                    }

                    $result_array = array();
                    $result_array['is_error']  = 0;
                    $result_array['error_msg'] = "operation Complete Successfuly";
                    $result_array['image_url'] = $image_url;
                    echo  json_encode($result_array);
                }
                break;
        }
    }
}