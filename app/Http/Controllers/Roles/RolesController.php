<?php
/************************************************************
RolesController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Roles controle contain all functionality related to role table
************************************************************/

namespace App\Http\Controllers\Roles;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class RolesController extends Controller
{
    /**
     * Display add role form to add new role to the database
     *
     * @author Moe Mantach
     * @access pubic
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function AddRoleForm()
    {
        return view('roles.addrole',array());
    }

    public function EditRoleForm( $id )
    {
        $roles = DB::table('roles')->where('role_id', $id)->orderBy('role_id', 'asc')->get();
        return view('roles.editrole',array("roles" =>$roles , 'role_id' => $id));
    }

    /**
     * Display list of roles saved in the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function ajaxListRoles()
    {
        $roles = DB::table('roles')->where('role_is_active', 1)->where('role_is_deleted', 0)->orderBy('role_name', 'ASC')->get();

        $data = array(
            "roles" => $roles
        );
        return view("roles.displaylist",$data);
    }


    /**
     *Add new role to the database with title and description
     *
     *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
    public function ajaxAddRole()
    {
        $role_name          = $_POST['role_name'];
        $role_description   = $_POST['role_description'];

        $fields_array = array(
            'role_name' => $role_name,
            'role_description' => $role_description,
            'role_is_active' => 1
        );
        $id = DB::table('roles')->insertGetId($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";
        $result_array['id']         = $id;

        return json_encode($result_array);
    }

    public function ajaxEditRole()
    {
        $role_name          = $_POST['role_name'];
        $role_id            = $_POST['role_id'];
        $role_description   = $_POST['role_description'];

        $fields_array = array(
            'role_name' => $role_name,
            'role_description' => $role_description
        );
        DB::table('roles')->where('role_id', $role_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    /**
     * function to delete role from the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function ajaxDeleteRole()
    {
        $role_id            = $_POST['role_id'];

        $fields_array = array(
            "role_is_deleted" => 1
        );
        DB::table('roles')->where('role_id', $role_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

}
