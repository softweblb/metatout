<?php
/***********************************************************
FreeTextController.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 2, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controller to manage the free text section
***********************************************************/



namespace App\Http\Controllers\CMS;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Models\CMS\CMSFreeText;
use App\Models\SYSTEM\SYSTEMLanguages;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;



class FreeTextController extends Controller
{


    public function index()
    {
        $listLanguages      = SYSTEMLanguages::getAll();

        $data = array(
            "listLanguages" => $listLanguages
        );
        return view('pages.freeText',$data);
    }

    /**
     * Display  list of Free Text Added to the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return View
     */
    public function displayListFreeText( Request $request )
    {
        $cms_lang       = $request->input('cms_lang');


        $cms_free_text = CMSFreeText::getFreeTextByLang($cms_lang);

        $data = array(
            "cms_free_text" => $cms_free_text
        );
        return view("pages.displaylistfreeText",$data);
    }


    /**
     * display add page form in the popup
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array Html
     */
    public function AddFreeTextForm()
    {
        $listLanguages      = SYSTEMLanguages::getAll();
        $data = array(
            "listLanguages" => $listLanguages
        );
        return view("pages.addFreeTextForm",$data);
    }



    public function EditFreeTextForm($id)
    {
        $cms_free_text  = CMSFreeText::GetRowTextByLang($id);
        $listLanguages  = SYSTEMLanguages::getAll();
        $data = array(
            "cms_free_text" => $cms_free_text,
            "row_id" => $id,
            "listLanguages" => $listLanguages
        );
        return view("pages.editFreeTextForm",$data);
    }


    /**
     *Add new Free Text Row
     *
     *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
    public function ajaxAddFreeText( Request $request )
    {
        $listLanguages      = SYSTEMLanguages::getAll();
        $ft_row_id          =  CMSFreeText::getNextRowId();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $ft_title_text           = $request->input('ft_title_text_' . $listLanguages[$i]->sl_language_code);
            $ft_text_content         = $request->input('ft_text_content_' . $listLanguages[$i]->sl_language_code);
            $fields_array = array(
                'ft_row_id' => $ft_row_id,
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'ft_title_text' => $ft_title_text,
                'ft_text_content' => $ft_text_content
            );
            CMSFreeText::saveInfo($fields_array);
        }



        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
    }


    public function ajaxEditFreeText( Request $request )
    {
        $listLanguages      = SYSTEMLanguages::getAll();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $ft_title_text           = $request->input('ft_title_text_' . $listLanguages[$i]->sl_language_code);
            $ft_text_content         =  $request->input('ft_text_content_' . $listLanguages[$i]->sl_language_code);
            $ft_id                   =  $request->input('ft_id_' . $listLanguages[$i]->sl_language_code);
            $fields_array = array(
                'id' => $ft_id,
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'ft_title_text' => $ft_title_text,
                'ft_text_content' => $ft_text_content
            );
            CMSFreeText::saveInfo($fields_array);
        }


        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
    }


    /**
     * function to delete role from the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function ajaxDeleteFreeText(Request $request)
    {
        $ft_row_id            = $request->input('ft_row_id');

        CMSFreeText::deleteRow($ft_row_id);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
    }
}