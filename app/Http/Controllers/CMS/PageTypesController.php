<?php
/***********************************************************
PageTypesController.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 28, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/
namespace App\Http\Controllers\CMS;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cookie;
use Session;
use Config;
use Redirect;
use File;
use DB;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSPageTypes;
use Illuminate\Support\Facades\Hash;


class PageTypesController extends Controller
{
    /**
     * Display list of PageTypes
     *
     * @author Moe Mantach
     * @access public
     *
     */
    public function index()
    {
        $list_page_types = CMSPageTypes::all();


        return view('cms.pageconfiguration',array( 'list_page_types' => $list_page_types ));
    }


    public function SaveInformation(Request $request)
    {
        $list_page_types = CMSPageTypes::getAll();


        for ($i = 0; $i < count($list_page_types); $i++)
        {
            $input['id'] = $list_page_types[$i]->cs_id;
            $input['cs_is_active'] = $request->input('ck_is_active_' . $list_page_types[$i]->cs_id);
            $input['cs_is_active'] = strlen(trim($input['cs_is_active'])) > 0 ? 1 : 0;
            CMSPageTypes::SavePageTypeInformation($input);
        }

        $result_array = array();
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = 'Operation Complete Succesfully';

        return response()->json($result_array);
    }
}