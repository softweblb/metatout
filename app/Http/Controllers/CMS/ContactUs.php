<?php
/***********************************************************
ContactUs.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controller Contact us
***********************************************************/

namespace App\Http\Controllers\CMS;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Models\CMS\CMSCompanyPrecense;
use App\Models\System\SYSTEMCompanyDetails;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;
use App\Models\CMSPageType;



class ContactUs extends Controller
{

    /**
     * Display Contactus page management Where we can add/edit and delete precense to the google Map
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Ambigous <\Illuminate\View\View, mixed, \Illuminate\Foundation\Application, \Illuminate\Container\static>
     */
    public function DisplayCompanyPrecense()
    {

        $data = array();
        return view('cms.companyprecense',$data);
    }

    /**
     * Display  list of company precense points saved in the database
     *
     * @author Moe Mantach
     * @access public
     *
     * @return View
     */
    public function ListCompanyPrecense( Request $request )
    {

         $lstCompanyPrecense    = CMSCompanyPrecense::getAll();

        $data = array(
            "lstCompanyPrecense" => $lstCompanyPrecense
        );
        return view("cms.displaycompanyprecense",$data);
    }


    /**
     * display add page form in the popup
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array Html
     */
    public function AddCompanyPrecenseForm()
    {
        $data = array();
        return view("cms.addcompanyprecenseform",$data);
    }



    public function EditCompanyPrecenseForm($id)
    {
        $cms_company_precense  = CMSCompanyPrecense::find($id);
        $data = array(
            "cms_company_precense" => $cms_company_precense
        );
        return view("cms.editcompanyprecenseform",$data);
    }


    /**
     *Add new Free Text Row
     *
     *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
    public function AjaxAddCompanyPrecense( Request $request )
    {

        $cp_company_precense_name       = $request->input('cp_company_precense_name');
        $cp_latitude                    = $request->input('cp_latitude');
        $cp_longitude                   = $request->input('cp_longitude');
        $fields_array = array(
            'cp_company_precense_name' => $cp_company_precense_name,
            'cp_latitude' => $cp_latitude,
            'cp_longitude' => $cp_longitude
        );
        CMSCompanyPrecense::saveInfo($fields_array);



        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
    }


    public function AjaxEditCompanyPrecense( Request $request )
    {
         $cp_company_precense_name  = $request->input('cp_company_precense_name');
        $cp_latitude                = $request->input('cp_latitude');
        $cp_longitude               = $request->input('cp_longitude');
        $cp_id                      = $request->input('cp_id');
        $fields_array = array(
            'id' => $cp_id,
            'cp_company_precense_name' => $cp_company_precense_name,
            'cp_latitude' => $cp_latitude,
            'cp_longitude' => $cp_longitude
        );
        CMSCompanyPrecense::saveInfo($fields_array);



        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
    }


        /**
        * function to delete role from the database
        *
     * @author Moe Mantach
     * @access public
        *
        * @return Array $result_array
        */
        public function DeleteCompanyPrecense(Request $request)
        {
            $cp_id            = $request->input('cp_id');

            CMSCompanyPrecense::deleteRow($cp_id);

            $result_array               = array();
            $result_array['is_error']   = 0;
            $result_array['error_msg']  = "Operation Complete Successfully";

            return response()->json($result_array);
        }
}