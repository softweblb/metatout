<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CMSPages;
use App\Models\CMSRichTextContent;
use App\Models\CMSMedia;
use Illuminate\Support\Facades\Config;
use App\Models\CMSEvents;
use App\Models\CMSAlbums;
use App\Models\CMSTextContent;
use App\Models\SYSTEM\SYSTEMCompanyDetails;
use \Input;
use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Mail;
use App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public static function format_request_data(Request $request, $LangID = null, $PageName = null)
    {
        $return_array                   = array();
        $locale                         = ($LangID == null ? "fr" : $LangID);
        App::setLocale($locale);

        $return_array['lang_id']        = ($LangID == "fr" || $LangID == null ? 2 : 1);

        $page_array = explode("/", $request->path());

        if( $LangID == null )
        {
            $return_array['page_name'] = $page_array[0];
        }
        else
        {
            $return_array['page_name'] = $page_array[1];
        }
        if( $return_array['page_name'] == "admission" )
        {
            if( $PageName == null )
            {
                $return_array['child_pages_first']    = CMSPages::whereFkMenuId(4)->first();
            }
            else
            {
                $return_array['child_pages_first']    = CMSPages::whereFkMenuId(4)->whereCpPageTitle($PageName)->first();
            }
        }

        $return_array['cms_pages']      = CMSPages::select('cp_page_title')->where("cp_is_active","=",1)->whereFkMenuId(1)->get();
        $return_array['footer_pages']   = CMSPages::select('cp_page_title')->where("cp_is_active","=",1)->whereRaw("fk_menu_id IN(1,2)")->get();

        return $return_array;
    }

    public function index(Request $request, $LangID = null)
    {


        $sys_company_details = SYSTEMCompanyDetails::find(1);
        dd($sys_company_details); exit;

        $request_data           = self::format_request_data($request,$LangID);
        $index_controller       = new IndexController;
        $event_past_data        = CMSEvents::whereFkPageId(15)->whereFkLangId($request_data['lang_id'])->whereCeIsActive(1)->whereRaw("ce_date_to < NOW()")->first();
        $event_future_data      = CMSEvents::whereFkPageId(15)->whereFkLangId($request_data['lang_id'])->whereCeIsActive(1)->whereRaw("ce_date_to > NOW()")->first();

        $page_data              = CMSPages::whereCpPageTitle("distinction")->whereCpIsActive(1)->first();
        $scroller_content       = CMSRichTextContent::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->get();
        $scroller_array = [
            "page_name"         => "distinction",
            "language"          => ($LangID == null ? "fr" : $LangID),
            "scroller_content"  => $scroller_content,
            "items_count"       => $scroller_content->count()
        ];

        $distinction_data = $index_controller->renderScroller($scroller_array);

        $page_data              = CMSPages::whereCpPageTitle("testimonial")->whereCpIsActive(1)->first();
        $scroller_content       = CMSRichTextContent::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->get();
        $scroller_array = [
            "page_name"         => "testimonial",
            "language"          => ($LangID == null ? "fr" : $LangID),
            "scroller_content"  => $scroller_content,
            "items_count"       => $scroller_content->count()
        ];

        $testimonial_data = $index_controller->renderScroller($scroller_array);

        $director_message = CMSTextContent::whereFkLangId($request_data['lang_id'])->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages" => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name" => "home",
            "event_past_data"    => $event_past_data,
            "event_future_data"    => $event_future_data,
            "distinction_data" => $distinction_data,
            "testimonial_data" => $testimonial_data,
            "director_message"  => $director_message
        ];

        return view("frontend.main",$data_array);
    }

    public function academicLife(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_data      = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $page_content   = CMSMedia::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->whereCmMediaIsActive(1)->get();

        $index_controller   = new IndexController;

        $render_content = "";
        foreach ($page_content as $key => $content)
        {
           $key_direction   = $key % 2 ;
           $direction       = ($key_direction == 0 ? "left" : "right");
           $page_data_array = [
             "direction"    => $direction,
             "title"        => $content->cm_media_title,
             "caption"      => $content->cm_media_caption,
             "thumb_path"   => Config::get('constants.SINGLE_IMAGE_PATH') . "/" . $content->cm_media_base_dir,
             "filename"     => $content->cm_media_file_name,
             "extension"    => $content->cm_media_file_extention
           ];

           $render_content .= $index_controller->renderContent($page_data_array);
        }

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_content"  => $page_content,
            "content"       => $render_content,
            "page_name"     => $request_data['page_name'],

        ];

        return view("frontend.academic-life",$data_array);
    }


    public function alumni(Request $request, $LangID = null)
    {
        $request_data       = self::format_request_data($request,$LangID);
        $page_data          = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $page_content       = CMSMedia::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->whereCmMediaIsActive(1)->get();
        $index_controller   = new IndexController;

        $scroller_data          = CMSPages::whereCpPageTitle("testimonial")->whereCpIsActive(1)->first();
        $scroller_content       = CMSRichTextContent::whereFkPageId($scroller_data->cp_id)->whereFkLangId($request_data['lang_id'])->get();
        $scroller_array = [
            "page_name"         => "testimonial",
            "language"          => ($LangID == null ? "fr" : $LangID),
            "scroller_content"  => $scroller_content,
            "items_count"       => $scroller_content->count()
        ];

        $testimonial_data = $index_controller->renderScroller($scroller_array);

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_content"  => $page_content,
            "page_name"     => $request_data['page_name'],
            "testimonial_data" => $testimonial_data,
            "items_count"     => $page_content->count()
        ];

        return view("frontend.alumni",$data_array);
    }

    public function ajaxEvents()
    {
        $return_array   = array();

        $event_type     = Input::get('type');
        $skip           = Input::get('skip');
        $language       = Input::get('language');
        $date_condition = ( $event_type == "past" ? "ce_date_to < NOW()" : "ce_date_to > NOW()" );
        $lang_id        = ($language == "en" || $language == null ? 1 : 2);

        $event_data     = CMSEvents::whereFkPageId(15)->whereFkLangId($lang_id)->whereCeIsActive(1)->whereRaw($date_condition)->skip($skip)->take(1)->get();

        if( $event_data->count() > 0 )
        {
            $index_controller       = new IndexController;
            $data_array             = [ "event_data" => $event_data->first()];
            $return_array['html']   = $index_controller->renderEvent($data_array);
        }
        else
        {
            $return_array['html'] = null;
        }

        return Response::json($return_array);
    }

    public function events(Request $request, $LangID = null)
    {
        $request_data           = self::format_request_data($request,$LangID);
        $event_past_data        = CMSEvents::whereFkPageId(15)->whereFkLangId($request_data['lang_id'])->whereCeIsActive(1)->whereRaw("ce_date_to < NOW()")->first();
        $event_future_data      = CMSEvents::whereFkPageId(15)->whereFkLangId($request_data['lang_id'])->whereCeIsActive(1)->whereRaw("ce_date_to > NOW()")->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "event_past_data"  => $event_past_data,
            "event_future_data"  => $event_future_data,
            "page_name"     => $request_data['page_name'],

        ];

        return view("frontend.events",$data_array);
    }

    public function sitemap(Request $request, $LangID = null)
    {
        $request_data           = self::format_request_data($request,$LangID);

        $pages_list = [
          "home",
          "about-us",
          "administration",
          "admission",
          "academic-life",
          "gallery",
          "alumni",
          "calendar",
          "careers",
          "announcements",
          "contact-us",
        ];

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "pages_list"    => $pages_list
        ];

        return view("frontend.sitemap",$data_array);
    }

    public function aboutUs(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_data      = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $page_content   = CMSMedia::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->whereCmMediaIsActive(1)->get();

        $index_controller   = new IndexController;

        $render_content = "";
        foreach ($page_content as $key => $content)
        {
           $key_direction   = $key % 2 ;
           $direction       = ($key_direction == 0 ? "left" : "right");
           $page_data_array = [
             "direction"    => $direction,
             "title"        => $content->cm_media_title,
             "caption"      => $content->cm_media_caption,
             "thumb_path"   => Config::get('constants.SINGLE_IMAGE_PATH') . "" . $content->cm_media_base_dir,
             "filename"     => $content->cm_media_file_name,
             "extension"    => $content->cm_media_file_extention
           ];

           $render_content .= $index_controller->renderContent($page_data_array);
        }

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_content"  => $page_content,
            "content"       => $render_content,
            "page_name"     => $request_data['page_name'],

        ];

        return view("frontend.about-us",$data_array);
    }

    public function contactUs(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name']
        ];

        return view("frontend.contact-us",$data_array);
    }

    public function gallery(Request $request, $LangID = null)
    {
        $media_data     = array();
        $request_data   = self::format_request_data($request,$LangID);
        $page_data      = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $albums         = CMSAlbums::whereFkPageId($page_data->cp_id)->get();
        foreach($albums as $album)
        {
            $media_data[]     = CMSMedia::whereFkAlbumId($album->ca_id)->get();
        }

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "albums"        => $albums,
            "media_data"    => $media_data
        ];

        return view("frontend.gallery",$data_array);
    }
    public function search(Request $request, $LangID = null, $searchQuery = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $search_results = DB::table("cms_rich_text_content")
                ->join("cms_pages","cms_rich_text_content.fk_page_id","=","cms_pages.cp_id")
                ->select("cms_rich_text_content.*","cms_pages.cp_page_title","cms_pages.cp_is_active")
                ->where("cms_rich_text_content.rt_title_page","LIKE","%" . $searchQuery . "%")
                ->orWhere("cms_rich_text_content.rt_page_content","LIKE","%" . $searchQuery . "%")
                ->where("cms_pages.cp_is_active","=",1)
                ->get();
        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "search_results" => $search_results
        ];

        return view("frontend.search",$data_array);
    }

    public function careers(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_data      = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $post_list      = CMSRichTextContent::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->get();


        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "post_list"     => $post_list
        ];

        return view("frontend.careers",$data_array);
    }

    public function announcements(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_data      = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $page_content   = CMSRichTextContent::whereFkPageId($page_data->cp_id)->whereFkLangId($request_data['lang_id'])->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_content"  => $page_content,
            "page_name"     => $request_data['page_name']
        ];

        return view("frontend.announcements",$data_array);
    }

    public function administration(Request $request, $LangID = null)
    {
        $request_data           = self::format_request_data($request,$LangID);
        $page_data              = CMSPages::whereCpPageTitle($request_data['page_name'])->whereCpIsActive(1)->first();
        $child_page_content     = CMSRichTextContent::whereFkPageId($page_data->cp_id)->whereFkLangId(1)->get();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "child_pages"   => $child_page_content
        ];


        return view("frontend.administration",$data_array);
    }

    public function admissions(Request $request, $LangID = null, $PageName = null)
    {
        $request_data         = self::format_request_data($request,$LangID,$PageName);
        $child_pages          = CMSPages::whereFkMenuId(4)->get();

        $child_pages_first    = $request_data['child_pages_first'];
        $first_page_content   = CMSRichTextContent::whereFkPageId($child_pages_first->cp_id)->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "child_pages"   => $child_pages,
            "first_child"   => $first_page_content
        ];

        return view("frontend.admissions",$data_array);
    }

    public function distinction(Request $request, $LangID = null, $PageID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_content   = CMSRichTextContent::whereRtId($PageID)->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "page_content"  => $page_content
        ];

        return view("frontend.text_content",$data_array);
    }

    public function testimonial(Request $request, $LangID = null, $PageID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $page_content   = CMSRichTextContent::whereRtId($PageID)->first();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages"     => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name"     => $request_data['page_name'],
            "page_content"  => $page_content
        ];

        return view("frontend.text_content",$data_array);
    }

    public function calendar(Request $request, $LangID = null)
    {
        $request_data   = self::format_request_data($request,$LangID);
        $event_data     = CMSEvents::whereFkPageId(15)->whereFkLangId($request_data['lang_id'])->whereCeIsActive(1)->get();

        $data_array = [
            "language"      => ($LangID == null ? "fr" : $LangID),
            "cms_pages" => $request_data['cms_pages'],
            "footer_pages" => $request_data['footer_pages'],
            "page_name" => $request_data['page_name'],
            "event_data"    => $event_data
        ];

        return view("frontend.calendar",$data_array);
    }

    public function submitCareers()
    {
        $rules = array(
            'first_name'    => 'required|min:3',
            'last_name'     => 'required|min:3',
            'email'         => 'required|email|min:3',
        );
        $validator  = Validator::make(Input::all(), $rules);
        $language   = (Input::get('language') == null ? "fr" : Input::get('language'));


        if ($validator->fails()) {
            return Redirect::to($language . '/careers')
                ->withErrors($validator)
                ->withInput(Input::all());
        }
        else
        {
            $destinationPath    = public_path() . "/" . Config::get('constants.CV_PATH');
            $save_file_name     = str_replace("@", "_", Input::get('email')) . "_" . rand(11111,99999) . "." . Input::file('cv')->getClientOriginalExtension();
            Input::file('cv')->move($destinationPath, $save_file_name); // uploading file to given path

//            $email_data = [
//                'first_name'    => Input::get('first_name'),
//                'last_name'     => Input::get('last_name'),
//                'phone'         => Input::get('phone'),
//                'email'         => Input::get('email'),
//                'country'         => Input::get('country'),
//                'employment_status'       => Input::get('employment_status'),
//                'position'       => Input::get('position')
//            ];
//
//            Mail::send('emails.careers', $email_data, function ($message) {
//
//                $message->from('contact@hripsimiantzlb.com', 'Hripsimiantz Careers');
//
//                $message->to('ahmad.sharif@live.com')->subject('Hripsimiantz Careers Message');
//
//            });

            return Redirect::to($language . '/careers')->withErrors(['status' => 'CV Sent Successfully!']);
        }
    }

    public function submitContact()
    {
        $rules = array(
            'first_name'    => 'required|min:3',
            'last_name'     => 'required|min:3',
            'email'         => 'required|email|min:3',
            'message'       => 'required|min:3'
        );
        $validator  = Validator::make(Input::all(), $rules);
        $language   = (Input::get('language') == null ? "fr" : Input::get('language'));


        if ($validator->fails()) {
            return Redirect::to($language . '/contact-us')
                ->withErrors($validator)
                ->withInput(Input::all());
        }
        else
        {
            $email_data = [
                'first_name'    => Input::get('first_name'),
                'last_name'     => Input::get('last_name'),
                'phone'         => Input::get('phone'),
                'email'         => Input::get('email'),
                'message'       => Input::get('message')
            ];

            Mail::send('emails.contact-us', $email_data, function ($message) {

                $message->from('contact@hripsimiantzlb.com', 'Hripsimiantz Contact Form');

                $message->to('ahmad.sharif@live.com')->subject('Hripsimiantz Contact Message');

            });

            return Redirect::to($language . '/contact-us')->withErrors(['status' => 'Message Sent Successfully!']);
        }
    }

    public function renderContent($page_data_array)
    {
        return view("frontend.content",$page_data_array)->render();
    }

    public function renderEvent($page_data_array)
    {
        return view("frontend.event_block",$page_data_array)->render();
    }

    public function renderScroller($scroller_data_array)
    {
        return view("frontend.scroller",$scroller_data_array)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
