<?php
/************************************************************
MenuController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 10, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controler related to Menu table contain all functionality related
************************************************************/


namespace App\Http\Controllers\Menu;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSMenu;
use Illuminate\Support\Facades\Hash;



class MenuController extends Controller
{
    public function __construct()
    {

    }


    /**
     * Get list of data saved in the database and dispay it in the grid
     *
     * @author Moe Mantach
     * @access public
     *
     * @return String{HTML} contain grid for list of menus
     */
    public function ajaxListMenus()
    {
        $cm_is_active = Input::get('cm_is_active');
        $menus = DB::table('cms_menu')->where('cm_is_active', $cm_is_active)->where('cm_is_deleted', 0)->orderBy('cm_menu_id', 'ASC')->get();

        $data = array(
            "menus" => $menus
        );
        return view("menu.displaylist",$data);
    }


    public function AddNewMenu()
    {
        $data = array();
        return view("menu.addmenu",$data);
    }


    public function EditMenu( $id )
    {
        $cms_menu = CMSMenu::find($id);

        $data = array(
            "cms_menu" => $cms_menu['attributes'],
            "id" => $id
        );
        return view("menu.editmenu",$data);
    }


    public function ajaxAddMenuInformation()
    {
       $cm_menu_id          = Input::get('cm_menu_id');
       $cm_menu_position    = Input::get('cm_menu_position');
       $cm_is_active        = Input::get('cm_is_active');
       $cm_is_active        = ($cm_is_active == '1') ? 1 : 0;
       $fields_array = array(
            "cm_menu_id" => $cm_menu_id,
            "cm_menu_position" => $cm_menu_position,
            "cm_is_active" => $cm_is_active
       );
       CMSMenu::saveMenuInformation($fields_array);

       $result_array = array();

       $result_array['is_error']    = 0;
       $result_array['error_msg']   = "Operation Complete successfully";

       return json_encode($result_array);

    }


    public function ajaxEditMenuInformation()
    {
       $id                  = Input::get('cm_id');
       $cm_menu_id          = Input::get('cm_menu_id');
       $cm_menu_position    = Input::get('cm_menu_position');
       $cm_is_active        = Input::get('cm_is_active');
       $cm_is_active        = ($cm_is_active == '1') ? 1 : 0;
       $fields_array = array(
            "id" => $id,
            "cm_menu_id" => $cm_menu_id,
            "cm_menu_position" => $cm_menu_position,
            "cm_is_active" => $cm_is_active
       );
       CMSMenu::saveMenuInformation($fields_array);

       $result_array = array();

       $result_array['is_error']    = 0;
       $result_array['error_msg']   = "Operation Complete successfully";

       return json_encode($result_array);

    }



    public function DeleteMenu( $id )
    {
        CMSMenu::deleteRow( $id );

        $result_array = array();

        $result_array['is_error']    = 0;
        $result_array['error_msg']   = "Operation Complete successfully";

        return json_encode($result_array);
    }


    public function __destruct()
    {

    }
}

