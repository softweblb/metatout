<?php
/************************************************************
LanguageController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 23, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

namespace App\Http\Controllers\Langs;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use DB;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;


class LanguageController extends Controller
{
    public function __construct()
    {

    }

    public function ajaxListLanguages()
    {
        $languages = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        $data = array(
            "languages" => $languages
        );
        return view("languages.displaylist",$data);
    }


    public function EditLanguageForm( $id )
    {
        $lang_info = DB::table('sys_languages')->where( 'sl_id', $id )->get();
        $data = array(
            "lang_info" => $lang_info
        );
        return view("languages.editLanguage",$data);
    }

    public function ajaxEditLang()
    {
        $sl_id       = $_POST['sl_id'];
        $lang_title  = $_POST['lang_title'];
        $lang_code   = $_POST['lang_code'];
        $lang_align   = $_POST['lang_align'];
        $lang_reverse_align   = $_POST['lang_reverse_align'];
        $lang_direction   = $_POST['lang_direction'];

        $fields_array = array(
            'sl_language_title' => $lang_title,
            'sl_language_code' => $lang_code,
            'sl_align' => $lang_align,
            'sl_reverse_align' => $lang_reverse_align,
            'sl_direction' => $lang_direction
        );
        DB::table('sys_languages')->where('sl_id', $sl_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    public function ajaxGenerateTranslation()
    {
        $lstCmsTranslation          = DB::table('sys_applang')->orderBy('al_id', 'ASC')->get();
        $lstCmsLanguage             = DB::table('sys_languages')->orderBy('sl_id', 'ASC')->get();

        $language_array = array();
        for ($i = 0; $i < count($lstCmsLanguage); $i++)
        {
            $language_array[$lstCmsLanguage[$i]->sl_id] = $lstCmsLanguage[$i]->sl_language_code;
        }


        $translation_array = array();

        for ($i = 0; $i < count($lstCmsTranslation); $i++)
        {
           $translation_array[ $language_array[ $lstCmsTranslation[$i]->fk_lang_id ] ][ $lstCmsTranslation[$i]->al_term_code ] = $_POST['translation_' . $lstCmsTranslation[$i]->al_id];
           $fields_array = array(
                'al_term_translation' => $_POST['translation_' . $lstCmsTranslation[$i]->al_id]
           );
           DB::table('sys_applang')->where('al_id', $lstCmsTranslation[$i]->al_id)->update($fields_array);
        }


        foreach ($translation_array as $lang_id => $lang_translation)
        {
            $str_save = "<?php \n return [ \n\n\t";
            $translation_path = base_path('resources/lang') . "/" . $lang_id . "/" . "translation.php";
            $dir = base_path('resources/lang') . "/" . $lang_id . "/";
            if(!is_dir($dir))
            {
                File::makeDirectory(base_path('resources/lang') . "/" . $lang_id . "/" ,0777);
            }
            $i = 0;
            $count = count($lang_translation);
           foreach ($lang_translation as $index => $value)
           {
               if( $i<$count - 1 )
               {
                   $str_save .= '"' . $index . '"=>"' . $value . '",';

               }
               else
               {
                   $str_save .= '"' . $index . '"=>"' . $value . '"';
               }
               $i++;

           }
           $str_save .= "\n\t ];";
           $fp = fopen($translation_path, "w+");
           fwrite($fp , $str_save);
           fclose($fp);

        }

        $result_array['is_error'] = 0;
        $result_array['error_msg'] ="Operation Complete Succesfully";
        return $result_array;

    }

    public function __destruct()
    {

    }
}