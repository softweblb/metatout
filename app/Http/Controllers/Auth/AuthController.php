<?php

namespace App\Http\Controllers\Auth;


use App\User;
use Validator;
use Input;
use Request;
use Redirect;
use Auth;
use Session;
use App\Models\CMSPages;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    public function Login()
    {
        $u_username = $_POST['u_username'];
        $u_password = $_POST['u_password'];

        $page_info_array = CMSPages::getListPagesByActive(1);

        $result_array = array();

        if (Auth::attempt(array('u_username' => $u_username, 'password' => $u_password)))
        {
            $userInfo = Auth::user();
            $page_info_array = CMSPages::getListPagesByActive(1);
            session()->put('pages_allowed' , $page_info_array);
            session()->put('user_id' , $userInfo->id);
            session()->put('fullname' , $userInfo->u_fullname);
            session()->put('username' , $userInfo->u_username);
            session()->put('website' , $userInfo->u_website);
            session()->put('email' , $userInfo->u_email);
            session()->put('phone' , $userInfo->u_phone);
            session()->put('mobile' , $userInfo->u_mobile);
            $result_array['is_error'] = 0;


        }
        else
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] ="Invalid Username And/Or Password";
        }


        return json_encode($result_array);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
