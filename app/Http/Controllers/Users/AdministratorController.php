<?php
/************************************************************
AdministratorController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 5, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Administrator Controller contain all main functionality of administrator Section
************************************************************/
namespace App\Http\Controllers\Users;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;


class AdministratorController extends Controller
{

    protected $layout = 'layouts.alogin';

    public function __construct()
    {

    }
    /**
     * get index : function open defaut when we open administrator link
     *
     * @author Moe Mantach
     * @access public
     *
     */
    public function getIndex(Request $request)
    {
        $data = array();
        if (Session::has('user_id'))
        {
            return redirect('administrator/dashboard');
        }
        else
        {
            $data = array(
                'request' => $request
            );
            return view('administrator.login',$data);
        }

    }


    /**
     * page of usermanagement to display list of users with ability to add/edit and delete userss
     *
     * @author Moe Mantach
     * @access public
     *
     * @return View
     */
    public function listUsers()
    {
        $data = array();
        return view('users.users',$data);
    }


    public function listAlbumsManagement()
    {
        $lstCmsPages        = DB::table('cms_pages')->where('cp_is_active', 1)->where('cp_page_type', 3)->orderBy('cp_id', 'ASC')->get();

        $data = array(
            "lstCmsPages" => $lstCmsPages
        );
        return view('pages.albums',$data);
    }


    public function listMediaAlbumManagement( $page_id , $ca_id )
    {
        $data = array(
            "ca_id" => $ca_id,
            "page_id" => $page_id
        );
        return view('pages.MediaAlbums',$data);
    }



    public function listMenu()
    {
        $data = array();
        return view('menu.menus',$data);
    }


    public function listLanguages()
    {
        $data = array();
        return view('languages.languages',$data);
    }


    public function translationManagement()
    {
        $lstCmsTranslation          = DB::table('sys_applang')->orderBy('al_id', 'ASC')->get();
        $lstCmsLanguage             = DB::table('sys_languages')->orderBy('sl_id', 'ASC')->get();



        $translation_array = array();

        for ($i = 0; $i < count($lstCmsTranslation); $i++)
        {
            $translation_array[ $lstCmsTranslation[$i]->al_term_code ][ $lstCmsTranslation[$i]->fk_lang_id ]['id'] = $lstCmsTranslation[$i]->al_id;
            $translation_array[ $lstCmsTranslation[$i]->al_term_code ][ $lstCmsTranslation[$i]->fk_lang_id ]['text'] = $lstCmsTranslation[$i]->al_term_translation;
        }

        $languages_array = array();

        for ($i = 0; $i < count($lstCmsLanguage); $i++)
        {
            $languages_array[ $lstCmsLanguage[$i]->sl_id ]= $lstCmsLanguage[$i]->sl_language_title;
        }


        $data = array(
            "translation_array" => $translation_array,
            "languages_array" => $languages_array
        );
        return view('pages.translations',$data);
    }

    public function listPages()
    {
        $lstCmsMenus = DB::table('cms_menu')->where('cm_is_active', 1)->where('cm_is_deleted', 0)->orderBy('cm_id', 'ASC')->get();
        $data = array("lstCmsMenus" => $lstCmsMenus);
        return view('pages.pages',$data);
    }


    public function listRichText()
    {
        $lstCmsPages        = DB::table('cms_pages')->where('cp_is_active', 1)->where('cp_page_type', 1)->orderBy('cp_id', 'ASC')->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        $data = array(
            "lstCmsPages" => $lstCmsPages ,
            "listLanguages" => $listLanguages
        );
        return view('pages.richText',$data);
    }


    public function SimpleTextManagement()
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        $data = array(
            "listLanguages" => $listLanguages
        );
        return view('pages.listSimpleText',$data);
    }


    public function listEventsManagement()
    {
        $lstCmsPages        = DB::table('cms_pages')->where('cp_is_active', 1)->where('cp_page_type', 8)->orderBy('cp_id', 'ASC')->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $data = array(
            "lstCmsPages" => $lstCmsPages,
            "listLanguages" => $listLanguages
        );
        return view('pages.events',$data);
    }

    public function listSingleImageDisplayManagement()
    {
        $lstCmsPages        = DB::table('cms_pages')->where('cp_is_active', 1)->where('cp_page_type', 5)->orderBy('cp_id', 'ASC')->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        $data = array(
            "lstCmsPages" => $lstCmsPages ,
            "listLanguages" => $listLanguages
        );
        return view('pages.SingleImageManagement',$data);
    }


    public function listRoles()
    {
        $data = array();
        return view('roles.roles',$data);
    }


    public function __destruct()
    {

    }
}