<?php
/************************************************************
UsersController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 5, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Administrator Controller contain all main functionality of administrator Section
************************************************************/
namespace App\Http\Controllers\Users;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{

    public function editProfile( $userId )
    {
        $users = DB::select('select * from users where id = ?', [$userId]);

        $data = array(
            'users' => $users,
            'userId' => $userId
        );
        return view('users/editprofile' , $data);
    }

    /**
     * change profile information changed in the database
     *
     * @author Moe Mantach
     * @access public
     */
    public function ajaxEditProfile(Request $request)
    {

        $user_id            = isset( $_POST['user_id'] ) ? $request->input('user_id') : 0;
        $username           = isset( $_POST['username'] ) ? $request->input('username') : "";
        $fullname           = isset( $_POST['fullname'] ) ? $request->input('fullname') : "";
        $u_password         = isset( $_POST['u_password'] ) ? $request->input('u_password') : "";
        $u_retype_password  = isset( $_POST['u_retype_password'] ) ? $request->input('u_retype_password') : "";
        $u_email            = isset( $_POST['u_email'] ) ? $request->input('u_email') : "";
        $u_website          = isset( $_POST['u_website'] ) ? $request->input('u_website') : "";
        $u_mobile           = isset( $_POST['u_mobile'] ) ?  $request->input('u_mobile') : "";
        $u_phone            = isset( $_POST['u_phone'] ) ?   $request->input('u_phone') : "";

        $fields_array = array();
        $result_array = array();

        if(strlen( trim( $username ) ) > 0)
         $fields_array['u_username'] = $username;
        if(strlen( trim( $fullname ) ) > 0)
            $fields_array['u_fullname'] = $fullname;
        if(strlen( trim( $u_website ) ) > 0)
            $fields_array['u_website']  = $u_website;
        if(strlen( trim( $u_email ) ) > 0)
            $fields_array['u_email']  = $u_email;
        if(strlen( trim( $u_phone ) ) > 0)
            $fields_array['u_phone']  = $u_phone;
        if(strlen( trim( $u_mobile ) ) > 0)
            $fields_array['u_mobile']  = $u_mobile;

        DB::table('users')->where('id', $user_id)->update($fields_array);

        $fields_array['id']  = $user_id;
        // change session information
        $this->_ChangeSessionUserInformation($fields_array , $user_id);

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "operation completed successfully";

        return response()->json($result_array);

    }


    public function ajaxListUsers()
    {
        $users = DB::table('users')->where('u_is_active', 1)->where('u_is_deleted', 0)->orderBy('id', 'ASC')->get();

        $data = array(
            "users" => $users
        );
        return view("users.displaylist",$data);
    }


    /**
     * Display Add User Form in the popup
     *
     * @author Moe Mantach
     * @access public
     *
     * @return ViewObject
     */
    public function addUserForm()
    {
        // get list of countries
        $countries = DB::table('country')->orderBy('id', 'asc')->get();

        // get list of roles
        $roles = DB::table('roles')->orderBy('role_id', 'asc')->get();

        $data = array(
            "countries" => $countries,
            "roles" => $roles
        );
        return view('users.adduser',$data);
    }


    /**
     * ajax Add new user functionality to the database
     *
     * @author Moe Mantach
     * @access public
     *
     *
     */
    public function ajaxaddUser(Request $request)
    {
        $fields_array = array();
        $fields_array['fk_role_id']     = $_POST['u_role'];
        $fields_array['u_username']     = $_POST['username'];
        $fields_array['password']       = Hash::make( $_POST['u_password'] );


        $fields_array['u_date_birth']   = $_POST['u_date_birth'];
        $fields_array['u_country']      = $_POST['u_country'];
        $fields_array['u_fullname']     = $_POST['fullname'];
        $fields_array['u_email']        = $_POST['u_email'];
        $fields_array['u_website']      = $_POST['u_website'];
        $fields_array['u_mobile']       = $_POST['u_mobile'];
        $fields_array['u_phone']        = $_POST['u_phone'];
        $fields_array['u_fax']          = $_POST['u_fax'];

        $id = DB::table('users')->insertGetId($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";
        $result_array['id']         = $id;

        return response()->json($result_array);

    }


    public function ajaxEditUser(Request $request)
    {
        $fields_array = array();
        $user_id = $_POST['user_id'];

        $fields_array['fk_role_id']     = $_POST['u_role'];
        $fields_array['u_username']     = $_POST['username'];

        if(strlen( trim( $_POST['u_password'] ) ) > 0)
        {
            $fields_array['password']       = Hash::make( $_POST['u_password'] );
        }

        $fields_array['u_date_birth']   = $_POST['u_date_birth'];
        $fields_array['u_country']      = $_POST['u_country'];
        $fields_array['u_fullname']     = $_POST['fullname'];
        $fields_array['u_email']        = $_POST['u_email'];
        $fields_array['u_website']      = $_POST['u_website'];
        $fields_array['u_mobile']       = $_POST['u_mobile'];
        $fields_array['u_phone']        = $_POST['u_phone'];
        $fields_array['u_fax']          = $_POST['u_fax'];

        $id = DB::table('users')->where('id', $user_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";
        $result_array['id']         = $user_id;

        return response()->json($result_array);

    }


    public function editUserForm($id)
    {
        // get list of countries
        $countries = DB::table('country')->orderBy('id', 'asc')->get();

        // get list of roles
        $roles = DB::table('roles')->orderBy('role_id', 'asc')->get();

        // get saved user information
        $users = DB::select('select * from users where id = ?', [$id]);


        $data = array(
            "countries" => $countries,
            "roles" => $roles,
            "users" => $users
        );
        return view('users.edituser',$data);
    }


    public function ajaxDeleteUser()
    {
        $user_id = $_POST['user_id'];
        DB::table('users')->where('id', '=', $user_id)->delete();

        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }


    private function _stdToArray($obj){
        $reaged = (array)$obj;
        foreach($reaged as $key => &$field){
            if(is_object($field))$field = $this->_stdToArray($field);
        }
        return $reaged;
    }



    /**
     * change session user information saved in the database , you check if fields not passed to the function
     * get the data information from the database
     *
     * @author Moe Mantach
     * @access private
     *
     */
    private function _ChangeSessionUserInformation( $user_data = array()  , $user_id)
    {

        if( count( $user_data ) == 0 )
        {
            $user_obj = DB::table('users')->where('id', $user_id)->get();
            $userInfo = $user_obj[0];
            $user_data = $this->_stdToArray($userInfo);
        }

        if(isset($user_data['id']))
            session()->put('user_id' , $user_data['id']);
        if(isset($user_data['u_fullname']))
            session()->put('fullname' , $user_data['u_fullname']);
        else
            session()->put('fullname' , '');
        if(isset($user_data['u_username']))
            session()->put('username' , $user_data['u_username']);
        else
            session()->put('username' , '');
        if(isset($user_data['u_website']))
            session()->put('website' , $user_data['u_website']);
        else
            session()->put('website' , '');
        if(isset($user_data['u_email']))
            session()->put('email' , $user_data['u_email']);
        else
            session()->put('email' , '');
        if(isset($user_data['u_phone']))
            session()->put('phone' , $user_data['u_phone']);
        else
            session()->put('phone' , '');
         if(isset($user_data['u_mobile']))
            session()->put('mobile' , $user_data['u_mobile']);
         else
             session()->put('mobile' , '');
    }


    public function getSignOut() {

        Auth::logout();
        Session::flush();
        return Redirect::to('administrator/');
    }


    public function getIndex()
    {

    }

}