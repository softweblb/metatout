<?php
/************************************************************
SingleImageController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 26, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controller of all pages contain list of single images
************************************************************/
namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use Config;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class SingleImageController extends Controller
{
    /**
     * Display  list of Pages saved in the database
     */
    public function ajaxListPages()
    {
        $cms_page           = $_POST['cms_page'];
        $cms_lang           = $_POST['cms_lang'];
        $cms_media_display  = DB::table('cms_media')->where('fk_page_id', $cms_page)->where('cm_media_is_active', 1)->where('fk_lang_id',$cms_lang)->orderBy('cm_id', 'ASC')->get();


        $data = array(
            "cms_media_display" => $cms_media_display
        );
        return view("pages.displayListSingleImages",$data);
    }


    /**
     * display add page form in the popup
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array Html
     */
    public function AddSImageForm($PageId)
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();


        $data = array(
            "PageId" => $PageId,
            "listLanguages" => $listLanguages
        );
        return view("pages.addSImageContent",$data);
    }

    /**
     * ListSImage By Language
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $pageId
     *
     * @return Array $ListSimpleMedia
     */
    private function __GetListSimpleMedia( $cms_media_display )
    {
        $ListSimpleMedia = array();

        for ($i = 0; $i < count($cms_media_display); $i++)
        {

            $ListSimpleMedia[ $cms_media_display[$i]->fk_lang_id ] = array(
                "cm_id" => $cms_media_display[$i]->cm_id,
                "cm_row_id" => $cms_media_display[$i]->cm_row_id,
                "cm_media_title" => $cms_media_display[$i]->cm_media_title,
                "cm_media_caption" => $cms_media_display[$i]->cm_media_caption
            );
        }

        return $ListSimpleMedia;
    }



    public function EditSImageForm($PageId)
    {
        $cms_media_display  = DB::table('cms_media')->where('cm_row_id', $PageId)->where('cm_media_is_active', 1)->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();



        $link = url() . "/assets/imgs/noimage.gif";
        $directory = public_path(). "/" . Config::get( 'constants.SINGLE_IMAGE_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name . "." . $cms_media_display[0]->cm_media_file_extention ;

        if(is_file($directory))
        {
            $link = url() . "/" . Config::get( 'constants.SINGLE_IMAGE_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name  . "." . $cms_media_display[0]->cm_media_file_extention ;
        }

        $ListSimpleMedia = $this->__GetListSimpleMedia($cms_media_display);


        $page_id = $cms_media_display[0]->fk_page_id;

        $data = array(
            "ListSimpleMedia" => $ListSimpleMedia,
            "listLanguages" => $listLanguages,
            "PageId" => $page_id,
            "link" => $link
        );
        return view("pages.editSImageContent",$data);
    }


    /**
     *Add new role to the database with title and description
     *
     *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
    public function ajaxAddSImage()
    {
        $fk_page_id              = $_POST['page_id'];
        $cm_media_is_active      = 1;
        $cm_media_type           = 1;
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $row = DB::table('cms_media')->max('cm_row_id') +  1;
        for ($i = 0; $i < count($listLanguages); $i++)
        {

            $cm_media_title          = $_POST['cm_media_title_' . $listLanguages[$i]->sl_language_code];
            $cm_media_caption        = $_POST['cm_media_caption_' . $listLanguages[$i]->sl_language_code];
            $fields_array[] = array(
                'fk_page_id' => $fk_page_id,
                'cm_row_id' => $row,
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'cm_media_title' => $cm_media_title,
                'cm_media_caption' => $cm_media_caption,
                'cm_media_is_active' => $cm_media_is_active
            );
        }
        DB::table('cms_media')->insert($fields_array);
        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }



        public function ajaxEditSImage()
        {
            $cm_media_is_active      = 1;
            $cm_media_type           = 1;
            $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

            for ($i = 0; $i < count($listLanguages); $i++)
            {
                $cm_id              = $_POST['cm_id_' . $listLanguages[$i]->sl_language_code];
                $cm_media_title     = $_POST['cm_media_title_' . $listLanguages[$i]->sl_language_code];
                $cm_media_caption   = $_POST['cm_media_caption_' . $listLanguages[$i]->sl_language_code];
                $fields_array = array(
                    'cm_media_title' => $cm_media_title,
                    'cm_media_caption' => $cm_media_caption,
                    'cm_media_is_active' => $cm_media_is_active
                );
                DB::table('cms_media')->where('cm_id', $cm_id)->update($fields_array);
            }


            $result_array               = array();
            $result_array['is_error']   = 0;
            $result_array['error_msg']  = "Operation Complete Successfully";

            return json_encode($result_array);
    }

    /**
    * function to delete Single Image from the database
    *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
     public function ajaxDeleteSImage()
     {
       $cm_row_id   = $_POST['cm_row_id'];

       $fields_array = array(
          "cm_media_is_active" => 0
       );
       DB::table('cms_media')->where('cm_row_id', $cm_row_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
     }
}