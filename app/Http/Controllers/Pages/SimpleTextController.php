<?php
/************************************************************
SimpleTextController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class SimpleTextController extends Controller
{
    /**
     * Display  list of Pages saved in the database
     */
    public function ajaxListSimpleText()
    {
        $cms_lang           = $_POST['cms_lang'];
        $cms_simple_text    = DB::table('cms_text_content')->where('fk_lang_id',$cms_lang)->orderBy('cc_id', 'ASC')->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        $data = array(
            "cms_simple_text" => $cms_simple_text,
            "listLanguages" => $listLanguages
        );
        return view("pages.displaylistSimpleText",$data);
    }


    /**
    * display add page form in the popup
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array Html
    */
    public function AddSTForm()
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();


        $data = array(
            "listLanguages" => $listLanguages
        );
        return view("pages.addSTContent",$data);
    }


    private function __GetListInfo( $lst_cms_st_info )
    {
        $ListSTInfo = array();

        for ($i = 0; $i < count( $lst_cms_st_info ); $i++)
        {

                $ListSTInfo[ $lst_cms_st_info[$i]->fk_lang_id ] = array(
                    "cc_id" => $lst_cms_st_info[$i]->cc_id,
                    "cc_text_title" => $lst_cms_st_info[$i]->cc_text_title,
                    "cc_text_description" => $lst_cms_st_info[$i]->cc_text_description
                );
        }



                    return $ListSTInfo;
        }


    public function EditSTForm($id)
    {
        $cms_text_content   = DB::table('cms_text_content')->where('cc_row_id', $id)->get();

       $lst_cms_st_info =  $this->__GetListInfo($cms_text_content);
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $data = array(
            "lst_cms_st_info" => $lst_cms_st_info,
            "listLanguages" => $listLanguages,
            "row_id" => $id
        );
        return view("pages.editSTContent",$data);
    }


    /**
    *Add new role to the database with title and description
    *
    *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
     public function ajaxAddSimpleTextContent()
     {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $cc_row_id = DB::table('cms_text_content')->max('cc_row_id') +  1;
        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $cc_text_title               = $_POST['cc_text_title_' . $listLanguages[$i]->sl_language_code];
            $cc_text_description         = $_POST['cc_text_description_' . $listLanguages[$i]->sl_language_code];

            $fields_array[] = array(
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'cc_text_title' => $cc_text_title,
                'cc_row_id' => $cc_row_id,
                'cc_text_description' => $cc_text_description
            );
        }
        $id = DB::table('cms_text_content')->insert($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    public function ajaxEditSimpleText()
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $cc_text_title          = $_POST['cc_text_title_' . $listLanguages[$i]->sl_language_code];
            $cc_text_description    = $_POST['cc_text_description_' . $listLanguages[$i]->sl_language_code];
            $cc_id                  = $_POST['cc_id_' . $listLanguages[$i]->sl_language_code];

            $fields_array = array(
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'cc_text_title' => $cc_text_title,
                'cc_text_description' => $cc_text_description
            );
            DB::table('cms_text_content')->where('cc_id', $cc_id)->update($fields_array);
        }


        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    /**
    * function to delete role from the database
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array $result_array
    */
    public function ajaxDeleteSimpleText()
   {
       $cc_row_id           = $_POST['cc_row_id'];

       DB::table('cms_text_content')->where('cc_row_id', $cc_row_id)->delete();

       $result_array               = array();
       $result_array['is_error']   = 0;
       $result_array['error_msg']  = "Operation Complete Successfully";

       return json_encode($result_array);
   }
}