<?php
/************************************************************
EventsController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 27, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use Config;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class EventsController extends Controller
{


    public function AjaxListEvents()
    {
        $cms_page       = $_POST['cms_page'];
        $cms_lang       = $_POST['cms_lang'];
        $cms_events  = DB::table('cms_events')->where('fk_page_id', $cms_page)->where('fk_lang_id',$cms_lang)->where('ce_is_deleted',0)->orderBy('ce_id', 'ASC')->get();


        $data = array(
            "cms_events" => $cms_events
        );
        return view("pages.displayListEvents",$data);
    }


    public function AddEventForm( $PageId )
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();


        $data = array(
            "PageId" => $PageId,
            "listLanguages" => $listLanguages
        );
        return view("pages.addEventForm",$data);
    }



    private function __GetListInfo( $lst_cms_events )
    {
        $ListEvents = array();

        for ($i = 0; $i < count( $lst_cms_events ); $i++)
        {

            $ListEvents[ $lst_cms_events[$i]->fk_lang_id ] = array(
                "ce_id" => $lst_cms_events[$i]->ce_id,
                "ce_event_title" => $lst_cms_events[$i]->ce_event_title,
                "ce_date_from" => $lst_cms_events[$i]->ce_date_from,
                "ce_date_to" => $lst_cms_events[$i]->ce_date_to,
                "ce_event_description" => $lst_cms_events[$i]->ce_event_description
            );
        }

         return $ListEvents;
    }

    public function EditEventForm($rowId)
    {
        $lst_cms_events     = DB::table('cms_events')->where('ce_row_id', $rowId)->where('ce_is_active', 1)->get();
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();



        $link = url() . "/assets/imgs/noimage.gif";
        $directory = public_path(). "/" . Config::get( 'constants.EVENT_PATH') . $lst_cms_events[0]->ce_media_base_dir . $lst_cms_events[0]->ce_media_file_name . "." . $lst_cms_events[0]->ce_media_file_extention ;

        if(is_file($directory))
        {
            $link = url() . "/" . Config::get( 'constants.EVENT_PATH') . $lst_cms_events[0]->ce_media_base_dir . $lst_cms_events[0]->ce_media_file_name  . "." . $lst_cms_events[0]->ce_media_file_extention ;
        }

        $ListEventInfo = $this->__GetListInfo($lst_cms_events);

        $data = array(
            "ListEventInfo" => $ListEventInfo,
            "listLanguages" => $listLanguages,
            "rowId" => $rowId,
            "pageId" => $lst_cms_events[0]->fk_page_id,
            "link" => $link
        );
        return view("pages.EditEventForm",$data);
    }

    public function AjaxEditEvent()
    {
        $ce_date_from       = $_POST['ce_from_date'];
        $ce_date_to         = $_POST['ce_to_date'];
        $ce_is_active       = 1;
        $ce_row_id          = $_POST['row_id'];
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $ce_id              = $_POST['ce_id_' . $listLanguages[$i]->sl_language_code];
            $ce_event_title     = $_POST['ce_event_title_' . $listLanguages[$i]->sl_language_code];
            $ce_event_description   = $_POST['ce_event_description_' . $listLanguages[$i]->sl_language_code];
                $fields_array = array(
                    'ce_event_title' => $ce_event_title,
                    'ce_event_description' => $ce_event_description,
                    'ce_date_from' => $ce_date_from,
                    'ce_date_to' => $ce_date_to
                );
                DB::table('cms_events')->where('ce_id', $ce_id)->update($fields_array);
        }


        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);

    }

    public function AjaxAddEvent()
    {
        $fk_page_id                 = $_POST['page_id'];
        $ce_date_from               = $_POST['ce_from_date'];
        $ce_date_to                 = $_POST['ce_to_date'];

        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $ce_row_id          = DB::table('cms_events')->max('ce_row_id') +  1;


        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $ce_title_event           = $_POST['ce_event_title_' . $listLanguages[$i]->sl_language_code];
            $ce_event_description     = $_POST['ce_event_description_' . $listLanguages[$i]->sl_language_code];
            $fields_array[] = array(
                'fk_page_id' => $fk_page_id,
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'ce_event_title' => $ce_title_event,
                'ce_event_description' => $ce_event_description,
                'ce_date_from' => $ce_date_from,
                'ce_date_to' => $ce_date_to,
                'ce_row_id' => $ce_row_id,
                'ce_is_active' => 1
            );
       }

        $id = DB::table('cms_events')->insert($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

            return json_encode($result_array);
    }

    public function DeleteEventManager($row_id)
    {

        $fields_array = array(
            "ce_is_deleted" => 1
        );
        DB::table('cms_events')->where('ce_row_id', $row_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }
}