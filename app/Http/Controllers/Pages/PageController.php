<?php
/************************************************************
PageController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 21, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
CMS Page COntroller
************************************************************/


namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\CMSPages;
use App\Models\CMS\CMSRichTextContent;
use App\Models\CMS\CMSPageTypes;
use App\Models\SYSTEM\SYSTEMLanguages;



class PageController extends Controller
{
   /**
    * Display  list of Pages saved in the database
    */
   public function ajaxListPages( Request $request )
   {
        $cms_menu = $_POST['cms_menu'];
        $pages = DB::table('cms_pages')->where('cp_is_active', 1)->orderBy('cp_id', 'ASC')->get();
        $page_types = DB::table('cms_page_type')->where('cs_is_active', 1)->orderBy('cs_id', 'ASC')->get();

        $types_array = array();

        for ($i = 0; $i < count( $page_types ); $i++)
        {
            $types_array[$page_types[$i]->cs_id] = $page_types[$i]->cs_page_type;
        }


        $data = array(
            "pages" => $pages,
            "types_array" => $types_array
        );
        return view("pages.displaylist",$data);
   }


   /**
    * display add page form in the popup
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array Html
    */
   public function AddPageForm($MenuId)
   {
       $page_types = DB::table('cms_page_type')->where('cs_is_active', 1)->orderBy('cs_id', 'ASC')->get();

       $data = array(
           "page_types" => $page_types,
           "MenuId" => $MenuId
       );
       return view("pages.addpage",$data);
   }

   public function EditPageForm($id)
   {
       $pages = DB::table('cms_pages')->where('cp_id', $id)->orderBy('cp_id', 'ASC')->get();
       $page_types = DB::table('cms_page_type')->where('cs_is_active', 1)->orderBy('cs_id', 'ASC')->get();

       $data = array(
           "page_types" => $page_types,
           "pages" => $pages
       );
       return view("pages.editpage",$data);
   }


   /**
    *Add new role to the database with title and description
    *
    *@author Moe Mantach
    *@access public
    *
    *@return Array $result_array
    */
   public function ajaxAddPage( Request $request )
   {
       $fk_menu_id              = $request->input('menu_id');
       $cp_page_title           = $request->input('page_title');
       $cp_page_type            = $request->input('page_type');
       $cp_page_menu_title      = $request->input('cp_page_menu_title');
       $cp_is_active            = 1;

       $fields_array = array(
           'fk_menu_id' => $fk_menu_id,
           'cp_page_title' => $cp_page_title,
           'cp_page_type' => $cp_page_type,
           'cp_page_menu_title' => $cp_page_menu_title,
           'cp_is_active' => $cp_is_active
       );
       $id = DB::table('cms_pages')->insertGetId($fields_array);

       $page_info_array = CMSPages::getListPagesByActive(1);
       session()->put('pages_allowed' , $page_info_array);


       $result_array               = array();
       $result_array['is_error']   = 0;
       $result_array['error_msg']  = "Operation Complete Successfully";
       $result_array['id']         = $id;
       return response()->json($result_array);
   }

   public function ajaxEditPage( Request $request )
   {
       $fk_menu_id              = $request->input('menu_id');
       $cp_page_title           = $request->input('page_title');
       $cp_page_type            = $request->input('page_type');
       $cp_page_menu_title      = $request->input('cp_page_menu_title');
       $cp_id                   = $request->input('cp_id');
       $cp_is_active            = 1;


       $fields_array = array(
           'fk_menu_id' => $fk_menu_id,
           'cp_page_title' => $cp_page_title,
           'cp_page_type' => $cp_page_type,
           'cp_page_menu_title' => $cp_page_menu_title,
           'cp_is_active' => $cp_is_active
       );
       DB::table('cms_pages')->where('cp_id', $cp_id)->update($fields_array);

       $page_info_array = CMSPages::getListPagesByActive(1);
       session()->put('pages_allowed' , $page_info_array);

       $result_array               = array();
       $result_array['is_error']   = 0;
       $result_array['error_msg']  = "Operation Complete Successfully";

        return response()->json($result_array);
   }

   /**
    * function to delete role from the database
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array $result_array
    */
   public function ajaxDeletePage( Request $request )
   {
       $cp_id            = $$request->input('cp_id');

       $fields_array = array(
           "cp_is_active" => 0
       );
       DB::table('cms_pages')->where('cp_id', $cp_id)->update($fields_array);

       $result_array               = array();
       $result_array['is_error']   = 0;
       $result_array['error_msg']  = "Operation Complete Successfully";

       return response()->json($result_array);
   }


   /**
    * display Page Content Management related to the Page type seleted for the page
    *
    * @author Moe Mantach
    * @access public
    * @param Integer $id
    *
    * @return View Content
    */
   public function displayPageContentManagement( $id )
   {
        $page_information = CMSPages::find($id);

        $page_type = $page_information['attributes']['cp_page_type'];
        $page_name = $page_information['attributes']['cp_page_title'];

        switch($page_type)
        {
            case CMSPageTypes::PAGE_TYPE_LIST_RICHTEXT :
            {
                  $listLanguages      = SYSTEMLanguages::all();

                    $data = array(
                        "page_id" => $id ,
                        "page_name" => $page_name ,
                        "listLanguages" => $listLanguages
                    );
                    return view('pages.richText',$data);
            }
           break;
            case CMSPageTypes::PAGE_TYPE_SINGE_IMAGE_PLUS_TEXT :
            {
                  $listLanguages      = SYSTEMLanguages::all();

                    $data = array(
                        "page_id" => $id ,
                        "page_name" => $page_name ,
                        "listLanguages" => $listLanguages
                    );
                    return view('pages.SingleImageManagement',$data);
            }
           break;
            case CMSPageTypes::PAGE_TYPE_SLIDERS:
            {
                  $listLanguages      = SYSTEMLanguages::all();

                    $data = array(
                        "page_id" => $id ,
                        "page_name" => $page_name ,
                        "listLanguages" => $listLanguages
                    );
                    return view('pages.SingleImageManagement',$data);
            }
           break;
            case CMSPageTypes::PAGE_TYPE_ALBUMS :
            {
                  $listLanguages      = SYSTEMLanguages::all();

                    $data = array(
                        "page_id" => $id ,
                        "page_name" => $page_name ,
                        "listLanguages" => $listLanguages
                    );
                    return view('pages.albums',$data);
            }
           break;
            case CMSPageTypes::PAGE_TYPE_RICHTEXT :
            {
                  $listLanguages      = SYSTEMLanguages::all();

                  $getRichText = CMSRichTextContent::getAllRichText($id);

                    $data = array(
                        "page_id" => $id ,
                        "page_name" => $page_name ,
                        "getRichText" => $getRichText ,
                        "listLanguages" => $listLanguages
                    );
                    return view('pages.singletext',$data);
            }
           break;
            case CMSPageTypes::PAGE_TYPE_CALENDAR :
            {
                $listLanguages      = SYSTEMLanguages::all();

                $data = array(
                    "page_id" => $id ,
                    "page_name" => $page_name ,
                    "listLanguages" => $listLanguages
                );
                return view('pages.events',$data);
            }
           break;
        }

   }
}