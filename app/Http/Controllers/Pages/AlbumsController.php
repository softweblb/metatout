<?php
/************************************************************
AlbumsController.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Controller to add / Edit and Delete ALbums name and description
************************************************************/



namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Request;
use Session;
use Redirect;
use Config;
use DB;
use App\Models\CMSPages;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class AlbumsController extends Controller
{


    public function AjaxListAlbums()
    {
        $cms_page       = $_POST['cms_page'];
        $cms_albums  = DB::table('cms_albums')->where('fk_page_id', $cms_page)->where('ca_is_deleted',0)->orderBy('ca_id', 'ASC')->get();

        $data = array(
            "cms_albums" => $cms_albums,
            "cms_page" => $cms_page
        );
        return view("pages.displayListAlbums",$data);
    }


    public function AddAlbumForm( $PageId )
    {
        $page_info = CMSPages::find($PageId);
        $data = array(
            "PageId" => $PageId,
            "page_name" => $page_info['attributes']['cp_page_title']
        );
        return view("pages.addAlbumForm",$data);
    }

    public function EditAlbumForm($AlbId)
    {
        $lst_cms_albums    = DB::table('cms_albums')->where('ca_id', $AlbId)->get();
        $PageId = $lst_cms_albums[0]->fk_page_id;
        $page_info = CMSPages::find($PageId);
        $data = array(
            "lst_cms_albums" => $lst_cms_albums,
            "ca_id" => $AlbId,
            "page_id" => $PageId,
            "page_name" => $page_info['attributes']['cp_page_title']
        );
        return view("pages.EditAlbumForm",$data);
    }

    public function AjaxEditAlbum()
    {
        $ca_album_title         = $_POST['ca_album_title'];
        $ca_id                  = $_POST['ca_id'];
        $ca_album_caption       = $_POST['ca_album_caption'];

        $fields_array = array(
           'ca_album_title' => $ca_album_title,
           'ca_album_caption' => $ca_album_caption
        );

        DB::table('cms_albums')->where('ca_id', $ca_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);

    }

    public function AjaxAddAlbum()
    {
        $fk_page_id             = $_POST['page_id'];
        $ca_album_title         = $_POST['ca_album_title'];
        $ca_album_caption       = $_POST['ca_album_caption'];
        $ca_date_creation       = date("Y-m-d H:i:s");
        $ca_albums_media_type   = 1;

        $fields_array[] = array(
            'fk_page_id' => $fk_page_id,
            'ca_album_title' => $ca_album_title,
            'ca_album_caption' => $ca_album_caption,
            'ca_date_creation' => $ca_date_creation,
            'ca_albums_media_type' => $ca_albums_media_type
        );
        $id = DB::table('cms_albums')->insert($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }

    public function DeleteAlbumManager( $ca_id )
    {

        $fields_array = array(
            "ca_is_deleted" => 1
        );
        DB::table('cms_albums')->where('ca_id', $ca_id)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }
}