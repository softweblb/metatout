<?php

/************************************************************
MediaAlbumsController.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
displayListAImages--
************************************************************/
namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use Config;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;
use App\Models\Media\WEBMedia;

class MediaAlbumsController extends Controller
{

    /**
     * Display list of Images In album
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public function DisplayListImages(Request $request)
    {
        $result_array = array();

        $page_number    = $request->input('page_number');
        $number_pages   = $request->input('number_pages');
        $cms_album      = $request->input('cms_album');
        $cms_page       = $request->input('cms_page');
        $count = 0;
        if($number_pages == '0')
        {
            $count = DB::table('cms_media')->where('fk_album_id',$cms_album)->where('cm_is_deleted',0)->where('fk_page_id',$cms_page)->count();
            $number_pages = $count / Config::get('appconfig.nbr_of_rows_per_page');
            $number_pages = ceil($number_pages);
        }

        if($page_number == 1)
        {
            $skip = 0;
        }
        else
        {

            $skip = ( $page_number - 1 ) * Config::get('appconfig.nbr_of_rows_per_page');
        }
        $lstCmsMedia        = DB::table('cms_media')->where('fk_album_id',$cms_album)->where('cm_is_deleted',0)->where('fk_page_id',$cms_page)->orderBy('cm_id', 'ASC')->skip($skip )->take( Config::get('appconfig.nbr_of_rows_per_page') )->get();
        $cms_albums_info    = DB::table('cms_albums')->where('ca_id', $cms_album)->get();

        $data = array(
           'lstCmsMedia' => $lstCmsMedia,
           'number_pages' => $number_pages,
           'page_number' => $page_number
        );

        return view('pages.DisplayListAlbumImages',$data);
    }



    public function SaveMediaInformation(Request $request)
    {
        $result_array = array();

        $cm_id          = $request->input('cm_id');
        $cm_media_title = $request->input('cm_media_title');
        $cm_media_caption = $request->input('cm_media_caption');


        $input = array(
            'id' => $cm_id,
            'cm_media_title' => $cm_media_title,
            'cm_media_caption' => $cm_media_caption
        );
        $res = WEBMedia::SaveMediaInformation($input);

        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "operation Complete Successfully";
        return Response()->json($result_array);
    }


    public function GetMediaInformation( $cm_id )
    {
        $result_array = array();


        $media = new WEBMedia;
        $media = WEBMedia::find($cm_id);

        $result_array['cm_id']              = $cm_id;
        $result_array['cm_media_title']     = $media->cm_media_title;
        $result_array['cm_media_caption']   = $media->cm_media_caption;

        return Response()->json($result_array);
    }



    public function AlbumsDropdown()
    {
        $cms_page        = $_POST['cms_page'];
        $lstCmsAlbums    = DB::table('cms_albums')->where('fk_page_id',$cms_page)->orderBy('ca_id', 'ASC')->get();

        $result_array = array();
        $data = array(
            'lstCmsAlbums' => $lstCmsAlbums
        );

        return view('pages.AlbumsDropdown',$data);
    }


    public function DeleteImageAlbum($cm_id)
    {
        $cmds_array = explode(',', $cm_id);
        if(!is_array($cmds_array))
        {
            $cmds_array = array($cm_id);
        }
        $fields_array = array(
            "cm_is_deleted" => 1
        );
        DB::table('cms_media')->whereIn('cm_id', $cmds_array)->update($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }


    public function AddImagesAlbumForm($cp_id,$ca_id)
    {
        $data = array(
            'cp_id' => $cp_id,
            'ca_id' => $ca_id
        );
        return view('pages.AddNewImagesAlbum',$data);
    }

    /**
     * Display Edit Image Album Form to save in the database
     *
     * @author Moe Mantach
     * @access public
     * @param unknown $cm_id
     */
    public function EditImageAlbum( $cm_id )
    {
        $cms_media_display  = DB::table('cms_media')->where('cm_id', $cm_id)->get();



        $link = url() . "/assets/imgs/noimage.gif";
        $directory = public_path(). "/" . Config::get( 'constants.ALBUMS_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name . "." . $cms_media_display[0]->cm_media_file_extention ;

        if(is_file($directory))
        {
            $link = url() . "/" . Config::get( 'constants.ALBUMS_PATH') . $cms_media_display[0]->cm_media_base_dir . $cms_media_display[0]->cm_media_file_name  . "." . $cms_media_display[0]->cm_media_file_extention ;
        }

        $data = array(
            "cms_media_display" => $cms_media_display,
            "cm_id" => $cm_id,
            "link" => $link
        );


        return view("pages.EditImageAlbum",$data);
    }


    /**
     * Edit Image ALbum to save in the database the information for currenct image
     *
     * @author Moe Mantach
     * @access public
     */
    public function AjaxEditIAlbum()
    {
        $cm_media_is_active      = 1;
        $cm_media_type           = 1;

        $cm_id              = $_POST['cm_id'];
        $cm_media_title     = $_POST['cm_media_title'];
        $cm_media_caption   = $_POST['cm_media_caption'];
        $fields_array = array(
                'cm_media_title' => $cm_media_title,
                'cm_media_caption' => $cm_media_caption,
                'cm_media_is_active' => $cm_media_is_active
         );
         DB::table('cms_media')->where('cm_id', $cm_id)->update($fields_array);


        $result_array               = array();
            $result_array['is_error']   = 0;
            $result_array['error_msg']  = "Operation Complete Successfully";

            return json_encode($result_array);
    }
}