<?php
/************************************************************
RichTextController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

namespace App\Http\Controllers\Pages;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Redirect;
use DB;
use App\Models\CMS\CMSRichTextContent;
use App\Models\SYSTEM\SYSTEMLanguages;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class RichTextController extends Controller
{
    /**
     * Display  list of Pages saved in the database
     */
    public function ajaxListPages()
    {
        $cms_page       = Input::get('cms_page');
        $cms_lang       = Input::get('cms_lang');


        $cms_rich_text = CMSRichTextContent::GetRichTextAdded($cms_page, $cms_lang);



        $data = array(
        "cms_rich_text" => $cms_rich_text
        );
        return view("pages.displaylistrichText",$data);
    }


    /**
    * display add page form in the popup
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array Html
    */
    public function AddRTForm($PageId)
    {
        $listLanguages      = SYSTEMLanguages::getAll();
        $data = array(
            "PageId" => $PageId,
            "listLanguages" => $listLanguages
        );
        return view("pages.addRTContent",$data);
    }



    public function EditRTForm($id)
    {
        $cms_rich_text  = CMSRichTextContent::getRichTextDataByLang($id);
        $listLanguages  = SYSTEMLanguages::getAll();
        $data = array(
            "cms_rich_text" => $cms_rich_text,
            "PageId" => $id,
            "listLanguages" => $listLanguages
        );
        return view("pages.editRTContent",$data);
    }


    /**
    *Add new role to the database with title and description
    *
    *@author Moe Mantach
     *@access public
     *
     *@return Array $result_array
     */
     public function ajaxAddRichTextContent()
     {
        $fk_page_id              = $_POST['page_id'];
        $rt_is_active            = 1;

        $listLanguages      = SYSTEMLanguages::getAll();
        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $rt_title_page           = $_POST['rt_title_page_' . $listLanguages[$i]->sl_language_code];
            $rt_page_content         = $_POST['rt_page_content_' . $listLanguages[$i]->sl_language_code];
            $fields_array[] = array(
                'fk_page_id' => $fk_page_id,
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'rt_title_page' => $rt_title_page,
                'rt_page_content' => $rt_page_content,
                'rt_is_active' => $rt_is_active
            );
        }


        $id = DB::table('cms_rich_text_content')->insert($fields_array);

        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }


    public function ajaxRichTextEditPage()
    {
        $listLanguages      = SYSTEMLanguages::getAll();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
        $rt_title_page          = $_POST['rt_title_page_' . $listLanguages[$i]->sl_language_code];
        $rt_page_content        = $_POST['rt_page_content_' . $listLanguages[$i]->sl_language_code];
        $rt_id                  = $_POST['rt_id_' . $listLanguages[$i]->sl_language_code];

            $fields_array = array(
                'fk_lang_id' => $listLanguages[$i]->sl_id,
                'rt_title_page' => $rt_title_page,
                'rt_page_content' => $rt_page_content
            );
            DB::table('cms_rich_text_content')->where('rt_id', $rt_id)->update($fields_array);
        }


        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

        return json_encode($result_array);
    }


    public function SaveRichText(Request $request)
    {
        $listLanguages      = SYSTEMLanguages::getAll();
         $page_id = $request->input('page_id');
        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $rt_title_page          = $request->input('rt_title_page_' . $listLanguages[$i]->sl_language_code);
            $rt_page_content        = $request->input('rt_page_content_' . $listLanguages[$i]->sl_language_code);
            $rt_id                  = $request->input('rt_id_' . $listLanguages[$i]->sl_language_code);
            if($rt_id == '')
            {
                $fields_array = array(
                    'fk_page_id' => $page_id,
                    'fk_lang_id' => $listLanguages[$i]->sl_id,
                    'rt_title_page' => $rt_title_page,
                    'rt_page_content' => $rt_page_content
                );
                DB::table('cms_rich_text_content')->insert($fields_array);
            }
            else
            {
                $fields_array = array(
                    'fk_page_id' => $page_id,
                    'fk_lang_id' => $listLanguages[$i]->sl_id,
                    'rt_title_page' => $rt_title_page,
                    'rt_page_content' => $rt_page_content
                );
                DB::table('cms_rich_text_content')->where('rt_id', $rt_id)->update($fields_array);

            }

        }


        $result_array               = array();
        $result_array['is_error']   = 0;
        $result_array['error_msg']  = "Operation Complete Successfully";

         return response()->json($result_array);
    }

    /**
    * function to delete role from the database
    *
    * @author Moe Mantach
    * @access public
    *
    * @return Array $result_array
    */
    public function ajaxDeleteRichTextEditor()
   {
       $rt_id            = $_POST['rt_id'];

       $fields_array = array(
       "rt_is_active" => 0
       );
       DB::table('cms_rich_text_content')->where('rt_id', $rt_id)->update($fields_array);

       $result_array               = array();
       $result_array['is_error']   = 0;
       $result_array['error_msg']  = "Operation Complete Successfully";

       return json_encode($result_array);
   }
}