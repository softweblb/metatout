<?php
/************************************************************
IndexController.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

namespace App\Http\Controllers\Frontend;

use App\User;
use Validator;
use Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cookie;
use Session;
use Config;
use Redirect;
use File;
use DB;
use App\Http\Controllers\Controller;
use App\Models\SYSTEM\SYSTEMCompanyDetails;
use Illuminate\Support\Facades\Hash;
use App\Models\SYSTEM\SYSTEMCountries;
use App\Models\CMSPages;
use App\Models\SYSTEM\SYSTEMLanguages;



class IndexController extends Controller
{
    public function __construct()
    {

    }


    private static function _SetLangCookie( $lang , Request $request)
    {
        return  response('Cookie set!')->withCookie(cookie('mt_lang', $lang, 60));
    }

    public function index( Request $request )
    {
        

        {/** INITIALISATION BLOCK */

            $mt_lang = $request->cookie('mt_lang');
            if($mt_lang == '')
            {
                $mt_lang = 'fr';
                self::_SetLangCookie('fr', $request);
            }

            $company_logo           = SYSTEMCompanyDetails::GetLogoUrl();
            $lstPages               = CMSPages::getListPagesByActive(1);
            $lang_info              = SYSTEMLanguages::getLangInfoByCode($mt_lang);
            $slideshow              = CMSPages::getDataRelatedToSelectedPage( 'homepage',$lang_info[0]->sl_id );
            $aboutus_info           = CMSPages::getDataRelatedToSelectedPage( 'aboutus',$lang_info[0]->sl_id );
            $scope_data_array       = CMSPages::getDataRelatedToSelectedPage( 'scope',$lang_info[0]->sl_id );
            $clients_data_array     = CMSPages::getDataRelatedToSelectedPage( 'clients',$lang_info[0]->sl_id );
            $projects_data_array    = CMSPages::getDataRelatedToSelectedPage( 'projects',$lang_info[0]->sl_id );
            $sys_company_details    = SYSTEMCompanyDetails::find(1);
        }
        $data = array(
            "company_logo" => $company_logo,
            "slideshow" => $slideshow,
            "aboutus_info" => $aboutus_info,
            "scope_data_array" => $scope_data_array,
            "clients_data_array" => $clients_data_array,
            "projects_data_array" => $projects_data_array,
            "lstPages" => $lstPages,
            "sys_company_details" =>$sys_company_details,
        );
        return view('frontend.site',$data);
    }

    public function __destruct()
    {

    }

}