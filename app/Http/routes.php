<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('administrator','Users\AdministratorController@getIndex');
Route::get('administrator/home','Users\AdministratorController@getIndex');
Route::get('administrator/UserManagement','Users\AdministratorController@listUsers');
Route::get('administrator/RolesManagement', 'Users\AdministratorController@listRoles');
Route::get('administrator/MenuManagement', 'Users\AdministratorController@listMenu');
Route::get('administrator/PageManagement', 'Users\AdministratorController@listPages');
Route::get('administrator/LanguageManagement', 'Users\AdministratorController@listLanguages');
Route::get('administrator/RichTextManagement', 'Users\AdministratorController@listRichText');
Route::get('administrator/SingleMediaManagement', 'Users\AdministratorController@listSingleImageDisplayManagement');
Route::get('administrator/CalendarManagement', 'Users\AdministratorController@listEventsManagement');
Route::get('administrator/AlbumManagement', 'Users\AdministratorController@listAlbumsManagement');
Route::get('administrator/MediaAlbumsManagement', 'Users\AdministratorController@listMediaAlbumManagement');
Route::get('administrator/TranslationsManagement', 'Users\AdministratorController@translationManagement');
Route::get('administrator/PageContent/{id}', 'Pages\PageController@displayPageContentManagement');
Route::get('administrator/CountryManagement', 'Configuration\CountryController@index');
Route::get('administrator/CompanyDetails', 'Configuration\CompanyDetailsController@index');
Route::get('administrator/ConfigurationManagement', 'Configuration\ConfigurationController@index');
Route::get('administrator/PageTypeManagement', 'CMS\PageTypesController@index');
Route::get('administrator/FreeTextManagement', 'CMS\FreeTextController@index');
Route::get('administrator/CompanyPrecense', 'CMS\ContactUs@DisplayCompanyPrecense');

Route::post('administrator/SavePageTypeManagement', 'CMS\PageTypesController@SaveInformation');

Route::post('administrator/displayListPrecense', 'CMS\ContactUs@ListCompanyPrecense');
Route::get('administrator/AddCompanyPrecense', 'CMS\ContactUs@AddCompanyPrecenseForm');
Route::get('administrator/EditCompanyPrecense/{id}', 'CMS\ContactUs@EditCompanyPrecenseForm');
Route::post('administrator/RequestAddCompanyPrecense', 'CMS\ContactUs@AjaxAddCompanyPrecense');
Route::post('administrator/RequestEditCompanyPrecense', 'CMS\ContactUs@AjaxEditCompanyPrecense');
Route::post('administrator/RequestDeleteCompanyPrecense', 'CMS\ContactUs@DeleteCompanyPrecense');


Route::post('administrator/displayListFreeText', 'CMS\FreeTextController@displayListFreeText');
Route::get('administrator/AddNewFreeText', 'CMS\FreeTextController@AddFreeTextForm');
Route::get('administrator/EditFreeText/{id}', 'CMS\FreeTextController@EditFreeTextForm');
Route::post('administrator/ajaxAddFreeText', 'CMS\FreeTextController@ajaxAddFreeText');
Route::post('administrator/ajaxEditFreeText', 'CMS\FreeTextController@ajaxEditFreeText');
Route::post('administrator/ajaxDeleteFreeText', 'CMS\FreeTextController@ajaxDeleteFreeText');


Route::post('administrator/displayListCountries', 'Configuration\CountryController@ajaxListCountries');
Route::get('administrator/AddNewCountryForm', 'Configuration\CountryController@AddNewCountryForm');
Route::get('administrator/EditCountry/{id}', 'Configuration\CountryController@EditCountryForm');
Route::post('administrator/DeleteCountry/{id}', 'Configuration\CountryController@DeleteCountry');
Route::post('administrator/ajaxAddCountry', 'Configuration\CountryController@ajaxAddCountry');
Route::post('administrator/ajaxEditCountry', 'Configuration\CountryController@ajaxEditCountry');

Route::post('displayListLanguages','Langs\LanguageController@ajaxListLanguages');
Route::get('editLanguage/{id}','Langs\LanguageController@EditLanguageForm');
Route::post('ajaxEditLang','Langs\LanguageController@ajaxEditLang');

Route::post('ajaxsaveConfiguration', 'Configuration\ConfigurationController@AjaxSaveConfiguration');
Route::post('administrator/ajaxSaveCompanyDetails', 'Configuration\CompanyDetailsController@SaveCompanyDetails');


Route::post('administrator/SaveSimpleTextInfo', 'Pages\RichTextController@SaveRichText');

Route::post('ajaxGenerateTranslation','Langs\LanguageController@ajaxGenerateTranslation');

Route::post('displayListAlbums', 'Pages\AlbumsController@AjaxListAlbums');
Route::get('administrator/AddAlbums/{page_id}', 'Pages\AlbumsController@AddAlbumForm');
Route::get('administrator/EditAlbums/{page_id}', 'Pages\AlbumsController@EditAlbumForm');
Route::post('administrator/DeleteAlbum/{ca_id}', 'Pages\AlbumsController@DeleteAlbumManager');
Route::post('administrator/RequestAddAlbum', 'Pages\AlbumsController@AjaxAddAlbum');
Route::post('administrator/RequestEditAlbum', 'Pages\AlbumsController@AjaxEditAlbum');
Route::delete('administrator/DeleteMedia/{id}', 'Media\MediaController@DeleteMedia');
Route::get('administrator/ViewAlbum/{page_id}/{ca_id}', 'Users\AdministratorController@listMediaAlbumManagement');

Route::post('displayListAlbumsDropdown', 'Pages\MediaAlbumsController@AlbumsDropdown');
Route::post('DeleteImageAlbum/{cm_id}', 'Pages\MediaAlbumsController@DeleteImageAlbum');
Route::get('AddImagesAlbum/{cp_id}/{ca_id}', 'Pages\MediaAlbumsController@AddImagesAlbumForm');
Route::get('EditImageAlbum/{cm_id}', 'Pages\MediaAlbumsController@EditImageAlbum');
Route::post('AjaxEditIAlbum', 'Pages\MediaAlbumsController@AjaxEditIAlbum');
Route::post('displayListImagesInAlbum', 'Pages\MediaAlbumsController@DisplayListImages');
Route::post('administrator/SaveMediaInformation', 'Pages\MediaAlbumsController@SaveMediaInformation');
Route::post('administrator/GetMediaInformation/{cm_id}', 'Pages\MediaAlbumsController@GetMediaInformation');


Route::post('aLogin','Auth\AuthController@Login');
Route::get('administrator/profile/{id}','Users\UsersController@editProfile');
Route::post('editProfile','Users\UsersController@ajaxEditProfile');
Route::post('displayListUsers','Users\UsersController@ajaxListUsers');
Route::get('addUser','Users\UsersController@addUserForm');
Route::get('editUser/{id}','Users\UsersController@editUserForm');
Route::post('DeleteUser','Users\UsersController@ajaxDeleteUser');
Route::post('ajaxAddUser','Users\UsersController@ajaxaddUser');
Route::post('ajaxEditUser','Users\UsersController@ajaxEditUser');

Route::post('displayListRoles','Roles\RolesController@ajaxListRoles');
Route::get('addRole','Roles\RolesController@AddRoleForm');
Route::post('ajaxAddRole','Roles\RolesController@ajaxAddRole');
Route::get('editRole/{id}','Roles\RolesController@EditRoleForm');
Route::post('ajaxEditRole','Roles\RolesController@ajaxEditRole');
Route::post('DeleteRole','Roles\RolesController@ajaxDeleteRole');


Route::get('administrator/dashboard','Dashboard\DashboardController@Index');

Route::post('ajaxGenerateTranslation','Langs\LanguageController@ajaxGenerateTranslation');


Route::post('displayListMenus','Menu\MenuController@ajaxListMenus');
Route::get('administrator/AddMenu','Menu\MenuController@AddNewMenu');
Route::post('administrator/DeleteMenu/{id}','Menu\MenuController@DeleteMenu');
Route::get('administrator/EditMenu/{id}','Menu\MenuController@EditMenu');
Route::post('administrator/AddMenuOperation','Menu\MenuController@ajaxAddMenuInformation');
Route::post('administrator/EditMenuOperation','Menu\MenuController@ajaxEditMenuInformation');


Route::post('displayListEvents','Pages\EventsController@AjaxListEvents');
Route::get('AddEvents/{PageId}','Pages\EventsController@AddEventForm');
Route::post('AjaxAddEvent','Pages\EventsController@AjaxAddEvent');
Route::get('EditEvents/{Id}','Pages\EventsController@EditEventForm');
Route::post('AjaxEditEvent','Pages\EventsController@AjaxEditEvent');
Route::post('DeleteEvent/{Id}','Pages\EventsController@DeleteEventManager');


Route::post('administrator/displayListRichText','Pages\RichTextController@ajaxListPages');
Route::get('administrator/AddRT/{PageId}','Pages\RichTextController@AddRTForm');
Route::get('administrator/EditRT/{id}','Pages\RichTextController@EditRTForm');
Route::post('administrator/ajaxAddRichText','Pages\RichTextController@ajaxAddRichTextContent');
Route::post('administrator/ajaxEditRichText','Pages\RichTextController@ajaxRichTextEditPage');
Route::post('administrator/deleteRichText','Pages\RichTextController@ajaxDeleteRichTextEditor');

Route::get('SimpleTextManagement', 'Users\AdministratorController@SimpleTextManagement');
Route::post('displayListSimpleText','Pages\SimpleTextController@ajaxListSimpleText');
Route::get('AddST','Pages\SimpleTextController@AddSTForm');
Route::get('EditST/{id}','Pages\SimpleTextController@EditSTForm');
Route::post('ajaxAddSimpleText','Pages\SimpleTextController@ajaxAddSimpleTextContent');
Route::post('ajaxEditSimpleText','Pages\SimpleTextController@ajaxEditSimpleText');
Route::post('deleteSimpleText','Pages\SimpleTextController@ajaxDeleteSimpleText');


Route::post('displayListPages','Pages\PageController@ajaxListPages');
Route::get('addPage/{MenuId}','Pages\PageController@AddPageForm');
Route::get('editPage/{id}','Pages\PageController@EditPageForm');
Route::post('AjaxAddPage','Pages\PageController@ajaxAddPage');
Route::post('ajaxEditPage','Pages\PageController@ajaxEditPage');
Route::post('ajaxDeletePage','Pages\PageController@ajaxDeletePage');


Route::post('displayListSingleImage','Pages\SingleImageController@ajaxListPages');
Route::get('addSImage/{PageId}','Pages\SingleImageController@AddSImageForm');
Route::get('editSImage/{RowId}','Pages\SingleImageController@EditSImageForm');
Route::post('AjaxAddSImage','Pages\SingleImageController@ajaxAddSImage');
Route::post('AjaxEditSImage','Pages\SingleImageController@ajaxEditSImage');
Route::post('ajaxDeleteSImage','Pages\SingleImageController@ajaxDeleteSImage');


Route::post('displayListLanguages','Langs\LanguageController@ajaxListLanguages');
Route::get('editLanguage/{id}','Langs\LanguageController@EditLanguageForm');
Route::post('ajaxEditLang','Langs\LanguageController@ajaxEditLang');


Route::get('/', 'FrontEnd\IndexController@index');

/**

Route::get('home', 'IndexController@index');
Route::get('about-us', 'IndexController@aboutUs');
Route::get('contact-us', 'IndexController@contactUs');
Route::get('administration', 'IndexController@administration');
Route::get('admission', 'IndexController@admissions');
Route::get('admission/{PageName}', 'IndexController@admissions');
Route::get('announcements', 'IndexController@announcements');
Route::get('academic-life', 'IndexController@academicLife');
Route::get('alumni', 'IndexController@alumni');
Route::get('calendar', 'IndexController@calendar');
Route::get('careers', 'IndexController@careers');

Route::post('send-contact-us', 'IndexController@submitContact');
Route::post('send-careers', 'IndexController@submitCareers');

Route::get('{langID}/home', 'IndexController@index');
Route::get('{langID}/about-us', 'IndexController@aboutUs');
Route::get('{langID}/contact-us', 'IndexController@contactUs');
Route::get('{langID}/administration', 'IndexController@administration');
Route::get('{langID}/admission', 'IndexController@admissions');
Route::get('{langID}/admission/{PageName}', 'IndexController@admissions');
Route::get('{langID}/announcements', 'IndexController@announcements');
Route::get('{langID}/academic-life', 'IndexController@academicLife');
Route::get('{langID}/alumni', 'IndexController@alumni');
Route::get('{langID}/calendar', 'IndexController@calendar');
Route::get('{langID}/careers', 'IndexController@careers');
Route::get('{langID}/gallery', 'IndexController@gallery');
Route::get('{langID}/distinction/{PageID}', 'IndexController@distinction');
Route::get('{langID}/testimonial/{PageID}', 'IndexController@testimonial');
Route::get('{langID}/events', 'IndexController@events');
Route::get('{langID}/sitemap', 'IndexController@sitemap');
Route::post('ajax/events', 'IndexController@ajaxEvents');
Route::get('{langID}/search/{searchQuery}', 'IndexController@search');


*/



Route::post('upload/{Index}', 'Utilities\UploaderController@Index');

Route::group(array('before' => 'auth'), function() {
    Route::get('/account/sign-out', array(
    'as' => 'account-sign-out',
    'uses' => 'Users\UsersController@getSignOut'
        ));

});