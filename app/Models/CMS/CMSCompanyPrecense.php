<?php
/***********************************************************
CMSContactUs.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use DB;

class CMSCompanyPrecense extends Model
{
    protected   $table          = 'cms_company_precense';
    public      $timestamps     = false;
    protected   $primaryKey     = "cp_id";

    public static function getIdField()
    {
        $model = new CMSCompanyPrecense;

        return $model->primaryKey;
    }


    public static function SaveInfo($input)
    {
        if( isset($input['id']) )
        {
            $model = self::find($input['id']);
        }
        else
        {
            $model = new CMSCompanyPrecense;
        }

        $model->cp_latitude                  = $input['cp_latitude'];
        $model->cp_longitude                 = $input['cp_longitude'];
        $model->cp_company_precense_name     = $input['cp_company_precense_name'];
        $model->save();
        return $model['attributes']['cp_id'];
    }


    public static function getAll()
    {
        $listData = DB::table('cms_company_precense')->where('cp_is_deleted',0)->orderBy('cp_id', 'ASC')->get();
        return $listData;
    }

    public static function deleteRow($id)
    {
        if( session('user_id') > 0 )
        {
            $model = self::find($id);
            $model->cp_is_deleted   = 1;
            $model->cp_deleted_by   = session('user_id');
            $model->save();
        }
    }
}