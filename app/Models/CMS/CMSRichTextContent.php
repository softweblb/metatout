<?php
/***********************************************************
CMSRichTextContent.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 21, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
RichText Model functionality
***********************************************************/

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\SYSTEM\SYSTEMLanguages;
use DB;
use App\Http\Controllers\Pages\RichTextController;

class CMSRichTextContent extends Model
{
    protected   $table          = 'cms_rich_text_content';
    public      $timestamps     = false;
    protected   $primaryKey     = "rt_id";

    public static function getIdField()
    {
        $model = new CMSRichTextContent;

        return $model->primaryKey;
    }

    public static function getTableName()
    {
        $model = new CMSRichTextContent;

        return $model->table;
    }



    public static function GetRichTextAdded( $page_id , $lang_id )
    {
        $table = self::getTableName();

        $cms_rich_text  = DB::table($table)->where('fk_page_id', $page_id)->where('fk_lang_id',$lang_id)->where('rt_is_active',1)->orderBy('rt_id', 'ASC')->get();

        return $cms_rich_text;
    }


    /**
     * get information about current page
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array Result Array
     */
    public static function getAllRichText( $page_id )
    {
        $table = self::getTableName();

        $cms_rich_text  = DB::table($table)->where('fk_page_id', $page_id)->orderBy('rt_id', 'ASC')->get();

        return $cms_rich_text;
    }


    /**
     * get richtext data array order by lang
     *
     * @author Moe Mantach
     * @access public
     *
     * @return Array $result_array
     */
    public static function getRichTextDataByLang( $page_id )
    {
        $table = self::getTableName();

        $cms_rich_text  = DB::table($table)->where('fk_page_id', $page_id)->orderBy('rt_id', 'ASC')->get();
        $languages_info = SYSTEMLanguages::getAllById();

        $cms_rt_array = array();

        for ($i = 0; $i < count($cms_rich_text); $i++)
        {
            $lang_code = $languages_info[ $cms_rich_text[$i]->fk_lang_id ]['language_code'];
            $cms_rt_array[ $lang_code ] = array(
                'rt_id' => $cms_rich_text[$i]->rt_id,
                'rt_title_page' => $cms_rich_text[$i]->rt_title_page,
                'rt_page_content' => $cms_rich_text[$i]->rt_page_content
            );
        }


        return $cms_rt_array;

    }


    public static function SaveRichTextContentPage($input)
    {
        if( isset($input['id']) )
        {
            $RichTextContent = self::find($input['id']);
        }
        else
        {
            $RichTextContent = new CMSRichTextContent;
        }

        $RichTextContent->fk_country_id            = $input['fk_country_id'];
        $RichTextContent->city_name                = $input['city_name'];
        $RichTextContent->save();
        return $RichTextContent['attributes']['id'];
    }

}


