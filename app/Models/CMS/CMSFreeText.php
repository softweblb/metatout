<?php
/***********************************************************
CMSFreeText.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 2, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Model related to the *cms_free_text*
***********************************************************/


namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use DB;

class CMSFreeText extends Model
{
    protected   $table          = 'cms_free_text';
    public      $timestamps     = false;
    protected   $primaryKey     = "ft_id";



    /**
     * Save Info to the table cms_free_text in the database
     *
     * @author Moe Mantach
     * @access public
     * @param Array $input
     *
     * @return Array $result_array
     */
    public static function saveInfo( $input )
    {
        if( isset($input['id']) )
        {
            $cmsFreeText = self::find($input['id']);
        }
        else
        {
            $cmsFreeText = new CMSFreeText;

        }
        if(isset($input['fk_lang_id']))
        $cmsFreeText->fk_lang_id                = $input['fk_lang_id'];
        if(isset($input['ft_row_id']))
        $cmsFreeText->ft_row_id                 = $input['ft_row_id'];
        $cmsFreeText->ft_title_text                 = $input['ft_title_text'];
        $cmsFreeText->ft_text_content               = $input['ft_text_content'];
        $cmsFreeText->save();
        return $cmsFreeText['attributes']['ft_id'];
    }

    /**
     * Delete row from the database
     *
     * @author Moe Mantach
     * @param Integer $id
     *
     * @return Array
     */
    public static function deleteRow($id)
    {
        $cms_free_text = new CMSFreeText;
        if( session('user_id') > 0 )
        {
            $cmsFreeText = self::FindFreeText($id);
            for ($i = 0; $i < count($cmsFreeText); $i++)
            {

                $cms_free_text = self::find($cmsFreeText[$i]->ft_id);
                $cms_free_text->ft_is_deleted   = 1;
                $cms_free_text->ft_deleted_by   = session('user_id');
                $cms_free_text->save();
            }
        }
    }


    public static function getNextRowId()
    {
        $ft_row_id          = DB::table('cms_free_text')->max('ft_row_id') +  1;
        return $ft_row_id;
    }

    /**
     * get array contain list of free text added to the database by language Id
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $lang_id
     *
     * @return Array
     */
    public static function getFreeTextByLang( $lang_id )
    {
        $list_free_text = DB::table('cms_free_text')->where('fk_lang_id',$lang_id)->where('ft_is_deleted',0)->orderBy('ft_id', 'ASC')->get();

        return $list_free_text;
    }


    /**
     * Get Free Text information for a selected row
     *
     * @author Moe mantach
     * @param Integer $id
     * @return unknown
     */
    public static function FindFreeText( $id )
    {
        $free_text_info = DB::table('cms_free_text')->where('ft_row_id',$id)->get();
        return $free_text_info;
    }


    public static function GetRowTextByLang( $id )
    {
        $free_text_info = DB::table('cms_free_text')->where('ft_row_id',$id)->get();

        $result_array = array();

        for ($i = 0; $i < count($free_text_info); $i++) {
            $result_array[$free_text_info[$i]->fk_lang_id] = array(
                'ft_id' => $free_text_info[$i]->ft_id,
                'ft_row_id' => $free_text_info[$i]->ft_row_id,
                'ft_title_text' => $free_text_info[$i]->ft_title_text,
                'ft_text_content' => $free_text_info[$i]->ft_text_content,
                'ft_is_deleted' => $free_text_info[$i]->ft_is_deleted,
                'ft_deleted_by' => $free_text_info[$i]->ft_deleted_by
            );
        }

        return $result_array;
    }

}