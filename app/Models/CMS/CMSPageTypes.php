<?php
/***********************************************************
CMSPageTypes.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 21, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use DB;

class CMSPageTypes extends Model
{
    protected   $table          = 'cms_page_type';
    public      $timestamps     = false;
    protected   $primaryKey     = "cs_id";

    const PAGE_TYPE_RICHTEXT              = 1;
    const PAGE_TYPE_LIST_RICHTEXT         = 2;
    const PAGE_TYPE_ALBUMS                = 3;
    const PAGE_TYPE_SLIDERS               = 4;
    const PAGE_TYPE_SINGE_IMAGE_PLUS_TEXT = 5;
    const PAGE_TYPE_FORMS                 = 6;
    const PAGE_TYPE_CONTACTUS             = 7;
    const PAGE_TYPE_CALENDAR              = 8;
    const PAGE_TYPE_SIMPLE_TEXT           = 9;
    const PAGE_TYPE_CUSTOM_PAGE           = 10;
    const PAGE_TYPE_LIST_IMAGES           = 11;


    public static function getAll()
    {
        $list_page_types = DB::table('cms_page_type')->orderBy('cs_id', 'ASC')->get();

        return $list_page_types;
    }

    /**
     * Save page type if it's active or not to the database
     *
     * @author Moe Mantach
     * @access public
     * @param Array $input
     */
    public static function SavePageTypeInformation($input)
    {
        if( isset($input['id']) )
        {
            $CMSPageTypes = self::find($input['id']);
        }
        else
        {
            $CMSPageTypes = new CMSPageTypes;
        }

        $CMSPageTypes->cs_is_active            = $input['cs_is_active'];
        $CMSPageTypes->save();
        return $CMSPageTypes['attributes']['cs_id'];
    }
}