<?php
/***********************************************************
CMSMenu.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 19, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Model related to the cms_menu
***********************************************************/

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;

class CMSMenu extends Model
{
    protected   $table          = 'cms_menu';
    public      $timestamps     = false;
    protected   $primaryKey     = "cm_id";

    public static function getIdField()
    {
        $model = new CMSMenu;

        return $model->primaryKey;
    }

    public static function saveMenuInformation($input)
    {
        if( isset($input['id']) &&  $input['id'] > 0 )
        {
            $cms_menu = CMSMenu::find($input['id']);
        }
        else
        {
            $cms_menu = new CMSMenu;
        }

        $cms_menu->cm_menu_id           = $input['cm_menu_id'];
        $cms_menu->cm_menu_position     = $input['cm_menu_position'];
        $cms_menu->cm_is_active         =$input['cm_is_active'];
        $cms_menu->save();
    }

    public static function deleteRow($id)
    {
        if( session('user_id') > 0 )
        {
            $form_category = self::find($id);
            $form_category->cm_is_deleted   = 1;
            $form_category->cm_deleted_by   = session('user_id');
            $form_category->save();
        }
    }

    public static function whereCmIsDeleted( $cm_is_deleted )
    {
        $cms_menu = DB::table('cms_menu')->where('cm_is_deleted', '=', $cm_is_deleted )->get();

        return $cms_menu;
    }
}
