<?php
/***********************************************************
SYSTEMUsers.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 30, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Models\SYSTEM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;


class SYSTEMUsers extends Model
{
    protected   $table          = 'users';
    public      $timestamps     = false;
    protected   $primaryKey     = "id";
}