<?php
/***********************************************************
SYSTEMCompanyDetails.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 27, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Company Details Model
***********************************************************/





namespace App\Models\SYSTEM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Config;
use Session;

class SYSTEMCompanyDetails extends Model
{
    protected   $table          = 'sys_company_details';
    public      $timestamps     = false;
    protected   $primaryKey     = "cd_id";

    public static function getIdField()
    {
        $model = new SYSTEMCompanyDetails;

        return $model->primaryKey;
    }


    public static function saveInformation($input)
    {
        if( isset($input['cd_id']) &&  $input['cd_id'] > 0 )
        {
            $sys_company_details = SYSTEMCompanyDetails::find($input['cd_id']);
        }
        else
        {
            $sys_company_details = new SYSTEMCompanyDetails;
            $input['cd_id'] = 1;
        }

        $sys_company_details->cd_company_name       = $input['cd_company_name'];
        $sys_company_details->cd_company_brief      = $input['cd_company_brief'];
        $sys_company_details->cd_company_website    = $input['cd_company_website'];
        $sys_company_details->cd_company_email      = $input['cd_company_email'];
        $sys_company_details->cd_comapny_phone      = $input['cd_comapny_phone'];
        $sys_company_details->cd_company_mobile     = $input['cd_company_mobile'];
        $sys_company_details->fk_country_id         = $input['fk_country_id'];
        $sys_company_details->cd_id         = $input['cd_id'];
        $sys_company_details->save();
    }



    public static function GetLogoUrl()
    {
        $mod = new SYSTEMCompanyDetails();
        $sys_company_details = SYSTEMCompanyDetails::find(1);
        $logo_url = url() . "/". Config::get( 'constants.COMPANY_LOGO_IMAGE_PATH') . $sys_company_details['attributes']['cd_company_logo_filename'] . "." . $sys_company_details['attributes']['cd_company_logo_extention'];
        if($sys_company_details['attributes']['cd_company_logo_filename'] == '')
        {
            $logo_url = url() . "/assets/imgs/logo.png";
        }
        return $logo_url;
    }

}