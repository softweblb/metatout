<?php
/***********************************************************
SYSTEMLanguages.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 21, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

namespace App\Models\SYSTEM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class SYSTEMLanguages extends Model
{
    protected   $table          = 'sys_languages';
    public      $timestamps     = false;
    protected   $primaryKey     = "sl_id";

    public static function getIdField()
    {
        $model = new SYSTEMLanguages;

        return $model->primaryKey;
    }

    public static function getAll()
    {
        $listLanguages     = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        return $listLanguages;
    }

    public static function getLangInfoByCode( $lan_code )
    {
        $lang_information  = DB::table('sys_languages')->where('sl_language_code', $lan_code )->get();
        return $lang_information;
    }


    public static function getAllById()
    {
        $listLanguages      = DB::table('sys_languages')->where('sl_is_active', 1)->get();
        $languages_info = array();

        for ($i = 0; $i < count($listLanguages); $i++)
        {
            $languages_info[ $listLanguages[$i]->sl_id ]['language_title']          = $listLanguages[$i]->sl_language_title;
            $languages_info[ $listLanguages[$i]->sl_id ]['language_code']           = $listLanguages[$i]->sl_language_code;
            $languages_info[ $listLanguages[$i]->sl_id ]['language_align']          = $listLanguages[$i]->sl_align;
            $languages_info[ $listLanguages[$i]->sl_id ]['language_reverse_align']  = $listLanguages[$i]->sl_reverse_align;
            $languages_info[ $listLanguages[$i]->sl_id ]['language_direction']      = $listLanguages[$i]->sl_direction;
        }

        return $languages_info;
    }


}