<?php
/***********************************************************
SYSTEMCountries.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 25, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/



namespace App\Models\SYSTEM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class SYSTEMCountries extends Model
{
    protected   $table          = 'country';
    public      $timestamps     = false;
    protected   $primaryKey     = "id";

    public static function getIdField()
    {
        $model = new SYSTEMCountries;

        return $model->primaryKey;
    }

    public static function getAll()
    {
        $listCountries      = DB::table('country')->get();
        return $listCountries;
    }


    public static function getAllById()
    {
        $listCountries      = DB::table('country')->get();
        $countries_info = array();

        for ($i = 0; $i < count($listCountries); $i++)
        {
            $countries_info[ $listCountries[$i]->id ]['id']            = $listCountries[$i]->id;
            $countries_info[ $listCountries[$i]->id ]['code']          = $listLanguages[$i]->code;
            $countries_info[ $listCountries[$i]->id ]['name']          = $listLanguages[$i]->name;
        }

        return $countries_info;
    }


    public static function saveInformation($input)
    {
        if( isset($input['id']) &&  $input['id'] > 0 )
        {
            $sys_country = SYSTEMCountries::find($input['id']);
        }
        else
        {
            $sys_country = new SYSTEMCountries;
        }

        $sys_country->code              = $input['code'];
        $sys_country->name              = $input['name'];
        $sys_country->save();
    }


    public static function deleteCountry( $id )
    {
        DB::table('country')->where('id', $id)->delete();

        return true;
    }


}