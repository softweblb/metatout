<?php
/***********************************************************
CMSPages.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 21, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Media\WEBMedia;
use App\Models\CMS\CMSRichTextContent;
use App\Models\CMSAlbums;

class CMSPages extends Model
{
    protected   $table          = 'cms_pages';
    public      $timestamps     = false;
    protected   $primaryKey     = "cp_id";


    public static function getIdField()
    {
        $model = new CMSPages;

        return $model->primaryKey;
    }


    /**
     * Get list of pages by condition if its active or not
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $cp_is_active
     * @return Object
     */
    public static function getListPagesByActive( $cp_is_active )
    {
        $pages = DB::table('cms_pages')->where('cp_is_active', $cp_is_active )->orderBy('cp_id', 'ASC')->get();
        return $pages;
    }


    /**
     * get list of pages by page_type
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $page_type
     * @return Object database_object
     */
    public static function getPageByPageType( $page_type )
    {
        $pages = DB::table('cms_pages')->where('cp_page_type', $page_type )->orderBy('cp_id', 'ASC')->get();
        return $pages;
    }


    public static function getPageByPageMenuTitle( $menu_page_title )
    {
        $pages = DB::table('cms_pages')->where('cp_page_menu_title', $menu_page_title )->orderBy('cp_id', 'ASC')->get();
        return $pages;
    }


    public static function getListofPageTitle()
    {
        $pages = self::getListPagesByActive(1);
        $result_array = array();

        for ($i = 0; $i < count($pages); $i++)
        {
            $result_array[ $pages[$i]->cp_id ] = $pages[$i]->cp_page_menu_title;
        }

        return $result_array;
    }


    /**
     * Get data related to the selected page type
     *
     * @author Moe Mantach
     * @access public
     * @param Integer $cp_id
     */
    public static function getDataRelatedToSelectedPage( $cp_title , $sl_id )
    {
        $lst_data_info = array();
        switch ($cp_title)
        {
            case "homepage":
                {
                    $page_info = self::getPageByPageMenuTitle($cp_title);
                    $cp_id = $page_info[0]->cp_id;
                    $lst_data_info = WEBMedia::getMediaInfoByPageId($cp_id , $sl_id);
                }
            break;
            case "aboutus":
                {
                    $page_info = self::getPageByPageMenuTitle($cp_title);
                    $cp_id = $page_info[0]->cp_id;
                    $lst_data_info = CMSRichTextContent::GetRichTextAdded($cp_id , $sl_id);
                }
            break;
            case "scope":
                {
                    $page_info = self::getPageByPageMenuTitle($cp_title);
                    $cp_id = $page_info[0]->cp_id;
                    $lst_data_info = WEBMedia::getMediaInfoByPageId($cp_id , $sl_id);
                }
            break;
            case "clients":
                {
                    $page_info = self::getPageByPageMenuTitle($cp_title);
                    $cp_id = $page_info[0]->cp_id;
                    $lst_data_info = WEBMedia::getMediaInfoByPageId($cp_id , $sl_id);
                }
            break;
            case "projects":
                {
                    $page_info = self::getPageByPageMenuTitle($cp_title);
                    $cp_id = $page_info[0]->cp_id;
                    $lst_data_info = CMSAlbums::getAlbums($cp_id , $sl_id);
                }
            break;
        }


        return $lst_data_info;
    }

}
