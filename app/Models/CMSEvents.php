<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSEvents extends Model
{
    protected   $table          = 'cms_events';
    public      $timestamps     = false;
    protected   $primaryKey     = "ce_id";
}
