<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSTextContent extends Model
{
    protected   $table          = 'cms_text_content';
    public      $timestamps     = false;
    protected   $primaryKey     = "cc_id";
}
