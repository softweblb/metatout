<?php
/***********************************************************
CMSAlbums.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 16, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
model for CMSAlbum pages functionality
***********************************************************/


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;

class CMSAlbums extends Model
{
    protected   $table          = 'cms_albums';
    public      $timestamps     = false;
    protected   $primaryKey     = "ca_id";



    /**
     * get list of all albums saved in this section with content of every albums
     *
     * @author Moe Mantach
     * @param integer $cp_id
     * @param integer $sl_id
     *
     * @return Array $media_albums_info
     */
    public static function getAlbums($cp_id , $sl_id)
    {
        $lst_albums = DB::table('cms_albums')->where('fk_page_id', $cp_id)->where('ca_is_deleted',0)->orderBy('ca_id', 'ASC')->get();
        $media_albums_info = array();
        for ($i = 0; $i < count($lst_albums); $i++)
        {
            $ca_id = $lst_albums[$i]->ca_id;
            $lst_media_albums = DB::table('cms_media')->where('fk_album_id', $ca_id)->where('cm_is_deleted',0)->where('cm_media_is_active',1)->get();

            $media_albums_info[ $i ]['id']      = $ca_id;
            $media_albums_info[ $i ]['title']   = $lst_albums[$i]->ca_album_title;
            $media_albums_info[ $i ]['caption'] = $lst_albums[$i]->ca_album_caption;
            for ($j = 0; $j < count($lst_media_albums); $j++)
            {
                $media_albums_info[ $i ]['images'][$j]['title']     = $lst_media_albums[$j]->cm_media_title;
                $media_albums_info[ $i ]['images'][$j]['caption']   = $lst_media_albums[$j]->cm_media_caption;
                $media_albums_info[ $i ]['images'][$j]['image']     = url()  . "/" . Config::get('constants.ALBUMS_PATH') .  $lst_media_albums[$j]->cm_media_base_dir . $lst_media_albums[$j]->cm_media_file_name . "." . $lst_media_albums[$j]->cm_media_file_extention ;
            }

        }

        return $media_albums_info;

    }

}
