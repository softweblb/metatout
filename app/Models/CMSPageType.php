<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSPageType extends Model
{
    protected   $table          = 'cms_page_type';
    public      $timestamps     = false;
    protected   $primaryKey     = "cs_id";
}
