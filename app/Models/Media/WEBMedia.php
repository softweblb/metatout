<?php
/***********************************************************
WEBMedia.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 4, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Model for media table
***********************************************************/

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;
use DB;

class WEBMedia extends Model
{
    protected   $table          = 'cms_media';
    public      $timestamps     = false;
    protected   $primaryKey     = "cm_id";

    /**
     * update is_deleted to 1 to the selected row
     *
     * @author Moe Mantach
     * @access public
     * static
     * @param Integer $id
     *
     * @return void
     */
    public static function deleteRow($id)
    {
        if( session('user_id') > 0 )
        {
            $webmedia = self::find($id);
            $webmedia->cm_is_deleted   = 1;
            $webmedia->cm_deleted_by   = session('user_id');
            $webmedia->save();
        }
    }



    public static function SaveMediaInformation( $input )
    {
        if( isset($input['id']) )
        {
            $media = self::find($input['id']);
        }
        else
        {
            $media = new WEBMedia;
        }

        $media->cm_media_title              = $input['cm_media_title'];
        $media->cm_media_caption            = $input['cm_media_caption'];
        $media->save();

        return $media['attributes']['cm_id'];
    }


    public static function getMediaInfoByPageId( $Id  , $lang_id)
    {
        $medias =  DB::table('cms_media')->where('fk_page_id', $Id )->where('cm_is_deleted', 0 )->where('cm_media_is_active', 1 )->where('fk_lang_id',$lang_id )->orderBy('cm_id', 'ASC')->get();
        return $medias;
    }
}