<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSMedia extends Model
{
    protected   $table          = 'cms_media';
    public      $timestamps     = false;
    protected   $primaryKey     = "cm_id";
}
