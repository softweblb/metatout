<?php
/************************************************************
menus.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 10, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
display page were we can Add/Edit and delete a Menu
************************************************************/

?>
@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/menus/menusmanagement.js"></script>
@endsection
@section('content')
<div class="PageHeader">
  List Menus
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
         <div class="col-md-2">
            <select name="cm_is_active" id="CM_IS_ACTIVE" style="width:100%" class="form-control">
                  <option value="1">Active</option>
                  <option value="0">Not Active</option>
            </select>
         </div>
         <div class="col-md-10">
         </div>
     </div>
     <div class="row">
         <div class="col-md-12" style="height:10px;">
         </div>
     </div>
     <div class="row">
         <div class="col-md-12">
             <div style="left:20%" class="ListMenuGirds">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_menu" id="BTN_ADD_MENU" value="ADD" />
         </div>
     </div>
</div>
@endsection