<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 10, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Grid of all menues added to the database
************************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Menu ID</th>
            <th>Page Count</th>
            <th style="width:1%" nowrap>Edit</th>
            <th style="width:1%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($menus as $index => $menu_info)
        {
            ?>
               <tr data-cm_id="<?php echo $menu_info->cm_id; ?>">
                   <td></th>
                   <td><?php echo $menu_info->cm_menu_id; ?></th>
                   <td><?php echo $menu_info->cm_pages_count; ?></td>
                   <td><img style="cursor: pointer;" height="16" id="EDIT_MENU_<?php echo $menu_info->cm_id; ?>" src="{{ url('/assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer;" height="16" id="DELETE_MENU_<?php echo $menu_info->cm_id; ?>" src="{{ url('/assets/imgs/delete.png') }}" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>