<?php
/***********************************************************
editmenu.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 20, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/menus/editMenu.js"></script>
@endsection
@section('content')

<form name="form_edit_menu" id="FORM_EDIT_MENU">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="cm_id" id="CM_ID" value="<?php echo $id; ?>" />
</span>
    <div class="container-fluid" style="width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Menu</h3></div>
        </div>
       <div class="row">
            <div class="col-md-12" align="left">
                <label>Menu Id</label>
                <span>
                    <input type="text" name="cm_menu_id" id="CM_MENU_ID" maxlength="255" style="width:100%;" value="<?php echo $cms_menu['cm_menu_id']; ?>" class="form-control" />
                </span>
            </div>
       </div>
       <div class="row">
            <div class="col-md-12"  align="left">
                 <label>Menu Position</label>
                 <span>
                    <select name="cm_menu_position" id="CM_MENU_POSITION" style="width:100%" class="form-control">
                            <option <?php echo $cms_menu['cm_menu_position'] == 1 ? "selected" : ""; ?> value="1"> TOP MENU </option>
                            <option <?php echo $cms_menu['cm_menu_position'] == 2 ? "selected" : ""; ?> value="2"> BOTTOM MENU </option>
                            <option <?php echo $cms_menu['cm_menu_position'] == 3 ? "selected" : ""; ?> value="3"> LEFT MENU </option>
                    </select>
                 </span>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
                    <div class="col-md-12"  align="left">
                 <label><input type="checkbox" name="cm_is_active" id="CM_IS_ACTIVE" <?php echo $cms_menu['cm_is_active'] == 1 ? "checked" : ""; ?> value="1" /> Is Active</label>
                 <span>
                 </span>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_menu" id="BTN_EDIT_MENU" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection