<?php
/************************************************************
companydetails.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Edit Company Details Info
************************************************************/


{/** Data retrivel block */
    $cd_id                  = isset( $sys_company_details[0]->cd_id ) ? $sys_company_details[0]->cd_id : 0;
    $cd_company_name        = isset( $sys_company_details[0]->cd_company_name ) ? $sys_company_details[0]->cd_company_name : '';
    $cd_company_brief       = isset( $sys_company_details[0]->cd_company_brief ) ? $sys_company_details[0]->cd_company_brief : '';
    $cd_company_website     = isset( $sys_company_details[0]->cd_company_website ) ? $sys_company_details[0]->cd_company_website : '';
    $cd_company_email       = isset( $sys_company_details[0]->cd_company_email ) ? $sys_company_details[0]->cd_company_email : '';
    $cd_comapny_phone       = isset( $sys_company_details[0]->cd_comapny_phone ) ? $sys_company_details[0]->cd_comapny_phone : '';
    $cd_company_mobile      = isset( $sys_company_details[0]->cd_company_mobile ) ? $sys_company_details[0]->cd_company_mobile : '';
    $fk_country_id          = isset( $sys_company_details[0]->fk_country_id ) ? $sys_company_details[0]->fk_country_id : 0;
    $cd_company_city        = isset( $sys_company_details[0]->fk_city_id ) ? $sys_company_details[0]->fk_city_id : 0;
}

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('assets/js/administrator/companydetails.js') }}"></script>
@endsection

@section('content')


<form name="form_add_companydetails" id="FORM_ADD_COMPANYDETAILS">
    <span id="hidden_fields">
        {!! csrf_field() !!}
        <input type="hidden" name="cd_id" id="CD_ID" value="<?php echo $cd_id; ?>" />
    </span>
    <div class="container-fluid">
         <div class="row">
            <div class="col-md-12"><h3>Company Details</h3></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>Company Name</label>
                <input type="text" name="cd_company_name" id="CD_COMPANY_NAME" class="form-control" value="<?php echo $cd_company_name; ?>" />
            </div>
            <div class="col-md-4">
                <label>Company Brief</label>
                <textarea name="cd_company_brief" id="CD_COMPANY_BRIEF" style="resize:none;" class="form-control" ><?php echo $cd_company_brief; ?></textarea>
            </div>
            <div class="col-md-4">
                <label>Company Website</label>
                <input type="url" name="cd_company_website" id="CD_COMPANY_WEBSITE" class="form-control" value="<?php echo $cd_company_website; ?>" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
       <div class="row">
            <div class="col-md-4">
                <label>Company Email</label>
                <input type="email" name="cd_company_email" id="CD_COMPANY_EMAIL" class="form-control" value="<?php echo  $cd_company_email; ?>" />
            </div>
            <div class="col-md-4">
                 <label>Company Phone</label>
                <input type="tel" name="cd_comapny_phone" id="CD_COMPANY_PHONE" class="form-control" value="<?php echo $cd_comapny_phone; ?>" />
            </div>
            <div class="col-md-4">
                <label>Company Mobile</label>
                <input type="text" name="cd_company_mobile" id="CD_COMPANY_MOBILE" class="form-control" value="<?php echo $cd_company_mobile; ?>" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-md-4">
                <label>Company Country</label>
                <select name="fk_country_id" id="FK_COUNTRY_ID" class="form-control" style="width:100%">
                    <option value="0">--Select One--</option>
                    <?php
                        for ($i=0;$i<count($lstCmsCountries);$i++)
                        {
                            ?>
                                <option <?php echo ($fk_country_id == $lstCmsCountries[$i]->id) ? "selected" : ""; ?> value="<?php echo $lstCmsCountries[$i]->id; ?>"><?php echo $lstCmsCountries[$i]->code . "-" . $lstCmsCountries[$i]->name; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
         <div class='row'>
                <div class="col-md-12">
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select files...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" data-url="<?php echo url(); ?>/upload/companylogo" type="file" name="files" />
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <img src="<?php echo $companyLogoLink; ?>" width="100" ID="COMPANY_LOGO_PICTURE" />
                </div>
            </div>
            <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2"><input type="button" name="btn_save_company_info" id="BTN_SAVE_COMPANY_INFO" class="btn btn-primary" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection