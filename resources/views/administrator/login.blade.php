<?php
/************************************************************
alogin.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>{{ $request->cookie('sw_company_name') !== null ? $request->cookie('sw_company_name') : "metaout" }} - Administrator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 <script src="<?php echo url();?>/assets/js/jquery-1.11.2.min.js"></script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo url();?>/assets/admin/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'material design' style just load 'components-md.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo url();?>/assets/admin/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo url();?>/assets/admin/assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo url();?>/assets/admin/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo url();?>/assets/js/app.js"></script>
        <script src="<?php echo url();?>/assets/js/administrator/login.js"></script>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a>
	<img src="assets/imgs/logo.png" width="250" />
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->

	<form name="LogInForm" id="LogInForm" method="post">
       {!! csrf_field() !!}
		<h3 class="form-title" style="text-transform: uppercase;" >{{ $request->cookie('sw_company_name') !== null
		 ? $request->cookie('sw_company_name') : "metaout" }}</h3>
		<div id="ALERT_BOX" class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span class='TextAlert'>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<input type="text" name="u_username" id="U_USERNAME" placeholder="Username" class="form-control form-control-solid placeholder-no-fix" value="" />
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" name="u_password" id="U_PASSWORD" autocomplete="off" placeholder="Password" />
		</div>
		<div class="form-actions">
			<button type="button" name="btn_login" id="BTN_LOGIN" class="btn btn-success uppercase">Login</button>
			<label class="rememberme check">
			<input type="checkbox" name="remember" value="1"/>Remember </label>
			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
	</form>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" action="index.html" method="post">
		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">Back</button>
			<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
</div>
<div class="copyright">
	 <?php echo date("Y"); ?> &copy; {{ $request->cookie('sw_company_name') !== null ? $request->cookie('sw_company_name') : "metaout" }}
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo url();?>/assets/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo url();?>/assets/admin/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo url();?>/assets/admin/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>