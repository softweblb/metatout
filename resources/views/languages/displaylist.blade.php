<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 23, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Language Code</th>
            <th>Title</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($languages as $index => $lang_info)
        {
            ?>
               <tr data-lang_id="<?php echo $lang_info->sl_id; ?>">
                   <td></th>
                   <td><?php echo $lang_info->sl_language_code; ?></th>
                   <td><?php echo $lang_info->sl_language_title; ?></td>
                   <td><img style="cursor: pointer;" height="16" id="EDIT_LANG_<?php echo $lang_info->sl_id; ?>" src="assets/imgs/edit.png" /></td>
                   <td><img style="cursor: pointer;display:none;" height="16" id="DELETE_MENU_<?php echo $lang_info->sl_id; ?>" src="assets/imgs/delete.png" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>