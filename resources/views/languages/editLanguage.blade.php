<?php
/************************************************************
editLanguage.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 23, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

$lang_info = $lang_info[0];
?>




@extends('layouts.playout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/langs/editlang.js"></script>

<form name="form_edit_language" id="FORM_EDIT_LANGUAGE">
    <span id="hidden_fields">
        {!! csrf_field() !!}
        <input type="hidden" name="sl_id" value="<?php echo $lang_info->sl_id; ?>" />
    </span>
    <div class="container-fluid" style="width:500px;">
         <div class="row">
            <div class="col-sm-12"><h3>Edit Language</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Language Title</div>
            <div class="col-sm-10"><input type="text" name="lang_title" id="LANG_TITLE" class="form-control" value="<?php echo $lang_info->sl_language_title; ?>" /></div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Language Code</div>
            <div class="col-sm-10">
                <input type="text" class="form-control" size="4" name="lang_code" id="LANG_CODE" value="<?php echo $lang_info->sl_language_code; ?>"  />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Align</div>
            <div class="col-sm-10">
                <select class="form-control" name="lang_align" id="LANG_ALIGN">
                    <option <?php echo $lang_info->sl_align == 'left' ? "selected" : ""; ?> value="left" >left</option>
                    <option <?php echo $lang_info->sl_align == 'right' ? "selected" : ""; ?> value="right" >right</option>
                </select>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Reverse Align</div>
            <div class="col-sm-10">
                <select class="form-control" name="lang_reverse_align" id="LANG_REVERSE_ALIGN">
                    <option <?php echo $lang_info->sl_reverse_align == 'left' ? "selected" : ""; ?> value="left" >left</option>
                    <option <?php echo $lang_info->sl_reverse_align == 'right' ? "selected" : ""; ?> value="right" >right</option>
                </select>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Direction</div>
            <div class="col-sm-10">
            <select class="form-control" name="lang_direction" id="LANG_DIRECTION">
                    <option <?php echo $lang_info->sl_direction == 'ltr' ? "selected" : ""; ?> value="ltr" >ltr</option>
                    <option <?php echo $lang_info->sl_direction == 'rtl' ? "selected" : ""; ?> value="rtl" >rtl</option>
                </select>
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_edit_lang" id="BTN_EDIT_LANG" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection