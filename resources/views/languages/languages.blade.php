<?php
/************************************************************
languages.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 23, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Language Management Page
************************************************************/

?>

@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/langs/languagesmanagement.js"></script>
<div class="PageHeader">
   List Languages
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListlangGirds">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_user" id="BTN_ADD_USER" value="ADD" style="display:none" />
         </div>
     </div>
</div>
@endsection