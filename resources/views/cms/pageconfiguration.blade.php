<?php
/***********************************************************
pageconfiguration.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 28, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
***********************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('/assets/js/pages/pagetypesmanagement.js') }}"></script>
@endsection
@section('content')

<div class="PageHeader">
List Page Types
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
    <form name="frm_page_types" id="FRM_PAGE_TYPES">
        <div class="row"><div class="col-md-12">&nbsp;</div></div>
         <div class="row">
             <div class="col-md-12">
                 <div style="left:20%" align='left' class="ListAlbumsGrid">
                    <table class="table table-hover" style="width:100%">
                    <thead>
                        <tr bgcolor="#3c8dbc">
                            <th>#</th>
                            <th>active</th>
                            <th>Page Type</th>
                        </tr>
                    </thead>
                     <tbody>
                        @for($i = 0; $i < count($list_page_types); $i++)
                            <tr data-rt_id="{{ $list_page_types[$i]['attributes']['cs_id'] }}">
                               <td>{{ $list_page_types[$i]['attributes']['cs_id'] }}</td>
                               <td><input type="checkbox" name="ck_is_active_{{ $list_page_types[$i]['attributes']['cs_id'] }}" id="CK_IS_ACTIVE_{{ $list_page_types[$i]['attributes']['cs_id'] }}" value="1" {{ $list_page_types[$i]['attributes']['cs_is_active'] == 1 ? "checked" : "" }}  /></td>
                               <td>{{ $list_page_types[$i]['attributes']['cs_page_type'] }}</td>
                            </tr>
                        @endfor
                     </tbody>
                    </table>
                 </div>
             </div>
         </div>
         <div class="row">
            <div class="col-md-12" align="right">
                <button type="button" name="btn_save" id="BTN_SAVE" class="btn btn-info" > Save </button>
            </div>
         </div>
    </form>
</div>
@endsection