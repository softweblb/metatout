<?php
/***********************************************************
displaycompanyprecense.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th><input type="checkbox" name="ck_all" id="CK_ALL" value="1"  /></th>
            <th>Id</th>
            <th>Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
       @foreach( $lstCompanyPrecense as $cp_id => $cp_info )
               <tr data-cp_id="{{ $cp_info->cp_id }}">
                   <td><input type="checkbox" name="cp_company_{{ $cp_info->cp_id }}" id="CP_COMPANY_{{ $cp_info->cp_id }}" value="1"  /></td>
                   <td>{{ $cp_info->cp_id }}</td>
                   <td>{{ $cp_info->cp_company_precense_name }}</td>
                   <td>{{ $cp_info->cp_latitude }}</td>
                   <td>{{ $cp_info->cp_longitude }}</td>
                   <td><img style="cursor: pointer" height="16" id="EDIT_COMPANY_{{ $cp_info->cp_id }}" src="{{ url('assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer" height="16" id="DELETE_COMPANY_{{ $cp_info->cp_id }}" src="{{ url('assets/imgs/delete.png') }}"  /></td>
               </tr>
        @endforeach
    </tbody>
</table>