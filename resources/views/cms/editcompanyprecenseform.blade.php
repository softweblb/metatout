<?php
/***********************************************************
editcompanyprecenseform.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editcompanyprecense.js"></script>
@endsection

@section('content')
<form name="form_edit_company" id="FORM_EDIT_COMPANY">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="cp_id" id="CP_ID" value="{{ $cms_company_precense['attributes']['cp_id'] }}" />
</span>
    <div class="container-fluid" align="left">
         <div class="row">
            <div class="col-sm-12"><h3>Add Company Precense</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Company Precense Title</div>
            <div class="col-sm-10">
                <input type="text" name="cp_company_precense_name" id="CP_COMPANY_PRECENSE_NAME" class="form-control" value="{{ $cms_company_precense['attributes']['cp_company_precense_name'] }}" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Latitude</div>
            <div class="col-sm-10">
                <input type="text" name="cp_latitude" id="CP_LATITUDE" class="form-control" value="{{ $cms_company_precense['attributes']['cp_latitude'] }}" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Longitude</div>
            <div class="col-sm-10">
                <input type="text" name="cp_longitude" id="CP_LONGITUDE" class="form-control" value="{{ $cms_company_precense['attributes']['cp_longitude'] }}" />
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_edit_company" id="BTN_EDIT_COMPANY" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection