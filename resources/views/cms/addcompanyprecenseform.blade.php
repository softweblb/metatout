<?php
/***********************************************************
addcompanyprecenseform.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/addcompanyprecense.js"></script>
@endsection

@section('content')
<form name="form_add_company" id="FORM_ADD_COMPANY">
<span id="hidden_fields">
     {!! csrf_field() !!}
</span>
    <div class="container-fluid" align="left">
         <div class="row">
            <div class="col-sm-12"><h3>Add Company Precense</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Company Precense Title</div>
            <div class="col-sm-10">
                <input type="text" name="cp_company_precense_name" id="CP_COMPANY_PRECENSE_NAME" class="form-control" value="" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Latitude</div>
            <div class="col-sm-10">
                <input type="text" name="cp_latitude" id="CP_LATITUDE" class="form-control" value="" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Longitude</div>
            <div class="col-sm-10">
                <input type="text" name="cp_longitude" id="CP_LONGITUDE" class="form-control" value="" />
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_add_company" id="BTN_ADD_COMPANY" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection