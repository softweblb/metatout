<?php
/***********************************************************
aboutus.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 10, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
view for about us page
***********************************************************/


?>



<div id="title" class="AboutUsTextSection">
        <div style="width:100%;height: 80px;">&nbsp;</div>
        <div id="Textcontainer" style="height: 370px;">
            <div id="ImageFloated"><img class="img-responsives" src="{{ url('libraries/imgs/aboutus-block.png') }}" /></div>
                <div class="AboutusText">
                    <div style="position:relative;">
                        <?php echo $aboutus_info[0]->rt_page_content ?>
                    </div>
                    <br>
                </div>
                <div class="aboutusgray"></div>
        </div>
        <div style="width:100%;text-align: center;position:relative;z-index:10">
            <img style="top: -37px;position: relative;" class="BtnReadMore" src="{{ url('libraries/imgs/btnshowmore.png') }}" />
        </div>



</div>