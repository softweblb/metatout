<?php
/***********************************************************
contactus.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 16, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>
<div class="row">
    <div class="col-md-12"  style="padding-left: 0px;padding-right: 0px;">
        <div id="map" style="width:100%;height:400px;" ></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12"  style="padding-left: 0px;padding-right: 0px;">
        <div class="InformationSection">
            <div class="col-md-3" style="top: 9px">
                <ul class="LstContactUs" style="padding-left: 50px;">
                    <li style="padding-top:20px;"><img src="{{ url('libraries/imgs/phone.png') }}" /></li>
                    <li><img src="{{ url('libraries/imgs/line.jpg') }}" /></li>
                    <li style="color:white;padding-top:20px;">
                        <span div class="contactusf">+242 06 9999666</span><br/>
                    </li>
                </ul>
            </div>
            <div class="col-md-3" style="top: 9px">
                <ul class="LstContactUs">
                    <li style="padding-top:20px;"><img src="{{ url('libraries/imgs/email.png') }}" /></li>
                    <li><img src="{{ url('libraries/imgs/line.jpg') }}" /></li>
                    <li style="color:white;padding-top:20px;">
                        <span div class="contactusf">metatoutprefabs@gmail.com</span><br/>
                    </li>
                </ul>
            </div>
            <div class="col-md-3" style="top: 9px">
                <ul class="LstContactUs">
                    <li style="padding-top:20px;"><img src="{{ url('libraries/imgs/website.png') }}" /></li>
                    <li><img src="{{ url('libraries/imgs/line.jpg') }}" /></li>
                    <li style="color:white;padding-top:20px;">
                        <span div class="contactusf">www.metatoutprefab.com</span><br/>
                    </li>
                </ul>
            </div>
            <div class="col-md-3" style="top: 9px">
                <ul class="LstContactUs">
                    <li style="padding-top:20px;"><img src="{{ url('libraries/imgs/address.png') }}" /></li>
                    <li><img src="{{ url('libraries/imgs/line.jpg') }}" /></li>
                    <li style="color:white;max-width: 176px;bottom: 12px;">
                        <span div class="contactusf" style="width:271.51px;height: 40px;">
                            Avenue Kimbambo<br/> Diagonale Servtec<br/> Centre Ville de Pointe Noire
                            République du CONGO
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="MInformationSection row" style="padding-top:35px">
            <div class="row" >
                <div  style="margin: 0 auto;width: 265px;height: 67px;">
                    <div class="col-xs-1"><img style="top:13px;position:relative" src="{{ url('libraries/imgs/phone.png') }}" /></div>
                    <div class="col-xs-1"><img src="{{ url('libraries/imgs/line.jpg') }}"></div>
                    <div class="col-xs-9">
                        <span style="color:white;position:relative;top:13px;right:18px">
                            +242 06 9999666<br/>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="height:10px;width:93%" ></div>
                <div class="row">
                    <div  style="margin: 0 auto;width: 265px;height: 67px;">
                        <div class="col-xs-1"><img style="top:13px;position:relative" src="{{ url('libraries/imgs/email.png') }}" /></div>
                        <div class="col-xs-1"><img src="{{ url('libraries/imgs/line.jpg') }}"></div>
                        <div class="col-xs-9">
                            <span style="color:white;position:relative;top:13px;right:18px">metatoutprefabs@gmail.com</span>
                        </div>
                    </div>
                </div>
                <div class="row" style="height:10px;width:93%"></div>
                <div class="row" >
                    <div  style="margin: 0 auto;width: 265px;height: 67px;">
                        <div class="col-xs-1"><img style="top:13px;position:relative" src="{{ url('libraries/imgs/website.png') }}" /></div>
                        <div class="col-xs-1"><img src="{{ url('libraries/imgs/line.jpg') }}"></div>
                        <div class="col-xs-9">
                            <span style="color:white;position:relative;top:13px;right:18px">www.metatoutprefab.com</span><br/>
                        </div>
                    </div>
                </div>
                <div class="row" style="height:10px;width:93%"></div>
                <div class="row" >
                    <div  style="margin: 0 auto;width: 265px;height: 67px;">
                        <div class="col-xs-1"><img style="top:13px;position:relative" src="{{ url('libraries/imgs/address.png') }}" /></div>
                        <div class="col-xs-1"><img src="{{ url('libraries/imgs/line.jpg') }}"></div>
                        <div class="col-xs-9">
                            <span  style="color: white; position: relative;right: 18px;bottom:23px" >
                                Avenue Kimbambo<br/> Diagonale Servtec<br/>Centre Ville de Pointe Noire</br>
                                République du CONGO
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row" style="height:1px;background-color:white;margin-top:30px"></div>
            </div>
             <div class="SocialMediaButtons row" align="center">
                <div  style="width: 34px;margin: 0 auto;position: relative;left:-7%;">
                    <table cellspacing="0" cellpadding="0" style="width:100%;position:absolute;top:10px;">
                        <tr>
                            <td style="width:100%;text-align: center;">
                                 <div>
                                    <img style="cursor:pointer;margin-bottom:7px" class="LinkItemlist" id="FACEBOOK_BTN" src="{{ url('libraries/imgs/FACEBOOK-ICON.png') }}" />
                                    <div style="display: block;position: relative;height: 20px;width: 315px;text-align:center"><a style="color:white" href="https://www.facebook.com/Metatout-Prefabs-1032253740123112">https://www.facebook.com/Metatout-Prefabs-1032253740123112</a></div>
                                </div>
                            </td>
                        </tr>
                    </table> 
                     
                </div>
            </div>
     </div>
</div>

<script type="text/javascript">
// This example adds a marker to indicate the position of Bondi Beach in Sydney,
// Australia.
function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: -4.794462, lng: 11.8388308}
  });
   map.setOptions({ minZoom: 1, maxZoom: 20 , scrollwheel: false });
  var image = 'libraries/imgs/MAP-ICON.png';
  var beachMarker = new google.maps.Marker({
    position: {lat: -4.794462, lng: 11.8388308},
    map: map,
    icon: image
  });
}
</script>
 <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaw1j6s8Ham2o3imLnEXSMRTQa65AMIk0&signed_in=false&callback=initMap"></script>