<?php
/************************************************************
alayout.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/
{/** Initialisation block */
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $sys_company_details['attributes']['cd_company_name']; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1,user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/bootstrap/css/bootstrapmob.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('libraries/js/lightbox/css/lightbox.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('libraries/js/lightbox/css/screen.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('libraries/js/lightboxGallery/light-gallery/css/lightGallery.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('libraries/js/jgallery/dist/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('libraries/js/jgallery/dist/css/jgallery.min.css?v=1.5.0') }}"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ url('assets/admin/assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN THEME STYLES -->

    <link href="{{ url('assets/js/colorbox/example4/colorbox.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ url('assets/js/craftpip/css/jquery-confirm.css') }}">
    <link rel="stylesheet" href="{{ url('libraries/js/jcarousel/jcarousel.responsive.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ url('libraries/css/style.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('libraries/js/Smooth-Div-Scroll/css/smoothDivScroll.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('assets/js/jssor/css/style.css') }}" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
    @yield("content")
    <!-- Plugins -->
    @yield("plugins")

    <script src="{{ url('assets/admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/datetimepicker/jquery.datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/assets/global/plugins/bootstrap-select/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/unslider/unslider.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/Smooth-Div-Scroll/js/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/Smooth-Div-Scroll/js/jquery.kinetic.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/Smooth-Div-Scroll/js/jquery.mousewheel.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/Smooth-Div-Scroll/js/jquery.smoothdivscroll-1.3-min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/jcarousel/jquery.jcarousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/jgallery/dist/js/jgallery.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/jgallery/dist/js/touchswipe.min.js') }}"></script>

    <script type="text/javascript" src="{{ url('libraries/js/lightbox/js/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libraries/js/lightGallery/light-gallery/js/lightGallery.min.js') }}"></script>
    <script src="{{ url('libraries/js/lightbox/js/lightbox.min.map') }}"></script>

    <script type="text/javascript" src="{{ url('libraries/js/main.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/jquery.ba-hashchange.js') }}"></script>
    <script src="{{ url('assets/js/craftpip/js/jquery-confirm.js') }}"></script>
    <script src="{{ url('libraries/js/jquery_scrollto/jquery.scrollTo.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/js/jssor/js/jssor.slider.min.js') }}"></script>
    @yield('footer_js')
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>