<?php
/***********************************************************
scope.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 11, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
View for scope section
***********************************************************/

$url1 = url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $scope_data_array[0]->cm_media_base_dir . $scope_data_array[0]->cm_media_file_name . "." . $scope_data_array[0]->cm_media_file_extention;
$url2 = url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $scope_data_array[1]->cm_media_base_dir . $scope_data_array[1]->cm_media_file_name . "." . $scope_data_array[1]->cm_media_file_extention;
$url3 = url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $scope_data_array[2]->cm_media_base_dir . $scope_data_array[2]->cm_media_file_name . "." . $scope_data_array[2]->cm_media_file_extention;
$url4 = url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $scope_data_array[3]->cm_media_base_dir . $scope_data_array[3]->cm_media_file_name . "." . $scope_data_array[3]->cm_media_file_extention;
$url5 = url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $scope_data_array[4]->cm_media_base_dir . $scope_data_array[4]->cm_media_file_name . "." . $scope_data_array[4]->cm_media_file_extention;

?>

<div class="row scope">
    <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
        <div class="col-md-12" style="padding-right: 0px;padding-left: 0px;">
         <div class="ScopeSection hidden-xs">
            <div class="firstrow">
              <div class="col-md-12" style="">
                <table class="cont" cellspacing="0" cellpadding="0" style="width:824px;position:relative;z-index:10;">
                    <tr>
                        <td class="ScopeMainItem" style="">
                           <img class="ScopeItem img-responsive" src="{{ url('libraries/imgs/scope-block.png') }}" />
                        </td>
                        <td class="ScopeMainItem" style="padding-left:13px;">
                            <a href="{{ $url1 }}" data-lightbox="image-1" data-title="{{ $scope_data_array[0]->cm_media_title }}">
                            <div class="ScopeSectionTitle" style="margin-left:13px;"><?php echo $scope_data_array[0]->cm_media_title ?></div>
                            <img class="ScopeItem img-responsive" style="cursor:pointer" src="{{ $url1 }}" data-src="{{ $url1 }}" data-over_src="{{ $url1 }}" />
                            </a>
                        </td>
                        <td class="ScopeMainItem"  style="padding-left:13px;">
                            <a href="{{ $url2 }}" data-lightbox="image-1" data-title="{{ $scope_data_array[1]->cm_media_title }}">
                            <div class="ScopeSectionTitle" style="margin-left:13px;">{{ $scope_data_array[1]->cm_media_title }}</div>
                            <img class="ScopeItem img-responsive" style="cursor:pointer" src="{{ $url2 }}" data-src="{{ $url2 }}" data-over_src="{{ $url2 }}" />
                            </a>
                        </td>
                        <td></td>
                    </tr>
                </table>
              </div>
                <div class="WhiteRow"></div>
            </div>
            <div class="secondrow">
              <div class="col-md-12" style="">
                <table class="cont" cellspacing="0" cellpadding="0" style="width:824px;position:relative;z-index:10;">
                    <tr>
                        <td class="ScopeMainItem" style="">
                              <a href="{{ $url3 }}" data-title="{{ $scope_data_array[2]->cm_media_title }}" data-lightbox="image-1">
                              <div class="ScopeSectionTitle" style="">{{ $scope_data_array[2]->cm_media_title }}</div>
                              <img class="ScopeItem  img-responsive" style="cursor:pointer" src="{{ $url3 }}" data-src="{{ $url3 }}" data-over_src="{{ $url3 }}" />
                              </a>
                          </td>
                          <td class="ScopeMainItem" style="padding-left:13px;">
                              <a href="{{ $url4 }}" data-title="{{ $scope_data_array[3]->cm_media_title }}" data-lightbox="image-1">
                              <div class="ScopeSectionTitle" style="margin-left:13px;">{{ $scope_data_array[3]->cm_media_title }}</div>
                              <img class="ScopeItem img-responsive" style="cursor:pointer" src="{{ $url4 }}" data-src="{{ $url4 }}" data-over_src="{{ $url4 }}" />
                              </a>
                          </td>
                          <td class="ScopeMainItem" style="padding-left:13px;">
                              <a href="{{ $url5 }}" data-title="{{ $scope_data_array[4]->cm_media_title }}" data-lightbox="image-1">
                              <div class="ScopeSectionTitle" style="margin-left:13px;">{{ $scope_data_array[4]->cm_media_title }}</div>
                              <img class="ScopeItem  img-responsive" style="cursor:pointer;" src="{{ $url5 }}" data-src="{{ $url5 }}" data-over_src="{{ $url5 }}" />
                              </a>
                          </td>
                        <td></td>
                    </tr>
                </table>
              </div>
                <div class="WhiteRow"></div>
            </div>
         </div>
        </div>
        </div>
        <div class="visible-xs" style="background-color:white">
        <div class="row" style="padding-top: 100px;">
           <div class="WhiteRowMob"></div>
          <div class="ScopeMob">
          <div class="ScopeMobItem" style="padding-right: 36px;">
           <img width="130px" height="130px" src="{{ url('libraries/imgs/scope-block.jpg') }}" />
            </div>

        <div class="ScopeMobItem">
        <a href="{{ $url1 }}" data-lightbox="image-2" data-title="{{ $scope_data_array[0]->cm_media_title }}">
           <img width="130px" height="130px" style="cursor:pointer" src="{{ $url1 }}"/>
           <div class="ScopeSectionTitleMobile" style="<?php if(strlen($scope_data_array[0]->cm_media_title)>15){echo 'font-size:13px;padding-top:0px';}?>">{{ $scope_data_array[0]->cm_media_title }}</div>
           </a>

           </div>
        </div>

        </div>

        <div class="row" style="padding-top:30px">
           <div class="WhiteRowMob"></div>
          <div class="ScopeMob">
        <div class="ScopeMobItem" style="padding-right: 36px;">
        <a href="{{ $url2 }}" data-title="{{ $scope_data_array[1]->cm_media_title }}" data-lightbox="image-2">
           <img width="130px" height="130px" style="cursor:pointer" src="{{ $url2 }}" />
           <div class="ScopeSectionTitleMobile" style="<?php if(strlen($scope_data_array[1]->cm_media_title)>15){echo 'font-size:13px;padding-top:0px';}?>">{{ $scope_data_array[1]->cm_media_title }}</div>
           </a>

           </div>

        <div class="ScopeMobItem">
        <a href="{{ $url3 }}" data-lightbox="image-2" data-title="{{ $scope_data_array[2]->cm_media_title }}">
           <img width="130px" height="130px" style="cursor:pointer" src="{{ $url3 }}" />
           <div class="ScopeSectionTitleMobile" style="<?php if(strlen($scope_data_array[2]->cm_media_title)>15){echo 'font-size:13px;padding-top:0px';}?>">{{ $scope_data_array[2]->cm_media_title }}</div>
           </a>
           </div>
        </div>

        </div>

        <div class="row" style="padding-top:30px;padding-bottom:30px">
           <div class="WhiteRowMob"></div>
          <div class="ScopeMob">
        <div class="ScopeMobItem" style="padding-right: 36px;">
        <a href="{{ $url4 }}" data-title="{{ $scope_data_array[3]->cm_media_title }}" data-lightbox="image-2">
           <img width="130px" height="130px" style="cursor:pointer" src="{{ $url4 }}"/>
           <div class="ScopeSectionTitleMobile" style="<?php if(strlen($scope_data_array[3]->cm_media_title)>15){echo 'font-size:13px;padding-top:0px';}?>">{{ $scope_data_array[3]->cm_media_title }}</div>
           </a>
           </div>

        <div class="ScopeMobItem">
        <a href="{{ $url5 }}" data-lightbox="image-2" data-title="{{ $scope_data_array[4]->cm_media_title }}">
           <img width="130px" height="130px" style="cursor:pointer" src="{{ $url5 }}"/>
           <div class="ScopeSectionTitleMobile" style="<?php if(strlen($scope_data_array[4]->cm_media_title)>15){echo 'font-size:13px;padding-top:0px';}?>">{{ $scope_data_array[4]->cm_media_title }}</div>
           </a>
           </div>
        </div>

        </div>
        </div>
    </div>
</div>