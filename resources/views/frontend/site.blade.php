<?php
/***********************************************************
site.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 9, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>

@extends('frontend.layout.layout')

@section('content')
<div id="pcmenu"  class="navbar">
    <div class="navbar-fixed-top">
       <div class="container-narrow" style="margin: 0px;padding: 0px;">
           <div class="MainMenu">
               <div class="TopLineMenu"></div>
               <div class="SecondLineMenu"></div>
               <div class="Menu">
            
                   <ul>
                        <?php for ($i = 0; $i < count($lstPages) - 1; $i++) {
                                $tag = strtolower($lstPages[$i]->cp_page_title);
                                $tag = str_replace(" ", "", $tag);
                                ?>
                                    <li class="LiItem"><span>&nbsp;&nbsp;&nbsp;<a class="MenuItem" data-menu="<?php echo $tag;  ?>"><?php echo trans('translation.'. $tag); ?></a>&nbsp;&nbsp;</span></li>
                                    <li style="width:2px  !important;color: #8CC63E;"><span>|</span></li>
                                <?php
                            }
                            $tag = strtolower($lstPages[ count($lstPages) - 1 ]->cp_page_title);
                            $tag = str_replace(" ", "", $tag);
                        ?>
                       <li class="LiItem" style="padding-left:13px;"><span><a class="MenuItem" data-menu="<?php echo $tag; ?>"><?php echo trans('translation.'. $tag); ?></a></span></li>
                   </ul>
                    
               </div>
           </div>
           <div class="LogoHolder"><img src="{{ $company_logo }}" class="EagleLogo" width="262px" style="cursor: pointer" /></div>
       </div>
   </div>
</div>
 <div id="mobilemenu" class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner" style="background-color: white;color:#8CC63E">
        <div class="container">
            <div class="navbar-header">
                <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                  <span class="sr-only">Toggle navigation</span>
                  <span style="background-color:#8CC63E !important;" class="icon-bar"></span>
                  <span style="background-color:#8CC63E !important;" class="icon-bar"></span>
                  <span style="background-color:#8CC63E !important;" class="icon-bar"></span>
                </button>
                <img src="{{ $company_logo }}" height="49" class="MenuItem EagleLogo"  data-page="homepage" style="cursor:pointer" />
            </div>
            <nav role="navigation" class="navbar-collapse bs-navbar-collapse collapse" style="height: 0.8px;">
                <ul class="nav navbar-nav">
                   <?php for ($i = 0; $i < count($lstPages) - 1; $i++) {
                                $tag = strtolower($lstPages[$i]->cp_page_title);
                                $tag = str_replace(" ", "", $tag);
                                ?>
                                    <li><a  class="MenuItem mobnav"  data-menu="<?php echo $tag;  ?>" style="cursor:pointer"><?php echo trans('translation.'. $tag); ?></a></li>
                                <?php
                            }
                            $tag = strtolower($lstPages[ count($lstPages) - 1 ]->cp_page_title);
                            $tag = str_replace(" ", "", $tag);
                        ?>
                       <li><a class="MenuItem mobnav"  data-menu="<?php echo $tag; ?>" style="cursor:pointer" ><?php echo trans('translation.'. $tag); ?></a></span></li>
                </ul>
            </nav>
        </div>
   </div>
   </div>
<div class="container-narrow" style="top:-57px;position:relative">
    @include('frontend.home', [ 'slideshow' => $slideshow ])
    @include('frontend.aboutus', [ 'aboutus_info' => $aboutus_info ])
    @include('frontend.ouroffice')
    @include('frontend.showroom')
    @include('frontend.scope', [ 'scope_data_array' => $scope_data_array ])
    @include('frontend.projects', [ 'projects_data_array' => $projects_data_array ])
    @include('frontend.clients', [ 'clients_data_array' => $clients_data_array ])
    @include('frontend.contactus')
    <div id="footer" style="color:#004A65">
        <div class="col-md-6 copyright-container"><a class="copyright">Copyright &copy; 2015 - METATOUT - All Rights Reserved</a></div>
        <div class="col-md-6 designed-container"><a class="designed">Designed by <b><span class="chameleon" style="cursor:pointer">CHAMELEON s.a.r.l </span>· <span class="softweb" style="cursor:pointer">SoftWeb</b> Co.</span></a></div>
    </div>
</div>
@endsection