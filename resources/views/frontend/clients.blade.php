<?php
/***********************************************************
clients.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 15, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
List of clients
***********************************************************/
  
?>

                      <div class="row">

                        <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">

                            <div class="ClientsSection">



                                <div id="makeMeScrollable">

                                    <div class="scrollingHotSpotLeft" style="display: block; opacity: 0;"></div>

                                    <div class="scrollingHotSpotRight" style="opacity: 0;"></div>

                                    <div class="scrollWrapper">

                                        <div class="scrollableArea" style="width: 3784px;">

                   <?php for ($i = 0; $i < count($clients_data_array); $i++) { ?>
                                   <img width="300px" height="180px" src="<?php echo url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $clients_data_array[$i]->cm_media_base_dir . $clients_data_array[$i]->cm_media_file_name . "." . $clients_data_array[$i]->cm_media_file_extention ?>" />
                   <?php } ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                      </div>