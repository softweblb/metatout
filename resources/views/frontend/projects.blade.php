<?php
/***********************************************************
projects.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 16, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/
{/** initialisation block */
    $nb_of_rows = 0;
}


{/** data retrivel block */
    $count = count($projects_data_array);
    if($count <= 2)
    {
        $nb_of_rows = 1;
    }
    else
    {
        $nb_of_rows =  ceil(count($projects_data_array) / 3);
    }
}

?>
<span id="hidden_fields">
    <input type="hidden" name="number_of_rows" value="<?php echo $nb_of_rows; ?>" />
    <input type="hidden" name="current_row" value="2" />
</span>
<div id="overlay"></div>

<div id="loading">
<div id="gallery">
    <?php
        for ($k = 0; $k < count($projects_data_array); $k = $k+1)
        {
            ?>
                <div class="album" data-jgallery-album-title="<?php echo $projects_data_array[$k]['title']; ?>">
                    <?php
                    if(isset($projects_data_array[$k]['images']))
                    {
                        for ($i = 0; $i < count($projects_data_array[$k]['images']); $i++)
                        {
                            ?>
                                <a href="<?php echo $projects_data_array[$k]['images'][$i]['image']; ?>"><img style="display:none;height:266px;width:266px;" src="<?php echo $projects_data_array[$k]['images'][$i]['image']; ?>" /></a>
                            <?php
                        }
                    }
                   
                    ?>
                </div>
            <?php
        }

    ?>
</div>
</div>
<div id="AllProjects" class="col-md-12" style="padding-right: 0;padding-left: 0px;" >
    <div id="ProjectAlbums" class="ProjectAlbums hidden-xs" style="height:590px">
        <div class="row1">
         <div class="col-md-12">
             <table class="cont" cellspacing="0" cellpadding="0" border="0" style="width:824px;position:relative;z-index:10;margin-bottom:8px;height:266px;">
                    <tr>
                        <td class="ProjectMainItem" >
                           <img width="266px" height="266px" class="ProjectItem img-responsive" src="{{ url('libraries/imgs/project-block.png') }}" />
                        </td>
                        <td class="ProjectMainItem" style="padding-left:13px;" >
                          @if(isset($projects_data_array[0]))
                           <div id="project1" title="{{ $projects_data_array[0]['title'] }}">
                                <div id="albumgal1" style="margin-left:13px;"  class="ProjectSectionTitle">
                                    <p style="font-size:16px;font-family:HelveticaNeue-Bold"><img style="margin-bottom:10px" src="{{ url('libraries/imgs/ZOOM.png') }}"><br>{{ $projects_data_array[0]['title'] }}</p>
                                </div>
                            	<img  class="ProjectItem img-responsive" width="266px"  style="height:266px;" src="{{ $projects_data_array[0]['images'][0]['image'] }}" />
                            </div>
                            @endif
                        </td>
                        <td class="ProjectMainItem" style="padding-left:13px;" >
                            @if(isset($projects_data_array[1]))
                            <div id="project2" title="{{ $projects_data_array[1]['title'] }}">
                                <div id="albumgal2" style="margin-left:13px;"  class="ProjectSectionTitle">
                                    <p style="font-size:16px;font-family:HelveticaNeue-Bold"><img style="margin-bottom:10px" src="{{ url('libraries/imgs/ZOOM.png') }}"><br>{{ $projects_data_array[1]['title'] }}</p>
                                </div>
                                <img  class="ProjectItem img-responsive" width="266px" style="height:266px;" src="{{ $projects_data_array[1]['images'][0]['image'] }}" />
                           </div>
                           @endif
                        </td>
                       
                    </tr>
                </table>
           
            </div>
             <div class="WhiteRow"></div>
        </div>

       <?php
       $top = 40;
       $p = 2;
            for ($k = 2; $k < count($projects_data_array); $k = $k + 3)
            {

                
                $top = $top + 275;
                ?>
                    <div class="row<?php echo $p?>" style="top:<?php echo $top; ?>px; <?php if($k!=2){echo 'visibility:hidden';}?>">
                      <div class="col-md-12">
                        <table class="cont" cellspacing="0" cellpadding="0" style="width:824px;position:relative;z-index:10;margin-bottom:8px;height: 266px">
                                <tr>
                                    <td class="ProjectMainItem"  >
                                      <?php if(isset($projects_data_array[$k]['images'])){ ?>
                                       <div id="project<?php echo $k+1; ?>" title="<?php echo $projects_data_array[$k+1]['title']; ?>">
                                            <div id="albumgal<?php echo $k+1; ?>" class="ProjectSectionTitle">
                                                <p style="font-size:16px;font-family:HelveticaNeue-Bold"><img style="margin-bottom:10px" src="{{ url('libraries/imgs/ZOOM.png') }}"><br><?php echo $projects_data_array[$k]['title']; ?></p>
                                            </div>
                                        	<img style="position: relative;height:266px;"  class="ProjectItem img-responsive" width="266px"   src="<?php echo $projects_data_array[$k]['images'][0]['image']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </td>
                                    <td class="ProjectMainItem"  style="padding-left:13px;" >
                                     <?php if(isset($projects_data_array[$k + 1]['images'])){ ?>
                                       <div id="project<?php echo ($k + 2); ?>" title="<?php echo isset( $projects_data_array[$k + 2]['title'] ) ? $projects_data_array[$k + 2]['title'] : ""; ?>">
                                            <div id="albumgal<?php echo ($k + 2); ?>" style="margin-left:13px;"  class="ProjectSectionTitle">
                                                <p style="font-size:16px;font-family:HelveticaNeue-Bold"><img style="margin-bottom:10px" src="{{ url('libraries/imgs/ZOOM.png') }}"><br><?php echo $projects_data_array[$k + 1]['title']; ?></p>
                                            </div>
                                        	<img style="position: relative;height:266px;"   class="ProjectItem img-responsive" width="266px"  style="" src="<?php echo $projects_data_array[$k  + 1]['images'][0]['image']; ?>" />
                                        </div>
                                       <?php } ?>
                                    </td>
                                    <td class="ProjectMainItem"  style="padding-left:13px;" >
                                        
                                        <?php if(isset($projects_data_array[$k + 2]['images'])){ ?>
                                        <div id="project<?php echo ($k + 3); ?>" title="<?php echo $projects_data_array[$k + 2]['title']; ?>">
                                            <div id="albumgal<?php echo ($k + 3); ?>" style="margin-left:13px;" class="ProjectSectionTitle">
                                                <p style="font-size:16px;font-family:HelveticaNeue-Bold"><img style="margin-bottom:10px" src="{{ url('libraries/imgs/ZOOM.png') }}"><br>{{ $projects_data_array[$k + 2]['title'] }}</p>
                                            </div>
                                            <img style="position: relative;height:266px;"  class="ProjectItem img-responsive" width="266px" style="" src="<?php echo $projects_data_array[$k + 2]['images'][0]['image']; ?>" />
                                       </div>
                                       <?php } ?>
                                    </td>
                                   
                                </tr>
                            </table>
                        
                        </div>
                        <div class="WhiteRow"></div>
                    </div>
                <?php
       $p=$p+1;    }
       ?>
    </div>

</div>

<div id="MobileAlbums" class="visible-xs" style="background-color:#CCDDE7;padding-bottom: 100px;">
<div class="row" style="padding-top:30px">
   <div class="GrayRowMob"></div>
  <div class="ProjMob">
  <div class="ProjMobItem" style="padding-right: 40px;">
   <img width="130px" height="130px" src="{{ url('libraries/imgs/project-block.jpg') }}" />
    </div>


     @if(isset($projects_data_array[0]))
<div class="ProjMobItem" id="albumgal1">
   <img width="130px"  height="130px" style="cursor:pointer" src="{{ $projects_data_array[0]['images'][0]['image'] }}"/>   
<h4 class="ProjMobTitle">{{ $projects_data_array[0]['title'] }}</h4>

   </div>
   @endif
</div>

</div>

<?php for ($k = 1; $k < count($projects_data_array); $k = $k + 2)
            { ?>

<div class="row <?php if($k>4){echo 'hiddenp';}?>" style="padding-top:30px;<?php if($k>4){echo 'display:none';}?>">
   <div class="GrayRowMob"></div>
  <div class="ProjMob">
  <?php if(isset($projects_data_array[$k]['images'])){ ?>
<div class="ProjMobItem" id="albumgal<?php echo $k+1?>" style="padding-right: 40px;">
   <img width="130px" height="130px" style="cursor:pointer" src="<?php echo $projects_data_array[$k]['images'][0]['image'] ?>" />   
   
    <h4 class="ProjMobTitle">{{ $projects_data_array[$k]['title'] }}</h4>
   </div>
   <?php } ?>
    
    <?php if(isset($projects_data_array[$k+1]['images'])){ ?>
<div class="ProjMobItem" id="albumgal<?php echo $k+2?>">
   <img width="130px" height="130px" style="cursor:pointer" src="<?php echo $projects_data_array[$k+1]['images'][0]['image'] ?>" />   
       <h4 class="ProjMobTitle">{{ $projects_data_array[$k+1]['title'] }}</h4>  

   </div>
      <?php } ?>
</div>

</div>
<?php } ?>

</div>
    <?php if($nb_of_rows > 2 ){ ?>
    <div class="hidden-xs" style="width:100%;text-align: center;position:relative;background-color:#CCDDE7;padding-bottom:30px">
    <img style="position: relative; right: 4px;" class="ProjectsBtnReadMore" src="{{ url('libraries/imgs/btnshowmore.png') }}" />
    <?php } ?>
</div>
    <?php if($nb_of_rows > 2 ){ ?>
    <div class="visible-xs" style="width:100%;text-align: center;position:relative;background-color:#CCDDE7;padding-bottom:30px">
    <img style="position: relative;" class="ProjectsBtnReadMoreMob" src="{{ url('libraries/imgs/btnshowmore.png') }}" />
    <?php } ?>
</div>

