<?php
/***********************************************************
home.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 11, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/


?>

 <div class="row">
    <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
        <div class="HomePageSection">
           <div class="HomePagebanner">
                <ul>
                    <?php
                        for ($i = 0; $i < count($slideshow); $i++)
                        {
                            ?>
                                <li class="mobresize" style="background-image: url('<?php echo url() . "/" . Config::get('constants.SINGLE_IMAGE_PATH') . $slideshow[$i]->cm_media_base_dir . $slideshow[$i]->cm_media_file_name . "." .  $slideshow[$i]->cm_media_file_extention; ?>');height:740px;background-position: center center;background-repeat: no-repeat;background-size: cover;">&nbsp;</li>
                            <?php
                        }
                    ?>
                </ul>

            </div>
         </div>
     </div>
</div>