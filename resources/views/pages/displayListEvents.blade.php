<?php
/************************************************************
displayListEvents.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 27, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display list of all events
************************************************************/


?>
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Event Name</th>
            <th>Dates (From / To)</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cms_events as $index => $ce_info)
               <tr data-ce_row_id="{{ $ce_info->ce_row_id }}">
                   <td>{{ $ce_info->ce_row_id }}</td>
                   <td>{{ $ce_info->ce_event_title }}</td>
                   <td>{{ $ce_info->ce_date_from . " / " . $ce_info->ce_date_to }}</td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_EVENT_{{ $ce_info->ce_id }}" src="{{ url('assets/imgs/edit.png') }}" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_EVENT_{{ $ce_info->ce_id }}" src="{{ url('assets/imgs/delete.png') }}" /></td>
               </tr>
        @endforeach
    </tbody>
</table>