<?php
/************************************************************
EditImageAlbum.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
display Edit Image ALbu form to save in the database
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editImageAlbum.js"></script>
@endsection
@section('content')

<form name="form_edit_ia_info" id="FORM_EDIT_IA_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="cm_id" id="CM_ID" value="<?php echo $cms_media_display[0]->cm_id; ?>" />
     <input type="hidden" name="album_id" id="album_id" value="<?php echo $cms_media_display[0]->fk_album_id; ?>" />
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $cms_media_display[0]->fk_page_id; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Album Image Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
         <div class="row">
            <div class="col-md-12">
                <table cellspacing="0" cellpadding="0" style="width:100%">
                    <tr>
                        <td align="left"><h4>Image Title</h4></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <input type="text" value="<?php echo $cms_media_display[0]->cm_media_title; ?>" name="cm_media_title" id="CM_IAMGE_TITLE" style="width:100%;" class="form-control" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12">
                <table cellspacing="0" cellpadding="0" style="width:100%">
                    <tr>
                        <td align="left"><h4>Image Caption </h4></td>
                    </tr>
                    <tr>
                        <td align="left">
                          <textarea style="width:100%;height:250px;" name="cm_media_caption" id="CM_MEDIA_CAPTION" class="form-control" ><?php echo $cms_media_display[0]->cm_media_caption; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
         <div class='row'>
        <div class="col-md-12">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" data-url="<?php echo url(); ?>/upload/AImages" type="file" name="files" />
            </span>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
            </div>
            <img src="<?php echo $link; ?>" width="100" ID="MEDIA_PICTURE" />
        </div>
    </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_image_album" id="BTN_EDIT_IMAGE_ALBUM" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection
