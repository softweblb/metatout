<?php
/************************************************************
EditAlbumForm.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editAlbums.js"></script>
@endsection
@section('content')

<form name="form_edit_album_info" id="FORM_EDIT_ALBUM_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="ca_id" id="CA_ID" value="<?php echo $ca_id; ?>" />
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $page_id; ?>" />
     <input type="hidden" name="page_name" id="PAGE_NAME" value="<?php echo $page_name; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit <?php echo $page_name; ?> Page </h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
        <div class="row">
        <div class="col-md-12">
            <table cellspacing="0" cellpadding="0" style="width:100%">
                <tr>
                    <td align="left"><h4>Album Title</h4></td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" name="ca_album_title" id="CA_ALBUM_TITLE" style="width:100%;" value="<?php echo $lst_cms_albums[0]->ca_album_title; ?>" class="form-control" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table cellspacing="0" cellpadding="0" style="width:100%">
                <tr>
                    <td align="left"><h4>Album Description</h4></td>
                </tr>
                <tr>
                    <td align="left">
                      <textarea style="width:100%;height:250px;" name="ca_album_caption" id="CA_ALBUM_CAPTION" class="form-control" ><?php echo $lst_cms_albums[0]->ca_album_caption; ?></textarea>
                    </td>
                </tr>
            </table>
        </div>
    </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_album" id="BTN_EDIT_ALBUM" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection