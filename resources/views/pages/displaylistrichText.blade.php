<?php
/************************************************************
displaylistrichText.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display List of rich Text Content
************************************************************/


?>
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Ritch Text Title</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($cms_rich_text as $index => $rt_info)
        {
            ?>
               <tr data-rt_id="<?php echo $rt_info->rt_id; ?>">
                   <td><?php echo $rt_info->rt_id; ?></td>
                   <td><?php echo $rt_info->rt_title_page; ?></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_RT_<?php echo $rt_info->rt_id; ?>" src="{{ url('assets/imgs/edit.png') }}" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_RT_<?php echo $rt_info->rt_id; ?>" src="{{ url('assets/imgs/delete.png') }}" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>