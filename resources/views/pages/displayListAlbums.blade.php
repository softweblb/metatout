<?php
/************************************************************
displayListAlbums.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display List of albums saved in the database
************************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Album Name</th>
            <th>Date Creation</th>
            <th>Album Images</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($cms_albums as $index => $ca_info)
        {
            ?>
               <tr data-ca_id="<?php echo $ca_info->ca_id; ?>">
                   <td><?php echo $ca_info->ca_id; ?></td>
                   <td><?php echo $ca_info->ca_album_title; ?></td>
                   <td><?php echo $ca_info->ca_date_creation ; ?></td>
                   <td><a target="_blank" href="<?php echo url(); ?>/administrator/ViewAlbum/<?php echo $cms_page; ?>/<?php echo $ca_info->ca_id; ?>">View Album</a></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_ALBUM_<?php echo $ca_info->ca_id; ?>" src="<?php echo url(); ?>/assets/imgs/edit.png" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_ALBUM_<?php echo $ca_info->ca_id; ?>" src="<?php echo url(); ?>/assets/imgs/delete.png" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>