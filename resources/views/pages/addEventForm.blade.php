<?php
/************************************************************
addEventForm.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 27, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>


@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/addEvents.js"></script>
<form name="form_add_event_info" id="FORM_ADD_EVENT_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $PageId; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Add New Event</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Event Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" name="ce_event_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_EVENT_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <?php
                    if($i == 0)
                    {
                        ?>
                             <div class="row">
                                <div class="col-md-2" align="left">From Date</div>
                                <div class="col-md-4" align="left">
                                    <input type="text" name="ce_from_date"  autocomplete="off" id="CE_FROM_DATE" class="form-control" value="" />
                                </div>
                                <div class="col-md-6" align="right"></div>
                            </div>
                            <div class="row" style="height:14px;"></div>
                            <div class="row">
                                <div class="col-md-2" align="left">To Date</div>
                                <div class="col-md-4" align="left">
                                    <input type="text" name="ce_to_date"  autocomplete="off" id="CE_TO_DATE" class="form-control" value="" />
                                </div>
                                <div class="col-md-6" align="right"></div>
                            </div>
                        <?php
                    }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Event Description (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="ce_event_description_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CE_EVENT_DESCRIPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_add_event" id="BTN_ADD_EVENT" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection