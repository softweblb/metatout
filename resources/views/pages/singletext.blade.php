<?php
/***********************************************************
singletext.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 30, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/singletext.js"></script>
@endsection

@section('content')

<form name="form_st_info" id="FORM_ST_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="page_id" id="PAGE_ID" value="{{ $page_id }}" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>{{ $page_name }} Page</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="hidden" value="<?php echo $getRichText[$i]->rt_id; ?>" name="rt_id_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_ID_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                    <input type="text" value="<?php echo  $getRichText[$i]->rt_title_page; ?>" name="rt_title_page_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="RT_TITLE_PAGE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Content (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="rt_page_content_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="RT_PAGE_CONTENT_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ><?php echo  $getRichText[$i]->rt_page_content; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_st" id="BTN_ST" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection