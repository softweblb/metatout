<?php
/************************************************************
DisplayListAlbumImages.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 5, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>
 <input type="hidden" name="number_pages" id="NUMBER_PAGES" value="{{ $number_pages }}" />
 <input type="hidden" name="page_number" id="PAGE_NUMBER" value="{{ $page_number }}" />
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th><input type="checkbox" name="ck_all_media" id="CK_ALL_MEDIA" value="1" /></th>
            <th>Image</th>
            <th>Image Title</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        for ($i = 0; $i < count($lstCmsMedia); $i++)
        {
            ?>
               <tr data-cm_id="<?php echo $lstCmsMedia[$i]->cm_id; ?>">
                   <td><input type="checkbox" name="a_media_<?php echo $lstCmsMedia[$i]->cm_id; ?>" id="A_MEDIA_<?php echo $lstCmsMedia[$i]->cm_id; ?>" value="1" /></td>
                   <td><img  src="<?php echo url() . "/" . Config::get( 'constants.ALBUMS_PATH') . $lstCmsMedia[$i]->cm_media_base_dir . $lstCmsMedia[$i]->cm_media_file_name  . "." . $lstCmsMedia[$i]->cm_media_file_extention ; ?>" width="72" /></td>
                   <td><?php echo $lstCmsMedia[$i]->cm_media_title; ?></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_IMAGE_<?php echo $lstCmsMedia[$i]->cm_id; ?>" src="<?php echo url(); ?>/assets/imgs/edit.png" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_IMAGE_<?php echo $lstCmsMedia[$i]->cm_id; ?>" src="<?php echo url(); ?>/assets/imgs/delete.png" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>