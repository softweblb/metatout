<?php
/************************************************************
displaylistrichText.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display List of rich Text Content
************************************************************/


?>
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Simple Text Title</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($cms_simple_text as $index => $st_info)
        {
            ?>
               <tr data-cc_row_id="<?php echo $st_info->cc_row_id; ?>">
                   <td><?php echo $st_info->cc_row_id; ?></td>
                   <td><?php echo $st_info->cc_text_title; ?></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_ST_<?php echo $st_info->cc_row_id; ?>" src="assets/imgs/edit.png" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_ST_<?php echo $st_info->cc_row_id; ?>" src="assets/imgs/delete.png" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>