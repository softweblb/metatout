<?php
/************************************************************
MediaAlbums.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display list of media in selected album
************************************************************/

?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/mediaAlbumsManagement.js"></script>
@endsection
@section('content')

<div class="PageHeader">
 List Images
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-2">
             <input type="hidden" name="cms_page" id="CMS_PAGE" value="<?php echo $page_id; ?>" />
        </div>
        <div class="col-md-8"></div>
        <div class="col-md-2">
             <input type="hidden" name="cms_albums" id="CMS_ALBUMS" value="<?php echo $ca_id; ?>" />
        </div>
     </div>
     <div class="row"><div class="col-md-12">&nbsp;</div></div>
     <div class="row">
        <div class="col-md-2">
            <select name="slc_action" id="SLC_ACTION" class="form-control" style="width:200px;">
                <option value="0">--Select One--</option>
                <option value="delete_image">Delete Items</option>
               <!--  <option value="edit_image">Edit Items</option> -->
            </select>
        </div>
        <div class="col-md-10"></div>
     </div>
     <div class="row"><div class="col-md-12">&nbsp;</div></div>
     <div class="row"><div class="col-md-12" align="left"><ul id="lst-pagination" class="pagination-sm"></ul></div></div>
     <div class="row">
         <div class="col-md-12">
             <div style="left:20%" class="ListAlbumsGrid"></div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_images" id="BTN_ADD_IMAGES" value="ADD IMAGES" />
         </div>
     </div>
</div>
@endsection