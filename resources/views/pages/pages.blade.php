<?php
/************************************************************
pages.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 21, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

@extends('layouts.alayout');

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/pagesManagement.js"></script>
<div class="PageHeader">
  List Pages
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-2">
             <select name="cms_menu" id="CMS_MENU" class="form-control" style="width:100%">
                    <option value="0">--Select One--</option>
                    <?php
                        for ($i=0;$i<count($lstCmsMenus);$i++)
                        {
                            ?>
                                <option value="<?php echo $lstCmsMenus[$i]->cm_id; ?>"><?php echo $lstCmsMenus[$i]->cm_menu_id; ?></option>
                            <?php
                        }
                    ?>
                </select>
        </div>
        <div class="col-md-10"></div>
     </div>
     <div class="row">
         <div class="col-md-12" style="height:10px;">
         </div>
     </div>
     <div class="row">
         <div class="col-md-12">
             <div style="left:20%" class="ListPagesGrid">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" style="display:none;" class="btn btn-primary" name="btn_add_page" id="BTN_ADD_PAGE" value="ADD Page" />
         </div>
     </div>
</div>
@endsection