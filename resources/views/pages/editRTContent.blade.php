<?php
/************************************************************
editRTContent.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Edit RT Content
************************************************************/


?>


@extends('layouts.alayout')
@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editRtContent.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
@endsection


@section('content')
<form name="form_edit_rt_info" id="FORM_EDIT_RT_INFO">
    <span id="hidden_fields">
         {!! csrf_field() !!}
         <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $PageId; ?>" />
    </span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit RichText Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="rt_id_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="RT_ID_<?php echo $listLanguages[$i]->sl_language_code; ?>" value="<?php echo $cms_rich_text[ $listLanguages[$i]->sl_language_code ]['rt_id']; ?>" />
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Page Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" name="rt_title_page_<?php echo $listLanguages[$i]->sl_language_code; ?>" value="<?php echo $cms_rich_text[ $listLanguages[$i]->sl_language_code ]['rt_title_page']; ?>" id="RT_TITLE_PAGE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Page Content (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="rt_page_content_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="RT_PAGE_CONTENT_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ><?php echo $cms_rich_text[ $listLanguages[$i]->sl_language_code ]['rt_page_content']; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_rt" id="BTN_EDIT_RT" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection