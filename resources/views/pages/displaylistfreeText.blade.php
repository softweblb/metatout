<?php
/***********************************************************
displaylistfreeText.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 3, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display list of FreeText
***********************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Text Title</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        @for($i = 0; $i < count($cms_free_text); $i++)
            <tr data-ft_row_id="{{ $cms_free_text[$i]->ft_row_id }}">
                   <td>{{ $cms_free_text[$i]->ft_row_id }}</td>
                   <td>{{ $cms_free_text[$i]->ft_title_text }}</td>
                   <td><img style="cursor: pointer;" height="16" id="EDIT_FT_{{ $cms_free_text[$i]->ft_row_id }}" src="{{ url('/assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer;" height="16" id="DELETE_FT_{{ $cms_free_text[$i]->ft_row_id }}" src="{{ url('/assets/imgs/delete.png') }}" /></td>
               </tr>
        @endfor
    </tbody>
</table>