<?php
/***********************************************************
addFreeTextForm.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Nov 3, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('/assets/js/pages/addFreeText.js') }}"></script>
@endsection

@section('content')

<form name="form_add_ft_info" id="FORM_ADD_FT_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Add FreeText Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       @for($i = 0; $i < count($listLanguages); $i++)
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Text Title ({{ $listLanguages[$i]->sl_language_code }})</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" name="ft_title_text_{{ $listLanguages[$i]->sl_language_code }}" id="FT_TITLE_TEXT_{{ $listLanguages[$i]->sl_language_code }}" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Text Content ({{ $listLanguages[$i]->sl_language_code }})</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="ft_text_content_{{ $listLanguages[$i]->sl_language_code }}" id="FT_TEXT_CONTENT_{{ $listLanguages[$i]->sl_language_code }}" class="form-control" ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
       @endfor
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_add_ft" id="BTN_ADD_FT" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection