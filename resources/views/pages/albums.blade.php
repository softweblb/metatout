<?php
/************************************************************
albums.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 4, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/albumsManagement.js"></script>
@endsection

@section('content')

<div class="PageHeader">
  <?php echo $page_name; ?> Page Management
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-2">
            <input type="hidden" name="cms_page" id="CMS_PAGE" value="<?php echo $page_id ?>" />
            <input type="hidden" name="page_name" id="PAGE_NAME" value="<?php echo $page_name; ?>" />
        </div>
        <div class="col-md-8"></div>
        <div class="col-md-2">
        </div>
     </div>
     <div class="row"><div class="col-md-12">&nbsp;</div></div>
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListAlbumsGrid"></div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_albums" id="BTN_ADD_ALBUMS" value="ADD ALBUMS" />
         </div>
     </div>
</div>
@endsection