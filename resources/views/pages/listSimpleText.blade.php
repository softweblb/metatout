<?php
/************************************************************
richText.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Rich Text management to add/edit and delete rich text content
************************************************************/


?>


@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/simpleTextManagement.js"></script>
<div class="PageHeader">
  List Simple Text Content Page
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8"></div>
        <div class="col-md-2">
            <select name="cms_lang" id="CMS_LANG" class="form-control" style="width:100%">
                <?php
                    for ($i=0;$i<count($listLanguages);$i++)
                    {
                        ?>
                            <option value="<?php echo $listLanguages[$i]->sl_id; ?>"><?php echo $listLanguages[$i]->sl_language_title; ?></option>
                        <?php
                    }
                ?>
            </select>
        </div>
     </div>
     <div class="row"><div class="col-md-12">&nbsp;</div></div>
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListSimpleText">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" style="display:none;" class="btn btn-primary" name="btn_add_st" id="BTN_ADD_ST" value="ADD Simple Text" />
         </div>
     </div>
</div>
@endsection