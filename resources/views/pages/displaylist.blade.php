<?php
/************************************************************
displaylist.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display list of all pages in this menu with ability to add/edit and delete a page
************************************************************/

?>
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Page Title</th>
            <th>Page Type</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($pages as $index => $page_info)
        {
            ?>
               <tr data-cp_id="<?php echo $page_info->cp_id; ?>">
                   <td><?php echo $page_info->cp_id; ?></td>
                   <td><?php echo $page_info->cp_page_title; ?></td>
                   <td><?php echo $types_array[ $page_info->cp_page_type ]; ?></td>
                   <td><img style="cursor: pointer;" height="16" id="EDIT_PAGE_<?php echo $page_info->cp_id; ?>" src="{{ url('/assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer;" height="16" id="DELETE_PAGE_<?php echo $page_info->cp_id; ?>" src="{{ url('/assets/imgs/delete.png') }}" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>