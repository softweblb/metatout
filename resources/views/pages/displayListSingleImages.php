<?php
/************************************************************
displayListSingleImages.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 26, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
view to display all images saved in the current page
************************************************************/
?>
<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Image Title</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($cms_media_display as $index => $media_info)
        {
            ?>
               <tr data-cm_row_id="<?php echo $media_info->cm_row_id; ?>">
                   <td><?php echo $media_info->cm_id; ?></td>
                   <td><?php echo $media_info->cm_media_title; ?></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="EDIT_SI_<?php echo $media_info->cm_row_id; ?>" src="<?php echo url(); ?>/assets/imgs/edit.png" /></td>
                   <td style="width:2%"><img style="cursor: pointer;" height="16" id="DELETE_SI_<?php echo $media_info->cm_row_id; ?>" src="<?php echo url(); ?>/assets/imgs/delete.png" /></td>
               </tr>
            <?php
        }
        ?>
    </tbody>
</table>