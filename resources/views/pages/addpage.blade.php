<?php
/************************************************************
addpage.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/addPage.js"></script>
@endsection

@section('content')
<form name="form_add_page" id="FORM_ADD_PAGE">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="menu_id" id="MENU_ID" value="<?php echo $MenuId; ?>" />
</span>
    <div class="container-fluid" align="left">
         <div class="row">
            <div class="col-sm-12"><h3>Add Page</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Page Type</div>
            <div class="col-sm-10">
                <select name="page_type" id="PAGE_TYPE" class="form-control" style="width:80%">
                        <option value="0">--Select One--</option>
                        <?php
                            for ($i=0;$i<count($page_types);$i++)
                            {
                                ?>
                                    <option value="<?php echo $page_types[$i]->cs_id; ?>"><?php echo $page_types[$i]->cs_page_type; ?></option>
                                <?php
                            }
                        ?>
                </select>
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Page Title</div>
            <div class="col-sm-10">
                <input type="text" style="width:80%;" name="page_title" id="PAGE_TITLE" class="form-control" />
            </div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Page Link Title</div>
            <div class="col-sm-10">
                <input type="text" style="width:80%;" name="cp_page_menu_title" id="CP_PAGE_MENU_TITLE" value="" class="form-control" />
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_add_page" id="BTN_ADD_PAGE" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection