<?php
/************************************************************
addRTContent.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
form for add Rich tect editor
************************************************************/


?>

@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editStContent.js"></script>
<form name="form_edit_st_info" id="FORM_EDIT_ST_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Simple Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Section Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="hidden" value="<?php echo $lst_cms_st_info[$listLanguages[$i]->sl_id]['cc_id']; ?>" name="cc_id_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_ID_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                    <input type="text" value="<?php echo $lst_cms_st_info[$listLanguages[$i]->sl_id]['cc_text_title']; ?>" name="cc_text_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_TEXT_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Section Content (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="cc_text_description_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_TEXT_DESCRIPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ><?php echo $lst_cms_st_info[$listLanguages[$i]->sl_id]['cc_text_description']; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_st" id="BTN_EDIT_ST" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection