<?php
/************************************************************
addSImageContent.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 26, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/addSIContent.js"></script>
@endsection
@section('content')

<form name="form_add_si_info" id="FORM_ADD_SI_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $PageId; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Add Single Image Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Image Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" name="cm_media_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_IAMGE_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Image Caption (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="cm_media_caption_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_MEDIA_CAPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_add_SI" id="BTN_ADD_SI" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection