<?php
/************************************************************
AlbumsDropdown.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 5, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

<select name="cms_albums" id="CMS_ALBUMS" class="form-control" style="width:100%">
                <option value="0">--Select One--</option>
                <?php
                    for ($i=0;$i<count($lstCmsAlbums);$i++)
                    {
                        ?>
                            <option value="<?php echo $lstCmsAlbums[$i]->ca_id; ?>"><?php echo $lstCmsAlbums[$i]->ca_album_title; ?></option>
                        <?php
                    }
                ?>
            </select>