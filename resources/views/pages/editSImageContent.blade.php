<?php
/************************************************************
editSImageContent.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 26, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editSIContent.js"></script>
@endsection

@section('content')
<form name="form_edit_si_info" id="FORM_EDIT_SI_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $PageId; ?>" />

     <input type="hidden" name="row_id" id="row_id" value="<?php echo $ListSimpleMedia[ $listLanguages[0]->sl_id ]['cm_row_id']; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Single Image Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                    <input type="hidden" name="cm_id_<?php echo $listLanguages[$i]->sl_language_code; ?>" value="<?php echo $ListSimpleMedia[ $listLanguages[$i]->sl_id ]['cm_id']; ?>" />
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Image Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" value="<?php echo $ListSimpleMedia[ $listLanguages[$i]->sl_id ]['cm_media_title']; ?>" name="cm_media_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_IAMGE_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Image Caption (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="cm_media_caption_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_MEDIA_CAPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ><?php echo $ListSimpleMedia[ $listLanguages[$i]->sl_id ]['cm_media_caption']; ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
         <div class='row'>
        <div class="col-md-12">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" data-url="<?php echo url(); ?>/upload/SImage" type="file" name="files" />
            </span>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
            </div>
            <img src="<?php echo $link; ?>" width="100" ID="MEDIA_PICTURE" />
        </div>
    </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_SI" id="BTN_EDIT_SI" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection