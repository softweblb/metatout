<?php
/************************************************************
addRTContent.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 24, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
form for add Rich tect editor
************************************************************/


?>

@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/addStContent.js"></script>
<form name="form_add_st_info" id="FORM_ADD_ST_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Add Simple Content</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Section Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" name="cc_text_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_TEXT_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Section Content (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="cc_text_description_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CC_TEXT_DESCRIPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_add_st" id="BTN_ADD_ST" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection