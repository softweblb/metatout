<?php
/************************************************************
translations.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Sep 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
List of translation translation_array
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('assets/js/langs/savetranslation.js') }}"></script>
@endsection

@section('content')

<div class="PageHeader">
  Translation Page
</div>
<form name="form_translation" id="FORM_TRANSLATION">
    <div class="container-fluid" style="height: 100%;background-color: white;" align="center">
         <div class="row"><div class="col-md-12">&nbsp;</div></div>
         <div class="row">
             <div class="col-md-2"></div>
             <div class="col-md-10">
                 <div style="left:20%" class="ListTranslation">
                    <table cellspacing="0" cellpadding="0" style="width:100%" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr role="row">
                                <th> Index </th>
                                <?php
                                   foreach ($languages_array as $lang_id => $lang_title)
                                   {
                                      ?>
                                      <th><?php echo $lang_title; ?></th>
                                      <?php
                                   }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($translation_array as $index_translation => $translation_info_key)
                                {
                                    ?>
                                     <tr class="" role="row">
                                     <td><?php echo $index_translation;  ?></td>
                                    <?php
                                        foreach ($translation_info_key as $lang_id => $lang_info)
                                        {
                                            ?>
                                                <td>
                                                    <input type="hidden" name="al_id[]" value="<?php echo $lang_info['id']; ?>" />
                                                    <input type="text" class="form-control" name="translation_<?php echo $lang_info['id']; ?>" id="TRANSLATION_<?php echo $lang_info['id']; ?>" value="<?php echo $lang_info['text']; ?>" />
                                                </td>
                                            <?php
                                        }
                                    ?>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-10"></div>
             <div class="col-md-2">
                 <input type="button"  class="btn btn-primary" name="btn_save_translation" id="BTN_SAVE_TRANSLATION" value="SAVE TRANSLATION" />
             </div>
         </div>
    </div>
</form>

@endsection