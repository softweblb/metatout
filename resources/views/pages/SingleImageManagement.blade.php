<?php
/************************************************************
singleimagemanagement.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 26, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
page management for add/edit and delete Single Image management page
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/singleImageManagement.js"></script>
@endsection
@section('content')

<div class="PageHeader">
  <?php echo $page_name; ?> Page Management
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-2">
            <input type="hidden" name="cms_page" id="CMS_PAGE" value="<?php echo $page_id; ?>" />
        </div>
        <div class="col-md-8"></div>
        <div class="col-md-2">
            <select name="cms_lang" id="CMS_LANG" class="form-control" style="width:100%">
                <?php
                    for ($i=0;$i<count($listLanguages);$i++)
                    {
                        ?>
                            <option value="<?php echo $listLanguages[$i]->sl_id; ?>"><?php echo $listLanguages[$i]->sl_language_title; ?></option>
                        <?php
                    }
                ?>
            </select>
        </div>
     </div>
     <div class="row"><div class="col-md-12">&nbsp;</div></div>
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListSingleImageGrid">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_si" id="BTN_ADD_SI" value="ADD Image" />
         </div>
     </div>
</div>
@endsection