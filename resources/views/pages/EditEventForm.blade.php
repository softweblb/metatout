<?php
/************************************************************
EditEventForm.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 28, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Edit Event Form to save in the database
************************************************************/

?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/pages/editEvents.js"></script>
@endsection
@section('content')

<form name="form_edit_event_info" id="FORM_EDIT_EVENT_INFO">
<span id="hidden_fields">
     {!! csrf_field() !!}
     <input type="hidden" name="row_id" id="ROW_ID" value="<?php echo $rowId; ?>" />
     <input type="hidden" name="page_id" id="PAGE_ID" value="<?php echo $pageId; ?>" />
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Edit Event</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <?php
            for ($i = 0; $i < count($listLanguages); $i++)
            {
                ?>
                 <div class="row">
                     <input type="hidden" name="ce_id_<?php echo $listLanguages[$i]->sl_language_code; ?>" value="<?php echo $ListEventInfo[ $listLanguages[$i]->sl_id ]['ce_id']; ?>" />
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Event Title (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" value="<?php echo $ListEventInfo[ $listLanguages[$i]->sl_id ]['ce_event_title'];  ?>" name="ce_event_title_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CM_EVENT_TITLE_<?php echo $listLanguages[$i]->sl_language_code; ?>" style="width:100%;" class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row" style="height:14px;"></div>
                <?php
                    if($i == 0)
                    {
                        ?>
                             <div class="row">
                                <div class="col-md-2" align="left">From Date</div>
                                <div class="col-md-4" align="left">
                                    <input type="text" name="ce_from_date"  autocomplete="off" id="CE_FROM_DATE" class="form-control" value="<?php echo $ListEventInfo[ $listLanguages[$i]->sl_id ]['ce_date_from'];  ?>" />
                                </div>
                                <div class="col-md-6" align="right"></div>
                            </div>
                            <div class="row" style="height:14px;"></div>
                            <div class="row">
                                <div class="col-md-2" align="left">To Date</div>
                                <div class="col-md-4" align="left">
                                    <input type="text" name="ce_to_date"  autocomplete="off" id="CE_TO_DATE" class="form-control" value="<?php echo $ListEventInfo[ $listLanguages[$i]->sl_id ]['ce_date_to'];  ?>" />
                                </div>
                                <div class="col-md-6" align="right"></div>
                            </div>
                        <?php
                    }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <table cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                                <td align="left"><h4>Event Description (<?php echo $listLanguages[$i]->sl_language_code; ?>)</h4></td>
                            </tr>
                            <tr>
                                <td align="left">
                                  <textarea style="width:100%;height:250px;" name="ce_event_description_<?php echo $listLanguages[$i]->sl_language_code; ?>" id="CE_EVENT_DESCRIPTION_<?php echo $listLanguages[$i]->sl_language_code; ?>" class="form-control" ><?php echo $ListEventInfo[ $listLanguages[$i]->sl_id ]['ce_event_description'];  ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }
       ?>
        <div class="row" style="height:14px;"></div>
          <div class='row'>
        <div class="col-md-12">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" data-url="<?php echo url(); ?>/upload/Event" type="file" name="files" />
            </span>
            <br>
            <br>
            <!-- The global progress bar -->
            <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
            </div>
            <img src="<?php echo $link; ?>" width="100" ID="MEDIA_PICTURE" />
        </div>
    </div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_edit_event" id="BTN_EDIT_EVENT" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection