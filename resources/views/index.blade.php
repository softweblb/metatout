@extends('frontend.app')

@section('header')

@endsection
@section('content')
@endsection

@section('menu')
<nav role="navigation" class="navbar navbar-default main-menu white-bg">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">
            <img src='{{ asset("/assets/imgs/logo-small.png") }}' class='' />
        </a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            @foreach($cms_pages as $page)
                @if($page->cp_page_title != "home")
                <li class="menu-separator"><a href=""></a></li>
                @endif
                @if($page->cp_page_title == $page_name)
                <?php $class_name = "active"; ?>
                @else
                <?php $class_name = ""; ?>
                @endif
            <li class="menu-links center-text {{ $class_name }}"><a href="/{{ $page->cp_page_title }}" ><?php echo str_replace("-"," ", $page->cp_page_title); ?></a></li>
            @endforeach
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="search">
                <a href=""><img src='{{ asset("/assets/imgs/search.png") }}'/></a>
            </li>
            <li class="lang-select">
                <a href=""><img src='{{ asset("/assets/imgs/lang-fr.png") }}'/></a>
            </li>
            <li class="lang-select">
                <a href=""><img src='{{ asset("/assets/imgs/lang-en.png") }}'/></a>
            </li>
        </ul>
    </div>
</nav>
@endsection

@section('video')
@endsection

@section('text_scroller')

@endsection


@section('footer')
<footer class="footer-distributed">
    <div class="footer-left">
        <p class="footer-links">
            @foreach($cms_pages as $page)
            @if($page->cp_page_title != "home")
            •
            @endif
            @if($page->cp_page_title == $page_name)
            <?php $class_name = "bold-text"; ?>
            @else
            <?php $class_name = ""; ?>
            @endif
            <a class="{{ $class_name }}" href="/{{ $page->cp_page_title }}" class="">{{ $page->cp_page_title }}</a>
            @endforeach
        </p>
    </div>
    <div class="footer-right">
        <div class='footer-copy'>Copyright &COPY; 2015 | Hripsimiantz College | All Rights Reserved</div>
        <div class='footer-soc'>
            <a href="#"><img src='{{ asset("/assets/imgs/facebook-footer.png") }}'/></a>
            <a href="#"><img src='{{ asset("/assets/imgs/instagram-footer.png") }}'/></a>
            <a href="#"><img src='{{ asset("/assets/imgs/youtube-footer.png") }}'/></a>
            <a href="#"><img src='{{ asset("/assets/imgs/linkedin-footer.png") }}'/></a>
        </div>
        
    </div>
</footer>
@endsection

@section('activities')
@endsection


@section('sidebar')
<div class='sidebar'>
    <div><a href='#'><img src='{{ asset("/assets/imgs/top.png") }}' /></a></div>
    <div><img src='{{ asset("/assets/imgs/calendar.png") }}' /></div>
    <div><img src='{{ asset("/assets/imgs/announce.png") }}' /></div>
    <div><img src='{{ asset("/assets/imgs/admissions.png") }}' /></div>
</div>
@endsection