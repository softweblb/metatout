<?php
/************************************************************
adduser.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
View of add users form
************************************************************/


?>

@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('assets/js/users/adduser.js') }}"></script>
@endsection

@section('content')
<div class="PageHeader">
    Add User
</div>
<form name="form_add_user" id="FORM_ADD_USER">
    <span id="hidden_fields">
        {!! csrf_field() !!}
    </span>
    <div class="container-fluid">
          <div class="row">
            <div class="col-md-4">
               <label>Username</label><br/>
                <input type="text" name="username" id="USERNAME" class="form-control"  autocomplete="off" value="" />
             </div>
            <div class="col-md-4">
               <label>Password</label><br/>
                <input type="password" name="u_password" id="U_PASSWORD"  autocomplete="off" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label> Retype Password</label><br/>
                <input type="password" name="u_retype_password" id="U_RETYPE_PASSWORD"  autocomplete="off" class="form-control" value="" />
             </div>
        </div>

        <div class="row">
            <div class="col-md-4">
               <label>Date Of Birth</label><br/>
                <input type="text" name="u_date_birth"  autocomplete="off" id="U_DATE_BIRTH" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label>Role</label><br/>
                <select name="u_role" id="U_ROLE" class="form-control" style="width:100%">
                    <option value="0">--Select One--</option>
                    <?php
                        for ($i=0;$i<count($roles);$i++)
                        {
                            ?>
                                <option value="<?php echo $roles[$i]->role_id; ?>"><?php echo $roles[$i]->role_name; ?></option>
                            <?php
                        }
                    ?>
                </select>
             </div>
            <div class="col-md-4">
               <label> Country </label><br/>
                <select name="u_country" id="U_COUNTRY" class="form-control" style="width:100%">
                    <option value="0">--Select One--</option>
                    <?php
                        for ($i=0;$i<count($countries);$i++)
                        {
                            ?>
                                <option value="<?php echo $countries[$i]->id; ?>"><?php echo $countries[$i]->name . "-" .  $countries[$i]->code; ?></option>
                            <?php
                        }
                    ?>
                </select>
             </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               <label>FullName</label><br/>
                <input type="text" name="fullname" id="FULLNAME"  autocomplete="off" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label>Email</label><br/>
                <input type="email" name="u_email" id="U_EMAIL"  autocomplete="off" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label>Website</label><br/>
                <input type="url" name="u_website" id="U_WEBSITE"  autocomplete="off" class="form-control" value="" />
             </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               <label>mobile</label><br/>
                <input type="text" name="u_mobile" id="U_MOBILE"  autocomplete="off" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label>Phone</label><br/>
                <input type="text" name="u_phone" id="U_PHONE"  autocomplete="off" class="form-control" value="" />
             </div>
            <div class="col-md-4">
               <label>Fax</label><br/>
                <input type="text" name="u_fax" id="U_FAX"  autocomplete="off" class="form-control" value="" />
             </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><input type="button" name="btn_add_user" id="BTN_ADD_USER" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection