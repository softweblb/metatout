<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/
$session_user_id = session()->get('user_id');
?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Username</th>
            <th>Full Name</th>
            <th style="width:2px" nowrap>Edit</th>
            <th style="width:2px" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
       @foreach($users as $user_id => $user_info)
               <tr data-user_id="<?php echo $user_info->id; ?>">
                   <td><?php echo $user_info->id; ?></td>
                   <td><?php echo $user_info->u_username; ?></td>
                   <td><?php echo $user_info->u_fullname; ?></td>
                   <td><img style="cursor: pointer" height="16" id="EDIT_USER_<?php echo $user_info->id; ?>" src="{{ url('assets/imgs/edit.png') }}" /></td>
                   <?php
                    $display = ($session_user_id == $user_info->id) ? "display:none;" : "";
                   ?>
                   <td><img style="cursor: pointer;<?php echo $display; ?>" height="16" id="DELETE_USER_<?php echo $user_info->id; ?>" src="{{ url('assets/imgs/delete.png') }}"  /></td>
               </tr>
        @endforeach
    </tbody>
</table>