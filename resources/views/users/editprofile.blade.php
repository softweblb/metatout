<?php
/************************************************************
editprofile.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/users/editProfile.js"></script>
@endsection

@section('content')
<form name="form_edit_profile" id="FORM_EDIT_PROFILE">
    <span id="hidden_fields">
        <input type="hidden" name="user_id" id="USER_ID" value="{{ $userId }}" />
        {!! csrf_field() !!}
    </span>
    <div class="container-fluid" style="width:500px;">
         <div class="row">
            <div class="col-sm-12"><h3>Edit Profile</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Username</div>
            <div class="col-sm-10"><input type="text" name="username" id="USERNAME" class="form-control" value="<?php echo $users[0]->u_username; ?>" /></div>
        </div>

        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">FullName</div>
            <div class="col-sm-10"><input type="text" name="fullname" id="FULLNAME" class="form-control" value="<?php echo $users[0]->u_fullname; ?>" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">password</div>
            <div class="col-sm-10"><input type="password" name="u_password" id="U_PASSWORD" class="form-control" value="" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">retype - password</div>
            <div class="col-sm-10"><input type="password" name="u_retype_password" id="U_RETYPE_PASSWORD" class="form-control" value="" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Email</div>
            <div class="col-sm-10"><input type="email" name="u_email" id="U_EMAIL" class="form-control" value="<?php echo $users[0]->u_email; ?>" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Website</div>
            <div class="col-sm-10"><input type="url" name="u_website" id="U_WEBSITE" class="form-control" value="<?php echo $users[0]->u_website; ?>" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">mobile</div>
            <div class="col-sm-10"><input type="text" name="u_mobile" id="U_MOBILE" class="form-control" value="<?php echo $users[0]->u_mobile; ?>" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Phone</div>
            <div class="col-sm-10"><input type="text" name="u_phone" id="U_PHONE" class="form-control" value="<?php echo $users[0]->u_phone; ?>" /></div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_edit_user" id="BTN_EDIT_USER" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection