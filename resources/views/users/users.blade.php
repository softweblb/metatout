<?php
/************************************************************
users.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
view of display usermanagement section
************************************************************/

?>
@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/users/usersmanagement.js"></script>
<div class="PageHeader">
   List Users
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListUserGirds">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_user" id="BTN_ADD_USER" value="ADD" />
         </div>
     </div>
</div>
@endsection