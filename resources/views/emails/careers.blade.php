<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Contact Message</h2>

<div>
    <div>
        <label>First Name</label>
        {!! $first_name !!}
    </div>
    <div>
        <label>Last Name</label>
        {!! $last_name !!}
    </div>
    <div>
        <label>Email</label>
        {!! $email !!}
    </div>
    <div>
        <label>Phone Number</label>
        {!! $phone !!}
    </div>
    <div>
        <label>Country</label>
        {!! $country !!}
    </div>
    <div>
        <label>Employment Status</label>
        {!! $employment_status !!}
    </div>
    <div>
        <label>Position</label>
        {!! $position !!}
    </div>
</div>

</body>
</html>