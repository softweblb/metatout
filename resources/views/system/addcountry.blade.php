<?php
/***********************************************************
addcountry.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 26, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Add New Country to the database
***********************************************************/



?>


@extends('layouts.playout')

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/configuration/addCountry.js"></script>
@endsection
@section('content')
<form name="form_add_country" id="FORM_ADD_COUNTRY">
<span id="hidden_fields">
     {!! csrf_field() !!}
</span>
    <div class="container" style="height: 100%;width:100%;" align="center">
         <div class="row">
            <div class="col-md-12"><h3>Add Country</h3></div>
        </div>
       <div class="row" style="height:14px;"></div>
       <div class="row">
            <div class="col-md-12">
                <label><h4>Country Code</h4></label>
                <span>
                    <input type="text" name="code" id="CODE" value="" class="form-control" />
                </span>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12">
                <table cellspacing="0" cellpadding="0" style="width:100%">
                    <tr>
                        <td align="left"><h4>Country Name</h4></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <input type="text" name="name" id="NAME" value="" class="form-control" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="height:14px;"></div>
        <div class="row">
            <div class="col-md-12" align="right"><input type="button" name="btn_add_country" id="BTN_ADD_COUNTRY" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection