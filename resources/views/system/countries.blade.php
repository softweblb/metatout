<?php
/***********************************************************
countries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 26, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Page to Manage Countries
***********************************************************/



?>


@extends('layouts.alayout');

@section('plugins')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/configuration/countriesManagement.js"></script>
@endsection

@section('content')

<div class="PageHeader">
  List Countries
</div>
<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <div class="row">
        <div class="col-md-12"></div>
     </div>
     <div class="row">
         <div class="col-md-12" style="height:10px;">
         </div>
     </div>
     <div class="row">
         <div class="col-md-12">
             <div style="left:20%" class="ListCountriesGrid">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_country" id="BTN_ADD_COUNTRY" value="ADD COUNTRY" />
         </div>
     </div>
</div>
@endsection