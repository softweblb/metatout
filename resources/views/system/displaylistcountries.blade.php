<?php
/***********************************************************
displaylistcountries.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 26, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Display list of countries
***********************************************************/

?>


<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Code</th>
            <th>Country</th>
            <th style="width:2%" nowrap>Edit</th>
            <th style="width:2%" nowrap>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($countries as $index => $country_info)
               <tr data-c_id="<?php echo $country_info->id; ?>">
                   <td><?php echo $country_info->id; ?></td>
                   <td><?php echo $country_info->code; ?></td>
                   <td><?php echo $country_info->name; ?></td>
                   <td><img style="cursor: pointer;" height="16" id="EDIT_COUNTRY_<?php echo $country_info->id; ?>" src="{{ url('/assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer;" height="16" id="DELETE_COUNTRY_<?php echo $country_info->id; ?>" src="{{ url('/assets/imgs/delete.png') }}" /></td>
               </tr>
        @endforeach
    </tbody>
</table>