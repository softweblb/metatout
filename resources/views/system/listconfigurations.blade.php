<?php
/***********************************************************
listconfigurations.blade.php
Product :
Version : 1.0
Release : 2
Date Created : Oct 26, 2015
Developed By  : Mohamad Mantach   PHP Department Softweb S.A.R.L
All Rights Reserved ,    Softweb S.A.R.L COPYRIGHT 2015

Page Description :
{Enter page description Here}
***********************************************************/



?>


@extends('layouts.alayout')

@section('plugins')
<script type="text/javascript" src="{{ url('assets/js/configuration/configuration.js') }}"></script>
@endsection
@section('content')

<div id="MSG_BOX" style="display:none" class="alert alert-success">
</div>
<div class="portlet-body flip-scroll">
<form name="form_save_configurations" id="FORM_SAVE_CONFIGURATIONS">
     <table  class="table table-bordered table-striped table-condensed flip-content" style="width:100%">
        <thead class="flip-content">
            <tr>
                <th style="width:2%">#</th>
                <th style="width:30%">Index</th>
                <th style="width:20%">Value</th>
                <th style="width:48%">Description</th>
            </tr>
        </thead>
        <tbody>
            <?php

                for ($i = 0; $i < count($sys_configurations); $i++) {
                    ?>
                    <tr>
                       <td>
                       <input type="hidden" name="sa_id[]" value="<?php echo $sys_configurations[$i]->sa_id; ?>" />
                       <input type="hidden" name="sa_config_index[]" value="<?php echo $sys_configurations[$i]->sa_config_index; ?>" />
                        <?php echo $sys_configurations[$i]->sa_id ?>
                       </td>
                       <td>
                            <?php echo $sys_configurations[$i]->sa_config_index ?>
                       </td>
                       <td>
                            <input type="text" name="sa_config_value[]" class="form-control" value="<?php echo $sys_configurations[$i]->sa_config_value; ?>" />
                       </td>
                       <td>
                            <input type="text" name="sa_config_description[]" class="form-control" value="<?php echo $sys_configurations[$i]->sa_config_description ?>" />
                       </td>
                   </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
</form>
</div>
<div class="row">
    <div class="col-md-12" align="right">
        <button id="BTN_SAVE_CONFIGURATION" name="btn_save_configuration" type="button" class="btn btn-info"  >Save</button>
    </div>
</div>
@endsection