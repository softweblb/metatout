<?php
/************************************************************
displaylist.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

<table class="table table-hover" style="width:100%">
    <thead>
        <tr bgcolor="#3c8dbc">
            <th>#</th>
            <th>Role</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
            @foreach($roles as $index => $role_info)
               <tr data-role_id="<?php echo $role_info->role_id; ?>">
                   <td><?php echo $role_info->role_id; ?></th>
                   <td><?php echo $role_info->role_name; ?></td>
                   <td><?php echo ( strlen($role_info->role_description) > 20 ) ?  substr($role_info->role_description, 20) : $role_info->role_description ; ?></td>
                   <td><img style="cursor: pointer" height="16" id="EDIT_ROLE_<?php echo $role_info->role_id; ?>" src="{{ url('assets/imgs/edit.png') }}" /></td>
                   <td><img style="cursor: pointer" height="16" id="DELETE_ROLE_<?php echo $role_info->role_id; ?>" src="{{ url('assets/imgs/delete.png') }}" /></td>
               </tr>
          @endforeach
    </tbody>
</table>