<?php
/************************************************************
editrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/


?>
@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/roles/editrole.js"></script>


<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
    <h4>Edit Role</h4>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div style="left:20%" class="ListRoleGirds">
                <form name="form_edit_role" id="FORM_EDIT_ROLE">
                <span id="hidden_fields">
                    {!! csrf_field() !!}
                    <input type="hidden" name="role_id" id="ROLE_ID" value="<?php echo $role_id; ?>" />
                </span>
               <div class="row">
                        <div class="col-md-2">Role Name</div>
                        <div class="col-md-10"><input type="text" name="role_name" id="ROLE_NAME" class="form-control" value="<?php echo $roles[0]->role_name; ?>" /></div>
                    </div>
                   <div class="row" style="height:5px;"></div>
                    <div class="row">
                        <div class="col-md-2">Role Description</div>
                        <div class="col-md-10">
                            <textarea style="width:100%;height:150px" name="role_description" id="ROLE_DESCRIPTION" style="resize:none" class="form-control"><?php echo $roles[0]->role_description; ?></textarea>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="cols-md-12">
                            <div class="tabbable tabbable-tabdrop">
								<ul class="nav nav-tabs"><li class="dropdown pull-right tabdrop active"><a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>&nbsp;<i class="fa fa-angle-down"></i> <b class="caret"></b></a><ul class="dropdown-menu"><li class="">
										<a href="#tab5" data-toggle="tab" aria-expanded="false">Section 5</a>
									</li><li class="active">
										<a href="#tab6" data-toggle="tab" aria-expanded="true">Section 6</a>
									</li><li class="">
										<a href="#tab7" data-toggle="tab" aria-expanded="false">Section 7</a>
									</li><li class="">
										<a href="#tab8" data-toggle="tab" aria-expanded="false">Section 8</a>
									</li></ul></li>
									<li class="">
										<a href="#tab1" data-toggle="tab" aria-expanded="false">Section 1</a>
									</li>
									<li class="">
										<a href="#tab2" data-toggle="tab" aria-expanded="false">Section 2</a>
									</li>
									<li class="">
										<a href="#tab3" data-toggle="tab" aria-expanded="false">Section 3</a>
									</li>
									<li>
										<a href="#tab4" data-toggle="tab">Section 4</a>
									</li>




								</ul>
								<div class="tab-content">
									<div class="tab-pane" id="tab1">
										<p>
											 I'm in Section 1.
										</p>
									</div>
									<div class="tab-pane" id="tab2">
										<p>
											 Howdy, I'm in Section 2.
										</p>
									</div>
									<div class="tab-pane" id="tab3">
										<p>
											 Howdy, I'm in Section 3.
										</p>
									</div>
									<div class="tab-pane" id="tab4">
										<p>
											 Howdy, I'm in Section 4.
										</p>
									</div>
									<div class="tab-pane" id="tab5">
										<p>
											 Howdy, I'm in Section 5.
										</p>
									</div>
									<div class="tab-pane active" id="tab6">
										<p>
											 Howdy, I'm in Section 6.
										</p>
									</div>
									<div class="tab-pane" id="tab7">
										<p>
											 Howdy, I'm in Section 7.
										</p>
									</div>
									<div class="tab-pane" id="tab8">
										<p>
											 Howdy, I'm in Section 8.
										</p>
									</div>
									<div class="tab-pane" id="tab9">
										<p>
											 Howdy, I'm in Section 9.
										</p>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                    <div class="row" style="height:5px;"></div>
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-2"><input type="button" name="btn_edit_role" id="BTN_EDIT_ROLE" class="btn btn-info" value="SAVE" /> </div>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection