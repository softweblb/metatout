<?php
/************************************************************
roles.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
Page of roles management where we can add/edit and delete roles
************************************************************/

?>

@extends('layouts.alayout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/roles/rolesmanagement.js"></script>


<div class="container-fluid" style="height: 100%;background-color: white;" align="center">
     <h4>List Roles</h4>
     <div class="row">
         <div class="col-md-2"></div>
         <div class="col-md-10">
             <div style="left:20%" class="ListRoleGirds">

            </div>
         </div>
     </div>
     <div class="row">
         <div class="col-md-10"></div>
         <div class="col-md-2">
             <input type="button" class="btn btn-primary" name="btn_add_roles" id="BTN_ADD_ROLES" value="ADD" />
         </div>
     </div>
</div>
@endsection