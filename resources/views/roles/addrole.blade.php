<?php
/************************************************************
addrole.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 7, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
View for add new role form
************************************************************/

?>


@extends('layouts.playout')

@section('content')
<script type="text/javascript" src="<?php echo url(); ?>/assets/js/roles/addrole.js"></script>

<form name="form_add_role" id="FORM_ADD_ROLE">
    <span id="hidden_fields">
        {!! csrf_field() !!}
    </span>
    <div class="container-fluid" style="width:500px;">
         <div class="row">
            <div class="col-sm-12"><h3>Add Role</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Role Name</div>
            <div class="col-sm-10"><input type="text" name="role_name" id="ROLE_NAME" class="form-control" value="" /></div>
        </div>
       <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-2">Role Description</div>
            <div class="col-sm-10">
                <textarea style="width:100%;height:150px" name="role_description" id="ROLE_DESCRIPTION" style="resize:none" class="form-control"></textarea>
            </div>
        </div>
        <div class="row" style="height:5px;"></div>
        <div class="row">
            <div class="col-sm-10"></div>
            <div class="col-sm-2"><input type="button" name="btn_add_role" id="BTN_ADD_ROLE" class="btn btn-info" value="SAVE" /> </div>
        </div>
    </div>
</form>
@endsection