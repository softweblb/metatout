<?php
/************************************************************
playout.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/

?>

<html lang="en">
    <head>
        <title>Administrator</title>
        <meta name="resource-type" content="document" />
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
        <meta name="robots" content="all, index, follow"/>
        <meta name="googlebot" content="all, index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <script src="{{ url('assets/js/jquery-1.11.2.min.js') }}"></script>
                <link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.css') }}"  />
        <link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap-theme.min.css') }}"  />
        <script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <link  rel="stylesheet"  type='text/css' href="{{ url('assets/css/themes/style.css') }}"  />
        <script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/app.js') }}"></script>
        <script src="{{ url('assets/js/colorbox/jquery.colorbox.js') }}" type="text/javascript"></script>
       <link href="{{ url('assets/js/colorbox/example4/colorbox.css') }}" rel="stylesheet" type="text/css" />
       <link href="{{ url('assets/admin/assets/global/css/components-md.css') }}" rel="stylesheet" type="text/css" />

       <script src="{{ url('assets/js/loadImage/js/load-image.all.min.js') }}"></script>
        <link href="{{ url('assets/js/jquery_file_upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
        <script src="{{ url('assets/js/jquery_file_upload/js/vendor/jquery.ui.widget.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.iframe-transport.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-process.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-image.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-audio.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-video.js') }}"></script>
        <script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-validate.js') }}"></script>
        <link rel="stylesheet" href="{{ url('assets/js/craftpip/css/jquery-confirm.css') }}">
        <script src="{{ url('assets/js/craftpip/js/jquery-confirm.js') }}"></script>
        <script src="{{ url('assets/js/datetimepicker/jquery.datetimepicker.js') }}"></script>
        <link type="text/css" rel="stylesheet" href="{{ url('assets/js/datetimepicker/jquery.datetimepicker.css') }}" />
                <script type='text/javascript'  src="{{ url('assets/ckeditor/ckeditor.js') }}"></script>
<script type='text/javascript'  src="{{ url('assets/ckeditor/adapters/jquery.js') }}"></script>
<!-- Plugins -->
@yield("plugins")
<!-- END Plugins -->
    </head>
    <body>
    <input type="hidden" name="base_url" id="BASE_URL" value="<?php echo url(); ?>" />
    @yield('content')
    </body>
</html>