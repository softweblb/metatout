<?php
/************************************************************
alayout.blade.php
Product :
Version : 1.0
Release : 0
Date Created : Aug 6, 2015
Developed By  : Mohamad. Mantach  PHP Department Softweb S.A.R.L
All Rights Reserved, Softweb S.A.R.L COPYRIGHT 2015

Page Description :
--
************************************************************/
{/** Initialisation block */
    $profile_url = session('profile_url');
    if( $profile_url == '' )
    {
        $profile_url = url() . "/assets/imgs/avatar.png";
    }

    $pages_allowed = Session('pages_allowed');
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>METAOUT - Administrator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="{{ url('assets/admin/assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="{{ url('assets/admin/assets/admin/pages/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/css/themes/backend.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ url('assets/admin/assets/global/css/components-md.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/admin/layout6/css/layout.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('assets/admin/assets/admin/layout6/css/custom.css') }}" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="{{ url('assets/admin/assets/global/plugins/bootstrap-select/bootstrap-select.min.css') }}"/>

<link href="{{ url('assets/js/colorbox/example4/colorbox.css') }}" rel="stylesheet" type="text/css" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ url('assets/admin/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<link href="{{ url('assets/js/jquery_file_upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ url('assets/css/themes/backend.css') }}">
<link rel="stylesheet" href="{{ url('assets/js/craftpip/css/jquery-confirm.css') }}">
<script src="{{ url('assets/js/datetimepicker/jquery.datetimepicker.js') }}"></script>
<link type="text/css" rel="stylesheet" href="{{ url('assets/js/datetimepicker/jquery.datetimepicker.css') }}" />
<script type="text/javascript" src="{{ url('assets/admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
           <script type='text/javascript'  src="{{ url('assets/ckeditor/ckeditor.js') }}"></script>
<script type='text/javascript'  src="{{ url('assets/ckeditor/adapters/jquery.js') }}"></script>

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-quick-sidebar-over-content">
    <span id="hidden_fields">
        <input type="hidden" name="base_url" id="BASE_URL" value="<?php echo url(); ?>" />
        {!! csrf_field() !!}
    </span>
	<!-- BEGIN MAIN LAYOUT -->
	<!-- HEADER BEGIN -->
    <header class="page-header">
        <nav class="navbar" role="navigation">
            <div class="container-fluid">
                <div class="havbar-header">
                	<!-- BEGIN LOGO -->
                    <a id="index" class="navbar-brand" href="{{ url('/administrator') }}">
                        <img src="{{ url('/assets/imgs/logo.png') }}" class="LOGOMAIN" alt="" height="32" />
                    </a>
                	<!-- END LOGO -->

	                <!-- BEGIN TOPBAR ACTIONS -->
	                <div class="topbar-actions">
		                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
						<form class="search-form" action="extra_search.html" method="GET">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search here" name="search_key">
								<span class="input-group-btn">
									<a href="javascript:;" class="btn submit"><i class="fa fa-search"></i></a>
								</span>
							</div>
						</form>
						<!-- END HEADER SEARCH BOX -->

	                	<!-- BEGIN GROUP NOTIFICATION -->
						<div class="btn-group-notification btn-group" id="header_notification_bar">
							<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<span class="badge">9</span>
							</button>
							<ul class="dropdown-menu-v2">
								<li class="external">
									<h3><span class="bold">12 pending</span> notifications</h3>
									<a href="#">view all</a>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-success">
														<i class="fa fa-plus"></i>
													</span>
													New user registered.
												</span>
												<span class="time">just now</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Server #12 overloaded.
												</span>
												<span class="time">3 mins</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-warning">
														<i class="fa fa-bell-o"></i>
													</span>
													Server #2 not responding.
												</span>
												<span class="time">10 mins</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-info">
														<i class="fa fa-bullhorn"></i>
													</span>
													Application error.
												</span>
												<span class="time">14 hrs</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Database overloaded 68%.
												</span>
												<span class="time">2 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
												A 	user IP blocked.
											</span>
												<span class="time">3 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-warning">
														<i class="fa fa-bell-o"></i>
													</span>
													Storage Server #4 not responding dfdfdfd.
												</span>
												<span class="time">4 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-info">
														<i class="fa fa-bullhorn"></i>
													</span>
													System Error.
												</span>
												<span class="time">5 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Storage server failed.
												</span>
												<span class="time">9 days</span>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
	                	<!-- END GROUP NOTIFICATION -->

	                	<!-- BEGIN USER PROFILE -->
		                <div class="btn-group-img btn-group">
							<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<img id="AVATAR_PROFILE_PIC" height="48" src="<?php echo $profile_url; ?>" alt="">
							</button>
							<ul class="dropdown-menu-v2" role="menu">
								<li class="active">
									<a class="EditProfile" href="{{ url('/administrator/profile/') }}/{{ session('user_id') }}">My Profile <span class="badge badge-danger">1</span> </a>
								</li>
								<li>
									<a href="#">My Inbox <span class="badge badge-info">3</span> </a>
								</li>
								<li>
									<a href="todo.html">My Tasks</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="lock_screen.html">Lock Screen</a>
								</li>
								<li>
									<a href="{{ URL::route('account-sign-out') }}">Sign Out</a>
								</li>
							</ul>
						</div>
						<!-- END USER PROFILE -->
					</div>
	                <!-- END TOPBAR ACTIONS -->
                </div>
            </div>
            <!--/container-->
        </nav>
    </header>
	<!-- HEADER END -->

	<!-- PAGE CONTENT BEGIN -->
    <div class="container-fluid">
    	<div class="page-content page-content-popup">
    		<!-- BEGIN PAGE CONTENT FIXED -->
			<div class="page-content-fixed-header">
				<ul class="page-breadcrumb">
					<li><a href="#">Applications</a></li>
				</ul>

				<div class="content-header-menu">
    				<!-- BEGIN DROPDOWN AJAX MENU -->
    				<div class="dropdown-ajax-menu btn-group">
						<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="fa fa-circle"></i>
							<i class="fa fa-circle"></i>
							<i class="fa fa-circle"></i>
						</button>
						<ul class="dropdown-menu-v2">
							<li> <a href="start.html">Application</a> </li>
							<li> <a href="start.html">Reports</a> </li>
							<li> <a href="start.html">Templates</a> </li>
							<li> <a href="start.html">Settings</a> </li>
						</ul>
					</div>
    				<!-- END DROPDOWN AJAX MENU -->

    				<!-- BEGIN MENU TOGGLER -->
    				<button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
	                    <span class="toggle-icon">
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </span>
	                </button>
    				<!-- END MENU TOGGLER -->
    			</div>
			</div>

			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper">
				<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
				<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
				<div style="min-width:300px;" class="page-sidebar navbar-collapse collapse">
					<!-- BEGIN SIDEBAR MENU -->
					<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
					<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
					<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
					<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
					<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
					<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
					<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
						<li>
							<a href="{{ url('/administrator/home') }}">
							<i class="icon-home"></i>
							<span class="title">Home</span>
							</a>
						</li>
						<li class="active">
							<a href="{{ url('/administrator/dashboard') }}">
							<i class="icon-list"></i>
							<span class="title">Dashboard</span>
							</a>
						</li>
						<li>
							<a class="EditProfile" href="{{ url('/administrator/profile/') }}/{{ session('user_id') }}">
							<i class="icon-user"></i>
							<span class="title">User Profile</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
							<i class="icon-folder"></i>
							<span class="title">System Management</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
							     <li><a href="{{ url('/administrator/UserManagement') }}"><i class="fa fa-users"></i> Users Management</a></li>
                                   <li><a href="{{ url('/administrator/RolesManagement') }}"><i class="fa fa-users"></i> Roles Management</a></li>
                                   <li><a href="{{ url('/administrator/CountryManagement') }}"><i class="fa fa-users"></i> Country Management</a></li>
                                   <li><a href="{{ url('/administrator/CompanyDetails') }}"><i class="fa fa-users"></i> Company Details Management</a></li>
                                   <li><a href="{{ url('/administrator/ConfigurationManagement') }}"><i class="fa fa-users"></i> Configuration Management</a></li>
                                   <li><a href="{{ url('/administrator/LanguagesManagement') }}"><i class="fa fa-users"></i> Language Management</a></li>
                                   <li><a href="{{ url('/administrator/TranslationsManagement') }}"><i class="fa fa-users"></i> Translations Management</a></li>
                                   <li><a href="{{ url('/administrator/CompanyPrecense') }}"><i class="fa fa-users"></i> Company Precense Management</a></li>
							</ul>
						</li>
						<li>
						  <a href="javascript:;">
							<i class="icon-folder"></i>
							<span class="title">CMS Management</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
                                    <li><a href="{{ url('/administrator/MenuManagement') }}"><i class="fa fa-wrench"></i> Menu Management</a></li>
                                    <li><a href="{{ url('/administrator/PageTypeManagement') }}"><i class="fa fa-wrench"></i> Page Types Management</a></li>
                                    <li><a href="{{ url('/administrator/PageManagement') }}"><i class="fa fa-wrench"></i> Page Management</a></li>
                                    <li><a href="{{ url('/administrator/FreeTextManagement') }}"><i class="fa fa-wrench"></i> Free Text Management</a></li>
                                    <?php
                                        for ($i = 0; $i < count($pages_allowed); $i++)
                                        {
                                            ?>
                                                <li><a href="<?php echo url(); ?>/administrator/PageContent/<?php echo $pages_allowed[$i]->cp_id;  ?>"><i class="fa fa-wrench"></i> <?php echo $pages_allowed[$i]->cp_page_title;  ?> Management</a></li>
                                            <?php
                                        }
                                    ?>
							</ul>
						</li>
					</ul>
					<!-- END SIDEBAR MENU -->
				</div>
			</div>
			<!-- END SIDEBAR -->

			<div class="page-fixed-main-content" style="min-height:620px;margin-left:300px;">
                @yield('content')
			</div>
    		<!-- END PAGE CONTENT FIXED -->

    		<!-- Copyright BEGIN -->
			<p class="copyright-v2"><?php echo date("Y"); ?> &copy; Metatout Pefabs.
	 		</p>
			<!-- Copyright END -->
    	</div>
    </div>
	<!-- PAGE CONTENT END -->
    <!-- END MAIN LAYOUT -->
    <a href="#index" class="go2top"><i class="icon-arrow-up"></i></a>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- Plugins -->
@yield("plugins")
<!-- END Plugins -->
<script src="{{ url('assets/js/jquery_file_upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/load-image.all.min.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/canvas-to-blob.min.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-ui.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-audio.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-video.js') }}"></script>
<script src="{{ url('assets/js/jquery_file_upload/js/jquery.fileupload-validate.js') }}"></script>

<script src="{{ url('assets/admin/assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ url('assets/admin/assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript" ></script>
<script src="{{ url('assets/admin/assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript" ></script>
<script src="{{ url('assets/admin/assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript" ></script>
<script src="{{ url('assets/admin/assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript" ></script>
<script src="{{ url('assets/admin/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/global/plugins/bootstrap-pagination/jquery.twbsPagination.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('assets/admin/assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/admin/layout6/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/admin/layout6/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/admin/layout6/scripts/index.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript" src="{{ url('assets/admin/assets/global/plugins/bootstrap-select/bootstrap-select.js') }}"></script>
<script src="{{ url('assets/js/craftpip/js/jquery-confirm.js') }}"></script>

<script src="{{ url('assets/js/admin.js') }}"></script>
<script src="{{ url('assets/js/colorbox/jquery.colorbox.js') }}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core componets
   	Layout.init(); // init layout
    Index.init(); // init index page
    QuickSidebar.init(); // init quick sidebar
    Tasks.initDashboardWidget(); // init tash dashboard widget
});
</script>
@yield('footer_js')
<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>